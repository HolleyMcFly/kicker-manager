package de.managerspiel.model;

public class Prioritaeteneintrag {

	private String fussballername;
	private int fussballer_id;
	private int prioritaet;

	public Prioritaeteneintrag() {
		this.fussballername = "";
		this.fussballer_id = -1;
		this.prioritaet = -1;
	}

	public int getFussballer_id() {
		return fussballer_id;
	}

	public void setFussballer_id(int fussballer_id) {
		this.fussballer_id = fussballer_id;
	}

	public String getFussballername() {
		return fussballername;
	}

	public void setFussballername(String fussballername) {
		this.fussballername = fussballername;
	}

	public int getPrioritaet() {
		return prioritaet;
	}

	public void setPrioritaet(int prioritaet) {
		this.prioritaet = prioritaet;
	}
}