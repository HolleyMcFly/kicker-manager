package de.managerspiel.model;

public class Ergebniseintrag {

	String mitspielername;
	Integer ergebnis;

	public Ergebniseintrag() {
		this.mitspielername = "";
		this.ergebnis = 0;
	}

	public Ergebniseintrag(String mitspielername, Integer ergebnis) {
		this.mitspielername = mitspielername;
		this.ergebnis = ergebnis;
	}

	public void setMitspielername(String pMitspielername) {
		this.mitspielername = pMitspielername;
	}

	public String getMitspielername() {
		return mitspielername;
	}

	public void setErgebnis(Integer pErgebnis) {
		this.ergebnis = pErgebnis;
	}

	public Integer getErgebnis() {
		return ergebnis;
	}
}
