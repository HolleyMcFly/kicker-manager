package de.managerspiel.model;

import de.managerspiel.util.Konstanten;

/**
 * In dieser Klasse stehen alle Daten zu einem Fu�baller nach der Auswertung,
 * die f�r die Erstellung des Textfiles notwendig sind.
 *
 */
public class FussballerErgebnis {

	private String fussballername;
	private int fussballer_id;
	private Integer spielposition;
	private int verein_id;
	private String verein_name;
	private int anzahlTore;
	private int punkteTore;
	private boolean gelbroteKarte;
	private int punkteGelbrot;
	private boolean roteKarte;
	private int punkteRot;
	private double note;
	private int punkteNote;
	private boolean gespielt;
	private Integer prioritaet;

	public FussballerErgebnis() {

		this.fussballername = "";
		this.fussballer_id = -1;
		this.spielposition = -1;
		this.verein_id = -1;
		this.verein_name = "";
		this.anzahlTore = 0;
		this.punkteTore = 0;
		this.gelbroteKarte = false;
		this.punkteGelbrot = 0;
		this.roteKarte = false;
		this.punkteRot = 0;
		this.note = 3.5;
		this.punkteNote = 0;
		this.gespielt = false;
		this.prioritaet = 0;
	}

	public static void init(FussballerErgebnis ferg) {

		ferg.setFussballername("");
		ferg.setFussballer_id(-1);
		ferg.setSpielposition(-1);
		ferg.setVerein_id(-1);
		ferg.setVereinName("");
		ferg.setAnzahlTore(0);
		ferg.setPunkteTore(0);
		ferg.setGelbroteKarte(false);
		ferg.setPunkteGelbrot(0);
		ferg.setRoteKarte(false);
		ferg.setPunkteRot(0);
		ferg.setNote(3.5);
		ferg.setPunkteNote(0);
		ferg.setGespielt(false);
		ferg.setPrioritaet(0);
	}

	public int getFussballer_id() {
		return fussballer_id;
	}

	public void setFussballer_id(int fussballer_id) {
		this.fussballer_id = fussballer_id;
	}

	public String getFussballername() {
		return fussballername;
	}

	public void setFussballername(String fussballername) {
		this.fussballername = fussballername;
	}

	public int getAnzahlTore() {
		return anzahlTore;
	}

	public void setAnzahlTore(int anzahlTore) {
		this.anzahlTore = anzahlTore;
	}

	public boolean isGelbroteKarte() {
		return gelbroteKarte;
	}

	public void setGelbroteKarte(boolean gelbroteKarte) {
		this.gelbroteKarte = gelbroteKarte;
	}

	public boolean isGespielt() {
		return gespielt;
	}

	public void setGespielt(boolean gespielt) {
		this.gespielt = gespielt;
	}

	public double getNote() {
		return note;
	}

	public void setNote(double note) {
		this.note = note;
	}

	public boolean isRoteKarte() {
		return roteKarte;
	}

	public void setRoteKarte(boolean roteKarte) {
		this.roteKarte = roteKarte;
	}

	public Integer getSpielposition() {
		return spielposition;
	}

	public void setSpielposition(Integer spielposition) {
		this.spielposition = spielposition;
	}

	public int getVerein_id() {
		return verein_id;
	}

	public void setVerein_id(int verein_id) {
		this.verein_id = verein_id;
	}

	public String getVerein_name() {
		return verein_name;
	}

	public void setVereinName(String vereinName) {
		this.verein_name = vereinName;
	}

	public Integer getPrioritaet() {
		return prioritaet;
	}

	public void setPrioritaet(Integer prioritaet) {
		this.prioritaet = prioritaet;
	}

	public int getPunkteGelbrot() {
		return punkteGelbrot;
	}

	public void setPunkteGelbrot(int punkteGelbrot) {
		this.punkteGelbrot = punkteGelbrot;
	}

	public int getPunkteNote() {
		return punkteNote;
	}

	public void setPunkteNote(int punkteNote) {
		this.punkteNote = punkteNote;
	}

	public int getPunkteRot() {
		return punkteRot;
	}

	public void setPunkteRot(int punkteRot) {
		this.punkteRot = punkteRot;
	}

	public int getPunkteTore() {
		return punkteTore;
	}

	public void setPunkteTore(int punkteTore) {
		this.punkteTore = punkteTore;
	}
	
	public String toString() {
		
		StringBuilder sb = new StringBuilder();
		sb.append(fussballername);
		sb.append(", ");
		sb.append("Spielposition: ");
		if (spielposition == Konstanten.SPIELPOSITION_TORMANN)
			sb.append("Tormann");
		else if (spielposition == Konstanten.SPIELPOSITION_VERTEIDIGER)
			sb.append("Verteidiger");
		else if (spielposition == Konstanten.SPIELPOSITION_MITTELFELDSPIELER)
			sb.append("Mittelfeldspieler");
		else if (spielposition == Konstanten.SPIELPOSITION_STUERMER)
			sb.append("St�rmer");
		sb.append(", Priorit�t: ");
		sb.append(prioritaet);
		return sb.toString();
	}
}