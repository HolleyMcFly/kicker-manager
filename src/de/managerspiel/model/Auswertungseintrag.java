package de.managerspiel.model;

/**
 * In dieser Klasse stehen alle Daten, die f�r die Erfassung der Punktekriterien
 * in der Maske notwendig sind.
 */
public class Auswertungseintrag {

	private String fussballername;
	private int fussballer_id;
	private int anzahl_tore;
	private boolean gelbrote_karte;
	private boolean rote_karte;
	private double note;
	private boolean gespielt;

	public Auswertungseintrag() {

		this.fussballername = "";
		this.fussballer_id = -1;
		this.anzahl_tore = 0;
		this.gelbrote_karte = false;
		this.rote_karte = false;
		this.note = 3.5;
		this.gespielt = false;
	}

	public int getFussballer_id() {
		return fussballer_id;
	}

	public void setFussballer_id(int fussballer_id) {
		this.fussballer_id = fussballer_id;
	}

	public String getFussballername() {
		return fussballername;
	}

	public void setFussballername(String fussballername) {
		this.fussballername = fussballername;
	}

	public int getAnzahl_tore() {
		return anzahl_tore;
	}

	public void setAnzahl_tore(int anzahl_tore) {
		this.anzahl_tore = anzahl_tore;
	}

	public boolean isGelbrote_karte() {
		return gelbrote_karte;
	}

	public void setGelbrote_karte(boolean gelbrote_karte) {
		this.gelbrote_karte = gelbrote_karte;
	}

	public boolean isGespielt() {
		return gespielt;
	}

	public void setGespielt(boolean gespielt) {
		this.gespielt = gespielt;
	}

	public double getNote() {
		return note;
	}

	public void setNote(double note) {
		this.note = note;
	}

	public boolean isRote_karte() {
		return rote_karte;
	}

	public void setRote_karte(boolean rote_karte) {
		this.rote_karte = rote_karte;
	}
}