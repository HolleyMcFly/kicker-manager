package de.managerspiel.exception;

import de.managerspiel.util.Util;

public class DBException extends Exception {

	private static final long serialVersionUID = 1L;

	String statement;
	String error;

	public DBException() {

	}

	public DBException(String statement, String error) {
		this.statement = statement;
		this.error = error;
	}

	public DBException(String error) {
		this.error = error;
	}

	public String getError() {
		return error;
	}

	public void setError(String error) {
		this.error = error;
	}

	public void handleError() {
		Util.showMessage("Fehler beim Lesen aus der Datenbank", "Bei der Ausführung des DB-Befehls \"" + statement
				+ "\" ist folgender Fehler aufgetreten:\r\n" + error);
	}
}
