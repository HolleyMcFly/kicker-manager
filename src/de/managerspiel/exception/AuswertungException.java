package de.managerspiel.exception;

import de.managerspiel.util.Util;


public class AuswertungException extends Exception {

	private static final long serialVersionUID = 1L;

	String error;
	
	public AuswertungException(String error) {
		this.error = error;
	}
	
	public void handleError() {
		Util.showMessage("Fehler bei der Erstellung der Auswertung", error);
	}
}
