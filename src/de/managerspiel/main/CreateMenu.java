package de.managerspiel.main;

import org.eclipse.swt.SWT;
import org.eclipse.swt.events.SelectionEvent;
import org.eclipse.swt.events.SelectionListener;
import org.eclipse.swt.widgets.Display;
import org.eclipse.swt.widgets.Menu;
import org.eclipse.swt.widgets.MenuItem;

import de.managerspiel.exception.DBException;
import de.managerspiel.util.SessionVariablen;
import de.managerspiel.util.Util;
import de.managerspiel.windows.WinAuswertung;
import de.managerspiel.windows.WinBearbeiten;
import de.managerspiel.windows.WinEinstellungen;
import de.managerspiel.windows.WinInfo;
import de.managerspiel.windows.WinLaden;
import de.managerspiel.windows.WinLoeschen;
import de.managerspiel.windows.WinNeu;
import de.managerspiel.windows.WinPrioritaetenVerwaltung;
import de.managerspiel.windows.WinVerwaltung;

public class CreateMenu {

	/**
	 * Erstellt den Menuleisten-Eintrag "Info"
	 */
	public void createMenuInfo(final Display display, Menu hauptmenu) {

		// Men�titel "Info"
		MenuItem menuInfo = new MenuItem(hauptmenu, SWT.CASCADE);
		menuInfo.setText("Info");
		Menu menuInfo_Untermenu = new Menu((SessionVariablen.getInstance().getShell()), SWT.DROP_DOWN);
		menuInfo.setMenu(menuInfo_Untermenu);

		// Men�eintrag "Einstellungen ..."
		MenuItem menuEinstellungen = new MenuItem(menuInfo_Untermenu, SWT.NULL);
		menuEinstellungen.setText("Einstellungen");
		menuEinstellungen.addSelectionListener(new SelectionListener() {

			public void widgetSelected(SelectionEvent arg0) {
				WinEinstellungen wEin = new WinEinstellungen();
				wEin.createWindowEinstellungen(display);
			};

			public void widgetDefaultSelected(SelectionEvent arg0) {}
		});

		// Men�eintrag "�ber ..."
		MenuItem menuInfo_ueber = new MenuItem(menuInfo_Untermenu, SWT.NULL);
		menuInfo_ueber.setText("�ber...");
		menuInfo_ueber.addSelectionListener(new SelectionListener() {

			public void widgetSelected(SelectionEvent arg0) {
				WinInfo cw = new WinInfo();
				cw.createWindowInfo(display);
			};

			public void widgetDefaultSelected(SelectionEvent arg0) {}
		});
	}

	/**
	 * Erstellt den Menuleisten-Eintrag "L�schen"
	 */
	public void createMenuL�schen(final Display display, Menu hauptmenu) {

		// Men�titel "L�schen"
		MenuItem menuLoeschen = new MenuItem(hauptmenu, SWT.CASCADE);
		menuLoeschen.setText("L�schen");
		Menu menuLoeschen_Untermenu = new Menu((SessionVariablen.getInstance().getShell()), SWT.DROP_DOWN);
		menuLoeschen.setMenu(menuLoeschen_Untermenu);

		// Men�eintrag "Tippspiel l�schen"
		MenuItem menuTippspiel_loeschen = new MenuItem(menuLoeschen_Untermenu, SWT.NULL);
		menuTippspiel_loeschen.setText("Tippspiel l�schen");
		menuTippspiel_loeschen.addSelectionListener(new SelectionListener() {

			public void widgetSelected(SelectionEvent arg0) {
				WinLoeschen cw = new WinLoeschen();
				cw.createWindowTippspielLoeschen(display);
			};

			public void widgetDefaultSelected(SelectionEvent arg0) {}
		});

		// Men�eintrag "Mitspieler l�schen"
		MenuItem menuMitspieler_loeschen = new MenuItem(menuLoeschen_Untermenu, SWT.NULL);
		menuMitspieler_loeschen.setText("Mitspieler l�schen");
		menuMitspieler_loeschen.addSelectionListener(new SelectionListener() {

			public void widgetSelected(SelectionEvent arg0) {

				if (!Util.isTippspielGeladen())
					return;

				WinLoeschen cw = new WinLoeschen();
				cw.createWindowMitspielerLoeschen(display);
			};

			public void widgetDefaultSelected(SelectionEvent arg0) {}
		});

		// Men�eintrag "Fu�baller l�schen"
		MenuItem menuFussballer_loeschen = new MenuItem(menuLoeschen_Untermenu, SWT.NULL);
		menuFussballer_loeschen.setText("Fu�baller l�schen");
		menuFussballer_loeschen.addSelectionListener(new SelectionListener() {

			public void widgetSelected(SelectionEvent arg0) {

				if (!Util.isTippspielGeladen())
					return;

				WinLoeschen cw = new WinLoeschen();
				cw.createWindowFussballerLoeschen(display);
			};

			public void widgetDefaultSelected(SelectionEvent arg0) {}
		});
	}

	/**
	 * Erstellt den Menuleisten-Eintrag "Bearbeiten"
	 */
	public void createMenuBearbeiten(final Display display, Menu hauptmenu) {

		// Men�titel "Bearbeiten"
		MenuItem menuBearbeiten = new MenuItem(hauptmenu, SWT.CASCADE);
		menuBearbeiten.setText("Bearbeiten");
		Menu menuBearbeiten_Untermenu = new Menu((SessionVariablen.getInstance().getShell()), SWT.DROP_DOWN);
		menuBearbeiten.setMenu(menuBearbeiten_Untermenu);

		// Men�eintrag "Tippspiel bearbeiten"
		MenuItem menuTippspiel_bearbeiten = new MenuItem(menuBearbeiten_Untermenu, SWT.NULL);
		menuTippspiel_bearbeiten.setText("Tippspiel bearbeiten");
		menuTippspiel_bearbeiten.addSelectionListener(new SelectionListener() {

			public void widgetSelected(SelectionEvent arg0) {
				WinBearbeiten cw = new WinBearbeiten();
				cw.createWindowTippspielBearbeiten(display);
			};

			public void widgetDefaultSelected(SelectionEvent arg0) {}
		});

		// Men�eintrag "Mitspieler bearbeiten"
		MenuItem menuMitspieler_bearbeiten = new MenuItem(menuBearbeiten_Untermenu, SWT.NULL);
		menuMitspieler_bearbeiten.setText("Mitspieler bearbeiten");
		menuMitspieler_bearbeiten.addSelectionListener(new SelectionListener() {

			public void widgetSelected(SelectionEvent arg0) {

				if (!Util.isTippspielGeladen())
					return;

				WinBearbeiten cw = new WinBearbeiten();
				cw.createWindowMitspielerBearbeiten(display);
			};

			public void widgetDefaultSelected(SelectionEvent arg0) {}
		});

		// Men�eintrag "Fu�baller bearbeiten"
		MenuItem menuFussballer_bearbeiten = new MenuItem(menuBearbeiten_Untermenu, SWT.NULL);
		menuFussballer_bearbeiten.setText("Fu�baller bearbeiten");
		menuFussballer_bearbeiten.addSelectionListener(new SelectionListener() {

			public void widgetSelected(SelectionEvent arg0) {

				if (!Util.isTippspielGeladen())
					return;

				WinBearbeiten cw = new WinBearbeiten();
				cw.createWindowFussballerBearbeiten(display);
			};

			public void widgetDefaultSelected(SelectionEvent arg0) {}
		});
	}

	/**
	 * Erstellt den Menuleisten-Eintrag "Verwaltung"
	 */
	public void createMenuVerwaltung(final Display display, Menu hauptmenu) {

		// Men�titel "Verwaltung"
		MenuItem menuVerwaltung = new MenuItem(hauptmenu, SWT.CASCADE);
		menuVerwaltung.setText("Verwaltung");
		Menu menuVerwaltung_Untermenu = new Menu((SessionVariablen.getInstance().getShell()), SWT.DROP_DOWN);
		menuVerwaltung.setMenu(menuVerwaltung_Untermenu);

		// Men�eintrag "Priorit�ten erfassen"
		MenuItem menuVerwaltung_Prioritaetenerfassen = new MenuItem(menuVerwaltung_Untermenu, SWT.NULL);
		menuVerwaltung_Prioritaetenerfassen.setText("Priorit�ten erfassen");
		menuVerwaltung_Prioritaetenerfassen.addSelectionListener(new SelectionListener() {

			public void widgetSelected(SelectionEvent arg0) {

				if (!Util.isTippspielGeladen())
					return;

				if (!Util.isSpieltagGeladen())
					return;

				WinPrioritaetenVerwaltung cw = new WinPrioritaetenVerwaltung();
				cw.createWindowPrioritaeten_AuswahlMitspieler(display);
			};

			public void widgetDefaultSelected(SelectionEvent arg0) {}
		});

		// Men�eintrag "Fu�baller wechseln"
		MenuItem menuVerwaltung_Fussballerwechseln = new MenuItem(menuVerwaltung_Untermenu, SWT.NULL);
		menuVerwaltung_Fussballerwechseln.setText("Fu�baller wechseln");
		menuVerwaltung_Fussballerwechseln.addSelectionListener(new SelectionListener() {

			public void widgetSelected(SelectionEvent arg0) {
				WinVerwaltung cw = new WinVerwaltung();
				cw.createWindowFussballerWechseln(display);
			};

			public void widgetDefaultSelected(SelectionEvent arg0) {}
		});

		// Men�eintrag "Vereine verwalten"
		MenuItem menuVerwaltung_Vereineverwalten = new MenuItem(menuVerwaltung_Untermenu, SWT.NULL);
		menuVerwaltung_Vereineverwalten.setText("Vereine verwalten");
		menuVerwaltung_Vereineverwalten.addSelectionListener(new SelectionListener() {

			public void widgetSelected(SelectionEvent arg0) {

				if (!Util.isTippspielGeladen())
					return;

				if (!Util.isSpieltagGeladen())
					return;

				WinVerwaltung cw = new WinVerwaltung();
				cw.createWindowVerwaltungVerein(display);
			}

			public void widgetDefaultSelected(SelectionEvent arg0) {}
		});
	}

	/**
	 * Erstellt den Menuleisten-Eintrag "Auswertung"
	 */
	public void createMenuAuswertung(final Display display, Menu hauptmenu) {

		// Men�titel "Auswertung"
		MenuItem menuAuswertung = new MenuItem(hauptmenu, SWT.CASCADE);
		menuAuswertung.setText("Auswertung");
		Menu menuAuswertung_Untermenu = new Menu((SessionVariablen.getInstance().getShell()), SWT.DROP_DOWN);
		menuAuswertung.setMenu(menuAuswertung_Untermenu);

		// Men�eintrag "Auswertung starten"
		MenuItem menuAuswertung_Auswertungstarten = new MenuItem(menuAuswertung_Untermenu, SWT.NULL);
		menuAuswertung_Auswertungstarten.setText("Auswertungsdaten erfassen");
		menuAuswertung_Auswertungstarten.addSelectionListener(new SelectionListener() {

			public void widgetSelected(SelectionEvent arg0) {

				if (!Util.isTippspielGeladen())
					return;

				if (!Util.isSpieltagGeladen())
					return;

				SpieltagErfassen aw = new SpieltagErfassen();
				aw.createWindowMain(display);
			};

			public void widgetDefaultSelected(SelectionEvent arg0) {}
		});

		// Men�eintrag "Auswertung durchf�hren"
		MenuItem menuAuswertung_Auswertungdurchfuehren = new MenuItem(menuAuswertung_Untermenu, SWT.NULL);
		menuAuswertung_Auswertungdurchfuehren.setText("Auswertung durchf�hren");
		menuAuswertung_Auswertungdurchfuehren.addSelectionListener(new SelectionListener() {

			public void widgetSelected(SelectionEvent arg0) {

				if (!Util.isTippspielGeladen())
					return;

				if (!Util.isSpieltagGeladen())
					return;

				WinAuswertung cw = new WinAuswertung();
				cw.createWindowAuswertungErstellen(display);
			};

			public void widgetDefaultSelected(SelectionEvent arg0) {}
		});

		// Men�eintrag "Fu�ballerliste erstellen"
		MenuItem menuAuswertung_Fussballerliste = new MenuItem(menuAuswertung_Untermenu, SWT.NULL);
		menuAuswertung_Fussballerliste.setText("Fu�ballerliste erstellen");
		menuAuswertung_Fussballerliste.addSelectionListener(new SelectionListener() {

			@Override
			public void widgetSelected(SelectionEvent arg0) {

				if (!Util.isTippspielGeladen())
					return;

				try {
					FussballerlisteErstellen.createFussballListe();
				}
				catch (DBException e) {
					e.handleError();
				}
			}

			@Override
			public void widgetDefaultSelected(SelectionEvent arg0) {}
		});
	}

	/**
	 * Erstellt den Menuleisten-Eintrag "Laden"
	 */
	public void createMenuLaden(final Display display, Menu hauptmenu) {

		// Men�titel "Laden"
		MenuItem menuLaden = new MenuItem(hauptmenu, SWT.CASCADE);
		menuLaden.setText("Laden");
		Menu menuLaden_Untermenu = new Menu((SessionVariablen.getInstance().getShell()), SWT.DROP_DOWN);
		menuLaden.setMenu(menuLaden_Untermenu);

		// Men�eintrag "Tippspiel"
		MenuItem menuLaden_Tippspiel = new MenuItem(menuLaden_Untermenu, SWT.NULL);
		menuLaden_Tippspiel.setText("Tippspiel");
		menuLaden_Tippspiel.addSelectionListener(new SelectionListener() {

			public void widgetSelected(SelectionEvent arg0) {
				WinLaden cw = new WinLaden();
				cw.createWindowLadenTippspiel(display);
			};

			public void widgetDefaultSelected(SelectionEvent arg0) {}
		});

		// Men�eintrag "Spieltag"
		MenuItem menuLaden_Spieltag = new MenuItem(menuLaden_Untermenu, SWT.NULL);
		menuLaden_Spieltag.setText("Spieltag");
		menuLaden_Spieltag.addSelectionListener(new SelectionListener() {

			public void widgetSelected(SelectionEvent arg0) {
				WinLaden cw = new WinLaden();
				cw.createWindowLadenSpieltag(display);
			};

			public void widgetDefaultSelected(SelectionEvent arg0) {}
		});
	}

	/**
	 * Erstellt den Menuleisten-Eintrag "Neu"
	 */
	public void createMenuNeu(final Display display, Menu hauptmenu) {

		// Men�titel "Neu"
		MenuItem menuNeu = new MenuItem(hauptmenu, SWT.CASCADE);
		menuNeu.setText("Neu");
		Menu menuNeu_Untermenu = new Menu((SessionVariablen.getInstance().getShell()), SWT.DROP_DOWN);
		menuNeu.setMenu(menuNeu_Untermenu);

		// Men�eintrag "Tippspiel"
		MenuItem menuNeu_Tippspiel = new MenuItem(menuNeu_Untermenu, SWT.NULL);
		menuNeu_Tippspiel.setText("Tippspiel");
		menuNeu_Tippspiel.addSelectionListener(new SelectionListener() {

			public void widgetSelected(SelectionEvent arg0) {
				WinNeu cw = new WinNeu();
				cw.createWindowNeuTippspiel(display);
			};

			public void widgetDefaultSelected(SelectionEvent arg0) {}
		});

		// Men�eintrag "Mitspieler"
		MenuItem menuNeu_Mitspieler = new MenuItem(menuNeu_Untermenu, SWT.NULL);
		menuNeu_Mitspieler.setText("Mitspieler");
		menuNeu_Mitspieler.addSelectionListener(new SelectionListener() {

			public void widgetSelected(SelectionEvent arg0) {

				if (!Util.isTippspielGeladen())
					return;

				WinNeu cw = new WinNeu();
				cw.createWindowNeuMitspieler(display);
			};

			public void widgetDefaultSelected(SelectionEvent arg0) {}
		});

		// Men�eintrag "Fu�baller"
		MenuItem menuNeu_Fussballer = new MenuItem(menuNeu_Untermenu, SWT.NULL);
		menuNeu_Fussballer.setText("Fu�baller");
		menuNeu_Fussballer.addSelectionListener(new SelectionListener() {

			public void widgetSelected(SelectionEvent arg0) {

				if (!Util.isTippspielGeladen())
					return;

				WinNeu cw = new WinNeu();
				cw.createWindowNeuFussballer(display);
			};

			public void widgetDefaultSelected(SelectionEvent arg0) {
			}
		});
	}
}
