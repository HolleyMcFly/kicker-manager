package de.managerspiel.main;

import java.awt.Dimension;
import java.awt.Toolkit;
import java.util.List;

import org.eclipse.swt.SWT;
import org.eclipse.swt.events.SelectionEvent;
import org.eclipse.swt.events.SelectionListener;
import org.eclipse.swt.graphics.Color;
import org.eclipse.swt.graphics.Font;
import org.eclipse.swt.layout.GridData;
import org.eclipse.swt.layout.GridLayout;
import org.eclipse.swt.widgets.Button;
import org.eclipse.swt.widgets.Display;
import org.eclipse.swt.widgets.Label;
import org.eclipse.swt.widgets.Menu;
import org.eclipse.swt.widgets.Shell;

import de.managerspiel.db.DBCreate;
import de.managerspiel.db.DBRead;
import de.managerspiel.db.DBUtil;
import de.managerspiel.exception.DBException;
import de.managerspiel.util.Konstanten;
import de.managerspiel.util.SessionVariablen;
import de.managerspiel.util.Util;

public class Managermain {

	public Display display;
	private Menu hauptmenu;

	public static void main(String[] args) {
		new Managermain();
	}

	Managermain() {

		DBUtil.open();

		display = new Display();
		Shell mainShell = Util.createShell(display, "Kicker - Managerspiel", Konstanten.WINDOW_NEU_HAUPTMENUE_BREITE,
				Konstanten.WINDOW_NEU_HAUPTMENUE_HOEHE);
		mainShell.setBackground(new Color(display, 255, 255, 255));
		SessionVariablen.getInstance().setShell(mainShell);

		createGUI();
		createDatabase();

		// Einstellungen laden
		try {
			DBRead.setAllEinstellungen();
		}
		catch (DBException e) {
			e.handleError();
			System.exit(1);
		}

		// Position des Fensters festlegen und anzeigen
		Dimension d = Toolkit.getDefaultToolkit().getScreenSize();
		(SessionVariablen.getInstance().getShell()).setLocation((d.width - Konstanten.WINDOW_NEU_HAUPTMENUE_BREITE) / 2,
				(d.height - Konstanten.WINDOW_NEU_HAUPTMENUE_HOEHE) / 2);

		(SessionVariablen.getInstance().getShell()).open();

		while (!(SessionVariablen.getInstance().getShell()).isDisposed()) {
			if (!display.readAndDispatch()) {
				display.sleep();
			}
		}
	}

	/**
	 * Erstellt die grafische Oberfläche
	 */
	private void createGUI() {

		createMenu();

		GridLayout gridl = new GridLayout();
		gridl.verticalSpacing = 20;
		gridl.marginHeight = 20;
		gridl.marginWidth = 20;
		gridl.numColumns = 2;
		gridl.makeColumnsEqualWidth = false;
		(SessionVariablen.getInstance().getShell()).setLayout(gridl);

		// aktueller Tippspielname
		SessionVariablen.getInstance().setNamelabel(new Label((SessionVariablen.getInstance().getShell()), SWT.CENTER));
		(SessionVariablen.getInstance().getNamelabel()).setText("Aktuelles Tippspiel:");
		(SessionVariablen.getInstance().getNamelabel()).setBackground(new Color(display, 255, 255, 255));
		(SessionVariablen.getInstance().getNamelabel()).setLayoutData(new GridData(GridData.HORIZONTAL_ALIGN_FILL));
		SessionVariablen.getInstance().setNamelabel(new Label((SessionVariablen.getInstance().getShell()), SWT.CENTER));
		(SessionVariablen.getInstance().getNamelabel()).setBackground(new Color(display, 255, 255, 255));
		(SessionVariablen.getInstance().getNamelabel()).setFont(new Font(display, "Arial", 8, SWT.ITALIC));

		if ("".equalsIgnoreCase(SessionVariablen.getInstance().getTippspielname())) {
			String text = getTippspielIfOnlyOne();
			if (text != null && text.length() > 0)
				(SessionVariablen.getInstance().getNamelabel()).setText(text);
			else
				(SessionVariablen.getInstance().getNamelabel()).setText("kein Spiel geladen");
		}
		else {
			(SessionVariablen.getInstance().getNamelabel()).setText(SessionVariablen.getInstance().getTippspielname());
		}

		// aktueller Spieltag
		SessionVariablen.getInstance().setSpieltaglabel(new Label((SessionVariablen.getInstance().getShell()), SWT.CENTER));
		(SessionVariablen.getInstance().getSpieltaglabel()).setText("Aktueller Spieltag:");
		(SessionVariablen.getInstance().getSpieltaglabel()).setBackground(new Color(display, 255, 255, 255));
		(SessionVariablen.getInstance().getSpieltaglabel()).setLayoutData(new GridData(GridData.HORIZONTAL_ALIGN_FILL));
		SessionVariablen.getInstance().setSpieltaglabel(new Label((SessionVariablen.getInstance().getShell()), SWT.CENTER));
		(SessionVariablen.getInstance().getSpieltaglabel()).setBackground(new Color(display, 255, 255, 255));
		(SessionVariablen.getInstance().getSpieltaglabel()).setFont(new Font(display, "Arial", 8, SWT.ITALIC));

		if ("".equalsIgnoreCase(SessionVariablen.getInstance().getSpieltagtext()))
			(SessionVariablen.getInstance().getSpieltaglabel()).setText("kein Spieltag festgelegt");
		else
			(SessionVariablen.getInstance().getSpieltaglabel()).setText(SessionVariablen.getInstance().getSpieltagtext());

		Button beendenbutton = new Button(SessionVariablen.getInstance().getShell(), SWT.RIGHT);
		beendenbutton.setText("Programm beenden");
		beendenbutton.addSelectionListener(new SelectionListener() {

			public void widgetSelected(SelectionEvent arg0) {
				DBUtil.close();
				(SessionVariablen.getInstance().getShell()).close();
			}

			public void widgetDefaultSelected(SelectionEvent arg0) {}
		});
	}

	/**
	 * Erstellt das Hauptmenü
	 */
	private void createMenu() {
		
		// Menüleiste
		hauptmenu = new Menu((SessionVariablen.getInstance().getShell()), SWT.BAR);
		(SessionVariablen.getInstance().getShell()).setMenuBar(hauptmenu);

		// Menuleisten-Eintrag: "Neu"
		CreateMenu cm_neu = new CreateMenu();
		cm_neu.createMenuNeu(display, hauptmenu);
		
		// Menuleisten-Eintrag: "Laden"
		CreateMenu cm_laden = new CreateMenu();
		cm_laden.createMenuLaden(display, hauptmenu);
		
		// Menuleisten-Eintrag: "SpieltagErfassen"
		CreateMenu cm_auswertung = new CreateMenu();
		cm_auswertung.createMenuAuswertung(display, hauptmenu);
		
		// Menuleisten-Eintrag: "Verwaltung"
		CreateMenu cm_verwaltung = new CreateMenu();
		cm_verwaltung.createMenuVerwaltung(display, hauptmenu);
		
		// Menüleisten-Eintrag: "Bearbeiten"
		CreateMenu cm_bearbeiten = new CreateMenu();
		cm_bearbeiten.createMenuBearbeiten(display, hauptmenu);
		
		// Menuleisten-Eintrag: "Löschen"
		CreateMenu cm_loeschen = new CreateMenu();
		cm_loeschen.createMenuLöschen(display, hauptmenu);
		
		// Menuleisten-Eintrag: "Info"
		CreateMenu cm_info = new CreateMenu();
		cm_info.createMenuInfo(display, hauptmenu);
	}

	/**
	 * Selektiert das Tippspiel zum Programmstart, falls nur eines erfasst ist.
	 */
	private String getTippspielIfOnlyOne() {

		try {
			List<String> tippspiele = DBRead.getTippspiele();

			if (tippspiele.size() != 1)
				return "";

			int id = DBRead.getTippspielID(tippspiele.get(0));

			DBRead.setTippspielData(id);
			return SessionVariablen.getInstance().getTippspielname();

		}
		catch (DBException re) {
			return "";
		}
	}

	/**
	 * Legt die Datenbanktabellen neu an, wenn sie nicht schon vorhanden sind.
	 */
	private void createDatabase() {

		try {
			DBCreate.createTable_Tippspiel(SessionVariablen.getInstance().getStmt());
			DBCreate.createTable_Mitspieler(SessionVariablen.getInstance().getStmt());
			DBCreate.createTable_Verein(SessionVariablen.getInstance().getStmt());
			DBCreate.createTable_Fussballer(SessionVariablen.getInstance().getStmt());
			DBCreate.createTable_Punktekriterien(SessionVariablen.getInstance().getStmt());
			DBCreate.createTable_R_MS_FB(SessionVariablen.getInstance().getStmt());
			DBCreate.createTable_Prioritaeten(SessionVariablen.getInstance().getStmt());
			DBCreate.createTable_Einstellungen(SessionVariablen.getInstance().getStmt());
		}
		catch (DBException e) {
			e.handleError();
			System.exit(1);
		}
	}
}
