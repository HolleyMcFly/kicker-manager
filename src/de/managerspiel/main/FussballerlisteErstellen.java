package de.managerspiel.main;

import java.util.List;

import de.managerspiel.db.DBRead;
import de.managerspiel.exception.DBException;
import de.managerspiel.util.Konstanten;
import de.managerspiel.util.SessionVariablen;

public class FussballerlisteErstellen {

	public static void createFussballListe() throws DBException {

		List<String> mitspieler = DBRead.getAlleMitspieler();

		StringBuilder outString = new StringBuilder();
		int i = 0;
		for (String spieler : mitspieler) {

			if (i != 0)
				outString.append("\r\n").append("----------------------------------------------------").append("\r\n");

			i++;

			outString.append("Tipper: ").append(spieler).append("\r\n\r\n");

			SessionVariablen.getInstance().setMitspielerId(DBRead.getMitspielerID(spieler));

			outString.append("Torm�nner:").append("\r\n");
			int position = Konstanten.SPIELPOSITION_TORMANN;
			List<String> fussballerListe = DBRead.getFussballer_MS_SP(position);
			for (String fussballer : fussballerListe) {
				outString.append("\t").append(fussballer).append("\r\n");
			}

			outString.append("Abwehrspieler:").append("\r\n");
			position = Konstanten.SPIELPOSITION_VERTEIDIGER;
			fussballerListe = DBRead.getFussballer_MS_SP(position);
			for (String fussballer : fussballerListe) {
				outString.append("\t").append(fussballer).append("\r\n");
			}

			outString.append("Mittelfeldspieler:").append("\r\n");
			position = Konstanten.SPIELPOSITION_MITTELFELDSPIELER;
			fussballerListe = DBRead.getFussballer_MS_SP(position);
			for (String fussballer : fussballerListe) {
				outString.append("\t").append(fussballer).append("\r\n");
			}

			outString.append("St�rmer:").append("\r\n");
			position = Konstanten.SPIELPOSITION_STUERMER;
			fussballerListe = DBRead.getFussballer_MS_SP(position);
			for (String fussballer : fussballerListe) {
				outString.append("\t").append(fussballer).append("\r\n");
			}
		}

		System.out.println(outString.toString());
	}
}
