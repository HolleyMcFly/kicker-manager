package de.managerspiel.main;

import java.io.DataOutputStream;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.IOException;
import java.util.ArrayList;
import java.util.Collections;
import java.util.Enumeration;
import java.util.Hashtable;
import java.util.List;

import com.lowagie.text.Chunk;
import com.lowagie.text.Document;
import com.lowagie.text.DocumentException;
import com.lowagie.text.Element;
import com.lowagie.text.FontFactory;
import com.lowagie.text.Paragraph;
import com.lowagie.text.pdf.PdfPCell;
import com.lowagie.text.pdf.PdfPTable;
import com.lowagie.text.pdf.PdfWriter;

import de.managerspiel.db.DBRead;
import de.managerspiel.db.DBUtil;
import de.managerspiel.exception.AuswertungException;
import de.managerspiel.exception.DBException;
import de.managerspiel.model.Ergebniseintrag;
import de.managerspiel.model.FussballerErgebnis;
import de.managerspiel.sort.ErgebnisseSort;
import de.managerspiel.sort.FussballerErgebnisComparator;
import de.managerspiel.util.Konstanten;
import de.managerspiel.util.SessionVariablen;
import de.managerspiel.util.Util;

public class AuswertungErstellen {

	ArrayList<PdfPTable> arlPDFTables;
	ArrayList<Ergebniseintrag> ergTag;
	ArrayList<Ergebniseintrag> ergGesamt;

	/**
	 * Diese Methode steuert die Durchf�hrung der Auswertung.
	 * 
	 * Die Auswertung l�uft in mehreren Schritten:
	 * - Iteration �ber alle Mitspieler des Tippspiels
	 * - Einlesen aller Fu�baller zum jeweiligen Mitspieler
	 * - Sortieren der Fu�baller nach Priorit�ten
	 * - Entfernen der Fu�baller, die nicht gespielt haben
	 * - Entfernen der Fu�baller, die nicht gewertet werden
	 * - Bewertung der verbleibenden Fu�baller
	 * - Erstellung eines Textfiles mit den ermittelten Daten
	 */
	public void auswertungDurchfuehren(String strPath, boolean bolOneFile, boolean bolPDF, boolean bolGesamtwertung) throws DBException,
			AuswertungException {

		List<FussballerErgebnis> fergs = new ArrayList<FussballerErgebnis>();

		int anzahlMitspieler = DBRead.getAnzahlMitspieler();

		// Iteration �ber alle Mitspieler
		for (int i = 0; i < anzahlMitspieler; i++) {

			// setze i-te Mitspieler_id
			List<String> mitspieler = DBRead.getAlleMitspieler();
			SessionVariablen.getInstance().setMitspielerId(DBRead.getMitspielerID(mitspieler.get(i)));

			// Wenn f�r den aktuellen Spieltag keine Punktekriterien erfasst wurden, Meldung ausgeben und beenden
			int anzKriterien = DBRead.getAnzahlPunktekriterien();
			if (anzKriterien == 0) {
				Util.showMessage("Fehler", "Die Daten zum aktuellen Spieltag wurden noch nicht erfasst!");
				return;
			}

			// Wenn f�r den aktuellen Spieltag keine Priorit�ten erfasst wurden, werden die des vorherigen Spieltags kopiert
			checkPrioritaeten();

			fergs = getFussballerForMitspieler();
			Collections.sort(fergs, new FussballerErgebnisComparator());
			entferneFussballerOhneWertung(fergs);
			entferneFussballerNichtAufgestellt(fergs);
			bewerteFussballer(fergs);
			
			handleCreateFile(fergs, strPath, bolPDF, bolOneFile, bolGesamtwertung);
		}
		
		if (bolPDF) {
			try {
				createPdf(strPath, bolOneFile, bolPDF, bolGesamtwertung);
			}
			catch (FileNotFoundException e) {
				throw new AuswertungException(e.getMessage());
			}
			catch (DocumentException e) {
				throw new AuswertungException(e.getMessage());
			}
		}

		if (!bolGesamtwertung) {
			Util.showMessage("Fertig", "Die Auswertung wurde nach " + SessionVariablen.getInstance().getEinstellung(Konstanten.AUSGABEPFAD)
					+ " geschrieben.");
			(SessionVariablen.getInstance().getShell()).setVisible(true);
		}
	}

	/**
	 * Diese Methode liefert alle Daten zu den Fu�ballern eines Mitspielers
	 */
	private List<FussballerErgebnis> getFussballerForMitspieler() throws DBException {

		List<FussballerErgebnis> fergs = new ArrayList<FussballerErgebnis>();
		List<String> fussballerList = DBRead.getAlleFussballerMS();

		for (String fussballer : fussballerList) {

			FussballerErgebnis ferg = new FussballerErgebnis();

			// Fu�ballername
			ferg.setFussballername(fussballer);

			// ID holen
			int fussballer_id = 0;
			fussballer_id = DBRead.getFussballerID(ferg.getFussballername());
			ferg.setFussballer_id(fussballer_id);

			// Spielposition holen
			int spielposition = 0;
			spielposition = DBRead.getSpielposition(ferg.getFussballer_id());
			ferg.setSpielposition(spielposition);

			// Vereinid holen
			int verein_id = 0;
			verein_id = DBRead.getVereinIdFS(ferg.getFussballer_id());
			ferg.setVerein_id(verein_id);

			// Vereinsname holen
			String vereinsname = "";
			vereinsname = DBRead.getVerein(ferg.getVerein_id());
			ferg.setVereinName(vereinsname);

			// Anzahl der Tore holen
			int anzahl_tore = 0;
			anzahl_tore = DBRead.getAnzahl_Tore(ferg.getFussballer_id());
			ferg.setAnzahlTore(anzahl_tore);

			// Flag "gespielt" holen
			boolean gespielt = false;
			gespielt = DBRead.getGespielt(ferg.getFussballer_id());
			ferg.setGespielt(gespielt);

			// Flag "gelbrote Karte" holen
			boolean gelbrot = false;
			gelbrot = DBRead.getGelbRot(ferg.getFussballer_id());
			ferg.setGelbroteKarte(gelbrot);

			// Flag "rote Karte" holen
			boolean rot = false;
			rot = DBRead.getRot(ferg.getFussballer_id());
			ferg.setRoteKarte(rot);

			// Note holen
			double note = 0.0;
			note = DBRead.getNote(ferg.getFussballer_id());
			ferg.setNote(note);

			// Priorit�t holen
			SessionVariablen.getInstance().setFussballerId(ferg.getFussballer_id());
			int prio = 0;
			prio = DBRead.getPrioritaet();
			ferg.setPrioritaet(prio);

			fergs.add(ferg);
		}

		return fergs;
	}

	/**
	 * Diese Methode entfernt die Spieler, die am aktuellen Spieltag nicht gespielt haben, aus der Liste
	 */
	private void entferneFussballerOhneWertung(List<FussballerErgebnis> fergs) {

		List<FussballerErgebnis> toRemove = new ArrayList<FussballerErgebnis>();

		for (FussballerErgebnis ferg : fergs) {
			if (!ferg.isGespielt())
				toRemove.add(ferg);
		}

		for (FussballerErgebnis ferg : toRemove)
			fergs.remove(ferg);
	}

	/**
	 * Diese Methode entfernt die Spieler, die nicht gewertet werden, weil die jeweilige Position schon voll besetzt ist
	 */
	private void entferneFussballerNichtAufgestellt(List<FussballerErgebnis> fergs) {

		List<FussballerErgebnis> guteSpieler = new ArrayList<FussballerErgebnis>();
		for (FussballerErgebnis ferg : fergs) {
			if (ferg.getSpielposition() == Konstanten.SPIELPOSITION_TORMANN) {
				guteSpieler.add(ferg);
				break;
			}
		}

		int curAnzahl = 0;
		// Es spielen h�chstens 3 Verteidiger
		for (FussballerErgebnis ferg : fergs) {
			if (curAnzahl == Konstanten.MAX_VERTEIDIGER_AUFGESTELLT)
				break;

			if (ferg.getSpielposition() == Konstanten.SPIELPOSITION_VERTEIDIGER) {
				guteSpieler.add(ferg);
				curAnzahl++;
			}
		}

		curAnzahl = 0;
		// Es spielen h�chstens 5 Mittelfeldspieler
		for (FussballerErgebnis ferg : fergs) {
			if (curAnzahl == Konstanten.MAX_MITTELFELDSPIELER_AUFGESTELLT)
				break;

			if (ferg.getSpielposition() == Konstanten.SPIELPOSITION_MITTELFELDSPIELER) {
				guteSpieler.add(ferg);
				curAnzahl++;
			}
		}

		curAnzahl = 0;
		// Es spielen h�chstens 2 St�rmer
		for (FussballerErgebnis ferg : fergs) {
			if (curAnzahl == Konstanten.MAX_STUERMER_AUFGESTELLT)
				break;

			if (ferg.getSpielposition() == Konstanten.SPIELPOSITION_STUERMER) {
				guteSpieler.add(ferg);
				curAnzahl++;
			}
		}

		List<FussballerErgebnis> toRemove = new ArrayList<FussballerErgebnis>();
		for (FussballerErgebnis ferg : fergs) {
			if (!guteSpieler.contains(ferg))
				toRemove.add(ferg);
		}

		for (FussballerErgebnis ferg : toRemove) {
			fergs.remove(ferg);
		}
	}

	/**
	 * Diese Methode bewertet die Fu�baller in der �bergebenen Liste und schreibt die ermittelte Punktezahl zum jeweiligen Objekt
	 */
	private void bewerteFussballer(List<FussballerErgebnis> fergs) {

		for (FussballerErgebnis ferg : fergs) {

			ferg.setPunkteTore(getPunkteForTore(ferg));

			if (ferg.isGelbroteKarte() == true)
				ferg.setPunkteGelbrot(Konstanten.PUNKTE_GELBROTE_KARTE);

			if (ferg.isRoteKarte() == true)
				ferg.setPunkteRot(Konstanten.PUNKTE_ROTE_KARTE);

			ferg.setPunkteNote(getPunkteForNote(ferg));
		}
	}

	/**
	 * Ermittelt die Punkte f�r die Anzahl der geschossenen Tore in Abh�ngigkeit zur Spielposition
	 */
	private int getPunkteForTore(FussballerErgebnis ferg) {

		if (ferg.getSpielposition() == Konstanten.SPIELPOSITION_TORMANN)
			return ferg.getAnzahlTore() * Konstanten.PUNKTE_TOR_TORMANN;
		else if (ferg.getSpielposition() == Konstanten.SPIELPOSITION_VERTEIDIGER)
			return ferg.getAnzahlTore() * Konstanten.PUNKTE_TOR_VERTEIDIGER;
		else if (ferg.getSpielposition() == Konstanten.SPIELPOSITION_MITTELFELDSPIELER)
			return ferg.getAnzahlTore() * Konstanten.PUNKTE_TOR_MITTELFELDSPIELER;
		else if (ferg.getSpielposition() == Konstanten.SPIELPOSITION_STUERMER)
			return ferg.getAnzahlTore() * Konstanten.PUNKTE_TOR_STUERMER;

		return 0;
	}

	/**
	 * Ermittelt die Punkte f�r die Note des �bergebenen Fu�ballers
	 */
	private int getPunkteForNote(FussballerErgebnis ferg) {

		if (ferg.getNote() == 1.0)
			return Konstanten.PUNKTE_NOTE_1_0;
		else if (ferg.getNote() == 1.5)
			return Konstanten.PUNKTE_NOTE_1_5;
		else if (ferg.getNote() == 2.0)
			return Konstanten.PUNKTE_NOTE_2_0;
		else if (ferg.getNote() == 2.5)
			return Konstanten.PUNKTE_NOTE_2_5;
		else if (ferg.getNote() == 3.0)
			return Konstanten.PUNKTE_NOTE_3_0;
		else if (ferg.getNote() == 3.5)
			return Konstanten.PUNKTE_NOTE_3_5;
		else if (ferg.getNote() == 4.0)
			return Konstanten.PUNKTE_NOTE_4_0;
		else if (ferg.getNote() == 4.5)
			return Konstanten.PUNKTE_NOTE_4_5;
		else if (ferg.getNote() == 5.0)
			return Konstanten.PUNKTE_NOTE_5_0;
		else if (ferg.getNote() == 5.5)
			return Konstanten.PUNKTE_NOTE_5_5;
		else if (ferg.getNote() == 6.0)
			return Konstanten.PUNKTE_NOTE_6_0;

		return 0;
	}

	/**
	 * Steuert die Erstellung der Ausgabedatei f�r die �bergebenen Fu�ballerErgebnisse
	 */
	private void handleCreateFile(List<FussballerErgebnis> fergs, String strPath, boolean bolPDF, boolean bolOneFile, boolean bolGesamtwertung)
			throws AuswertungException {

		try {
			if (bolPDF) {
				erstelleFile(fergs, strPath, true, true, bolGesamtwertung);
				return;
			}

			if (!bolOneFile) {
				erstelleFile(fergs, strPath, false, false, false);
				return;
			}
			
			erstelleFile(fergs, strPath, true, false, bolGesamtwertung);
		}
		catch (IOException ioe) {
			throw new AuswertungException(ioe.getMessage());
		}
		catch (DBException dbe) {
			dbe.handleError();
			return;
		}
	}

	/**
	 * Erstellt ein Textfile, in das die ermittelten Auswertungsdaten geschrieben werden
	 */
	private void erstelleFile(List<FussballerErgebnis> fergs, String strPath, boolean bolOneFile, boolean bolPDF, boolean bolGesamtwertung)
			throws DBException, IOException {

		String mitspielername = DBRead.getMitspieler(SessionVariablen.getInstance().getMitspielerId());

		if (!bolPDF)
			writeToTextFile(bolOneFile, strPath, mitspielername, fergs);
		else
			writeToPdfFile(bolGesamtwertung, mitspielername, fergs);
	}

	/**
	 * Kontrolliert, ob f�r den aktuellen Mitspieler und aktuellen Spieltag bereits Priorit�ten erfasst wurden.
	 * Wenn nicht, werden die Priorit�ten des letzten Spieltags kopiert.
	 */
	private void checkPrioritaeten() throws DBException {
		
		int anzahl_prio = DBRead.getAnzahlPrioritaeten();
		if (anzahl_prio == 0)
			DBUtil.copyPrioritaeten();
	}

	/**
	 * Erstellt die PDF-Auswertungsdatei.
	 */
	private void createPdf(String strPath, boolean bolOneFile, boolean bolPDF, boolean bolGesamtwertung) throws DocumentException,
			FileNotFoundException, AuswertungException {

		if (bolGesamtwertung)
			return;

		// Dateiname erstellen
		String dateiname = strPath + Konstanten.AUSGABEDATEI + "_" + SessionVariablen.getInstance().getTippspielname() + "_"
				+ SessionVariablen.getInstance().getSpieltag() + ".pdf";

		// Dann ein neues Dokument erzeugen
		Document d = new Document();
		PdfWriter.getInstance(d, new FileOutputStream(dateiname));
		d.open();

		// Die �berschrift erzeugen
		Paragraph p = new Paragraph("Auswertung f�r den " + SessionVariablen.getInstance().getSpieltag() + ". Spieltag.",
				FontFactory.getFont(FontFactory.TIMES_BOLDITALIC, 16));
		p.setAlignment(Element.ALIGN_CENTER);
		d.add(p);
		d.add(Chunk.NEWLINE);

		// Die Tageswertung erzeugen und schreiben
		PdfPTable tTageswertung = new PdfPTable(3);
		tTageswertung.setSpacingAfter(50);

		PdfPCell c = new PdfPCell(new Paragraph("Tageswertung"));
		c.setHorizontalAlignment(Element.ALIGN_CENTER);
		c.setGrayFill(0.95f);
		c.setColspan(3);
		tTageswertung.addCell(c);

		Collections.sort(ergTag, new ErgebnisseSort());

		for (int i = 0; i < ergTag.size(); i++) {
			if (i == 0 || (i > 0 && ergTag.get(i).getErgebnis() < ergTag.get(i - 1).getErgebnis()))
				tTageswertung.addCell("" + (i + 1) + ".");
			else
				tTageswertung.addCell("");

			tTageswertung.addCell(ergTag.get(i).getMitspielername());
			tTageswertung.addCell(ergTag.get(i).getErgebnis().toString());
		}

		d.add(tTageswertung);

		// Gesamtwertung erstellen und schreiben
		erstelleGesamtwertung(strPath, bolPDF);
		PdfPTable tGesamtwertung = new PdfPTable(3);

		PdfPCell c2 = new PdfPCell(new Paragraph("Gesamtwertung"));
		c2.setHorizontalAlignment(Element.ALIGN_CENTER);
		c2.setGrayFill(0.95f);
		c2.setColspan(3);
		tGesamtwertung.addCell(c2);

		Collections.sort(ergGesamt, new ErgebnisseSort());

		for (int i = 0; i < ergGesamt.size(); i++) {
			if (i == 0 || (i > 0 && ergGesamt.get(i).getErgebnis() < ergGesamt.get(i - 1).getErgebnis()))
				tGesamtwertung.addCell("" + (i + 1) + ".");
			else
				tGesamtwertung.addCell("");

			tGesamtwertung.addCell(ergGesamt.get(i).getMitspielername());
			tGesamtwertung.addCell(ergGesamt.get(i).getErgebnis().toString());
		}

		d.add(tGesamtwertung);

		// Einzelauswertungen schreiben
		for (int i = 0; i < arlPDFTables.size();) {
			d.newPage();

			PdfPTable tPage = new PdfPTable(2);
			for (int j = 0; j < 4; j++) {
				if (arlPDFTables.size() > i)
					tPage.addCell(arlPDFTables.get(i));
				else
					tPage.addCell("");
				i++;
			}
			d.add(tPage);
		}

		d.close();
	}

	private void erstelleGesamtwertung(String strPath, boolean bolPDF) throws AuswertungException {

		int intAktSpieltag = SessionVariablen.getInstance().getSpieltag();
		try {
			if (ergGesamt == null)
				ergGesamt = new ArrayList<Ergebniseintrag>();

			for (int i = 0; i < intAktSpieltag; i++) {
				SessionVariablen.getInstance().setSpieltag(i + 1);
				auswertungDurchfuehren(strPath, true, bolPDF, true);
			}

			SessionVariablen.getInstance().setSpieltag(intAktSpieltag);
		}
		catch (DBException dbe) {
			dbe.handleError();
			throw new AuswertungException(dbe.getError());
		}
		catch (Exception e) {
			SessionVariablen.getInstance().setSpieltag(intAktSpieltag);
			throw new AuswertungException(e.getMessage());
		}
	}

	private boolean isNewFile(boolean bolOneFile, String strPath, String mitspielername) {

		if (bolOneFile)
			return true;

		boolean newFile = false;

		String dateiname = strPath + Konstanten.AUSGABEDATEI + "_" + SessionVariablen.getInstance().getTippspielname() + "_"
				+ mitspielername + "_" + SessionVariablen.getInstance().getSpieltag() + ".txt";

		FileInputStream datei_input = null;
		if (!bolOneFile) {

			// Kontrolliere, ob die Datei schon da ist
			try {
				datei_input = new FileInputStream(dateiname);
			}
			catch (FileNotFoundException fnfe) {
				newFile = true;
			}
			finally {
				try {
					datei_input.close();
				}
				catch (IOException e) {
					// do nothing
				}
			}
		}

		return newFile;
	}

	private void writeToTextFile(boolean bolOneFile, String strPath, String mitspielername, List<FussballerErgebnis> fergs)
			throws IOException {

		int anzahl_torhueter = 0;
		int anzahl_verteidiger = 0;
		int anzahl_mittelfeldspieler = 0;
		int anzahl_stuermer = 0;
		int gesamtpunktzahl = 0;

		if (!isNewFile(bolOneFile, strPath, mitspielername)) {
			Util.showMessage("Fehler", "Die Ausgabedatei existiert bereits!");
			return;
		}

		String dateiname;
		if (!bolOneFile) {
			dateiname = strPath + Konstanten.AUSGABEDATEI + "_" + SessionVariablen.getInstance().getTippspielname() + "_" + mitspielername
					+ "_" + SessionVariablen.getInstance().getSpieltag() + ".txt";
		}
		else {
			dateiname = strPath + Konstanten.AUSGABEDATEI + "_" + SessionVariablen.getInstance().getTippspielname() + "_"
					+ SessionVariablen.getInstance().getSpieltag() + ".txt";
		}

		DataOutputStream ausgabedatei = new DataOutputStream(new FileOutputStream(dateiname, true));

		// Spielername
		ausgabedatei.writeBytes("################################################################################\r\n");
		ausgabedatei.writeBytes("\t\t\t" + mitspielername + "\r\n");
		ausgabedatei.writeBytes("################################################################################\r\n\r\n");

		// Position: Torh�ter
		ausgabedatei.writeBytes("PUNKTEVERTEILUNG F�R DIE POSITION: TORH�TER\r\n\r\n");

		// Laufe �ber alle Fu�baller
		for (FussballerErgebnis ferg : fergs) {
			int punkte_spieler = ferg.getPunkteNote() + ferg.getPunkteGelbrot() + ferg.getPunkteRot() + ferg.getPunkteTore();

			if (ferg.getSpielposition() == Konstanten.SPIELPOSITION_TORMANN) {
				ausgabedatei.writeBytes(ferg.getFussballername() + " (" + punkte_spieler + "):\r\n");
				ausgabedatei.writeBytes("Punkte f�r die Note: " + ferg.getPunkteNote() + "\r\n");

				if (ferg.getPunkteGelbrot() != 0)
					ausgabedatei.writeBytes("Punktabzug f�r die Gelbrote Karte: " + ferg.getPunkteGelbrot() + "\r\n");

				if (ferg.getPunkteRot() != 0)
					ausgabedatei.writeBytes("Punktabzug f�r die Rote Karte: " + ferg.getPunkteRot() + "\r\n");

				if (ferg.getPunkteTore() != 0)
					ausgabedatei.writeBytes("Punkte f�r geschossene Tore: " + ferg.getPunkteTore() + "\r\n");

				anzahl_torhueter++;
			}

			ausgabedatei.writeBytes("\r\n");
		}

		// Position: Verteidiger
		ausgabedatei.writeBytes("PUNKTEVERTEILUNG F�R DIE POSITION: VERTEIDIGER\r\n\r\n");

		// Laufe �ber alle Fu�baller
		for (FussballerErgebnis ferg : fergs) {
			int punkte_spieler = ferg.getPunkteNote() + ferg.getPunkteGelbrot() + ferg.getPunkteRot() + ferg.getPunkteTore();

			if (ferg.getSpielposition() == Konstanten.SPIELPOSITION_VERTEIDIGER) {
				ausgabedatei.writeBytes(ferg.getFussballername() + " (" + punkte_spieler + "):\r\n");
				ausgabedatei.writeBytes("Punkte f�r die Note: " + ferg.getPunkteNote() + "\r\n");

				if (ferg.getPunkteGelbrot() != 0)
					ausgabedatei.writeBytes("Punktabzug f�r die Gelbrote Karte: " + ferg.getPunkteGelbrot() + "\r\n");

				if (ferg.getPunkteRot() != 0)
					ausgabedatei.writeBytes("Punktabzug f�r die Rote Karte: " + ferg.getPunkteRot() + "\r\n");

				if (ferg.getPunkteTore() != 0)
					ausgabedatei.writeBytes("Punkte f�r geschossene Tore: " + ferg.getPunkteTore() + "\r\n");

				anzahl_verteidiger++;
			}

			ausgabedatei.writeBytes("\r\n");
		}

		// Position: Mittelfeldspieler
		ausgabedatei.writeBytes("PUNKTEVERTEILUNG F�R DIE POSITION: MITTELFELD\r\n\r\n");

		// Laufe �ber alle Fu�baller
		for (FussballerErgebnis ferg : fergs) {
			int punkte_spieler = ferg.getPunkteNote() + ferg.getPunkteGelbrot() + ferg.getPunkteRot() + ferg.getPunkteTore();

			if (ferg.getSpielposition() == Konstanten.SPIELPOSITION_MITTELFELDSPIELER) {
				ausgabedatei.writeBytes(ferg.getFussballername() + " (" + punkte_spieler + "):\r\n");
				ausgabedatei.writeBytes("Punkte f�r die Note: " + ferg.getPunkteNote() + "\r\n");

				if (ferg.getPunkteGelbrot() != 0)
					ausgabedatei.writeBytes("Punktabzug f�r die Gelbrote Karte: " + ferg.getPunkteGelbrot() + "\r\n");

				if (ferg.getPunkteRot() != 0)
					ausgabedatei.writeBytes("Punktabzug f�r die Rote Karte: " + ferg.getPunkteRot() + "\r\n");

				if (ferg.getPunkteTore() != 0)
					ausgabedatei.writeBytes("Punkte f�r geschossene Tore: " + ferg.getPunkteTore() + "\r\n");

				anzahl_mittelfeldspieler++;
			}

			ausgabedatei.writeBytes("\r\n");
		}

		// Position: St�rmer
		ausgabedatei.writeBytes("PUNKTEVERTEILUNG F�R DIE POSITION: ST�RMER\r\n\r\n");

		// Laufe �ber alle Fu�baller
		for (FussballerErgebnis ferg : fergs) {
			int punkte_spieler = ferg.getPunkteNote() + ferg.getPunkteGelbrot() + ferg.getPunkteRot() + ferg.getPunkteTore();

			if (ferg.getSpielposition() == Konstanten.SPIELPOSITION_STUERMER) {
				ausgabedatei.writeBytes(ferg.getFussballername() + "(" + punkte_spieler + "):\r\n");
				ausgabedatei.writeBytes("Punkte f�r die Note: " + ferg.getPunkteNote() + "\r\n");

				if (ferg.getPunkteGelbrot() != 0)
					ausgabedatei.writeBytes("Punktabzug f�r die Gelbrote Karte: " + ferg.getPunkteGelbrot() + "\r\n");

				if (ferg.getPunkteRot() != 0)
					ausgabedatei.writeBytes("Punktabzug f�r die Rote Karte: " + ferg.getPunkteRot() + "\r\n");

				if (ferg.getPunkteTore() != 0)
					ausgabedatei.writeBytes("Punkte f�r geschossene Tore: " + ferg.getPunkteTore() + "\r\n");

				anzahl_stuermer++;
			}

			ausgabedatei.writeBytes("\r\n");

			gesamtpunktzahl += ferg.getPunkteNote();
			gesamtpunktzahl += ferg.getPunkteGelbrot();
			gesamtpunktzahl += ferg.getPunkteRot();
			gesamtpunktzahl += ferg.getPunkteTore();
		}

		ausgabedatei.writeBytes("\r\n");

		// Ggf. noch Punktabzug f�r nicht besetzte Positionen
		if (anzahl_torhueter < 1) {
			int abzug = ((1 - anzahl_torhueter) * Konstanten.PUNKTE_POSITION_FEHLT);
			ausgabedatei.writeBytes("PUNKTABZUG F�R FEHLENDE BESETZUNG DER POSITION TORH�TER: " + abzug + "\r\n\r\n");
			gesamtpunktzahl += abzug;
		}

		if (anzahl_verteidiger < 3) {
			int abzug = ((3 - anzahl_verteidiger) * Konstanten.PUNKTE_POSITION_FEHLT);
			ausgabedatei.writeBytes("PUNKTABZUG F�R FEHLENDE BESETZUNG DER POSITION VERTEIDIGER: " + abzug + "\r\n\r\n");
			gesamtpunktzahl += abzug;
		}

		if (anzahl_mittelfeldspieler < 5) {
			int abzug = ((5 - anzahl_mittelfeldspieler) * Konstanten.PUNKTE_POSITION_FEHLT);
			ausgabedatei.writeBytes("PUNKTABZUG F�R FEHLENDE BESETZUNG DER POSITION MITTELFELDSPIELER: " + abzug + "\r\n\r\n");
			gesamtpunktzahl += abzug;
		}

		if (anzahl_stuermer < 2) {
			int abzug = ((2 - anzahl_stuermer) * Konstanten.PUNKTE_POSITION_FEHLT);
			ausgabedatei.writeBytes("PUNKTABZUG F�R FEHLENDE BESETZUNG DER POSITION ST�RMER: " + abzug + "\r\n\r\n");
			gesamtpunktzahl += abzug;
		}

		ausgabedatei.writeBytes("\r\n");
		ausgabedatei.writeBytes("DAS ERGIBT EINE GESAMTPUNKTZAHL VON: " + gesamtpunktzahl);
		ausgabedatei.writeBytes("\r\n\r\n");

		if (gesamtpunktzahl > 0) {
			ausgabedatei.writeBytes("Herzlichen Gl�ckwunsch!");
		}
		else {
			ausgabedatei.writeBytes("Das war diesmal wohl nichts...");
		}

		ausgabedatei.writeBytes("\r\n" + "--------------------------------------------------------------------------------" + "\r\n");
		ausgabedatei.writeBytes("--------------------------------------------------------------------------------\r\n\r\n\r\n");
		ausgabedatei.close();
	}

	private void writeToPdfFile(boolean bolGesamtwertung, String mitspielername, List<FussballerErgebnis> fergs) {

		int anzahl_torhueter = 0;
		int anzahl_verteidiger = 0;
		int anzahl_mittelfeldspieler = 0;
		int anzahl_stuermer = 0;
		int gesamtpunktzahl = 0;

		int intFehlt = 0;
		if (arlPDFTables == null)
			arlPDFTables = new ArrayList<PdfPTable>();
		if (ergTag == null)
			ergTag = new ArrayList<Ergebniseintrag>();

		Hashtable<String, Integer> htTore = new Hashtable<String, Integer>();
		ArrayList<String> arRot = new ArrayList<String>();
		ArrayList<String> arGelbRot = new ArrayList<String>();

		PdfPTable t = new PdfPTable(2);
		t.setSpacingAfter(30);
		t.setSpacingBefore(30);

		PdfPCell c = new PdfPCell(new Paragraph(mitspielername));
		c.setHorizontalAlignment(Element.ALIGN_CENTER);
		c.setGrayFill(0.95f);
		c.setColspan(2);
		t.addCell(c);

		int intPos = 0;

		for (int j = 0; j < 4; j++) {
			if (j == 0)
				intPos = Konstanten.SPIELPOSITION_TORMANN;
			if (j == 1)
				intPos = Konstanten.SPIELPOSITION_VERTEIDIGER;
			if (j == 2)
				intPos = Konstanten.SPIELPOSITION_MITTELFELDSPIELER;
			if (j == 3)
				intPos = Konstanten.SPIELPOSITION_STUERMER;

			// Laufe �ber alle Fu�baller
			for (FussballerErgebnis ferg : fergs) {
				if (ferg.getSpielposition() == intPos) {
					t.addCell(ferg.getFussballername());
					t.addCell(new Integer(ferg.getPunkteNote()).toString());

					if (ferg.getPunkteGelbrot() != 0)
						arGelbRot.add(ferg.getFussballername());

					if (ferg.getPunkteRot() != 0)
						arRot.add(ferg.getFussballername());

					if (ferg.getPunkteTore() != 0)
						htTore.put(ferg.getFussballername(), ferg.getPunkteTore());

					if (j == 0)
						anzahl_torhueter++;
					if (j == 1)
						anzahl_verteidiger++;
					if (j == 2)
						anzahl_mittelfeldspieler++;
					if (j == 3)
						anzahl_stuermer++;
				}
				
				if (j == 3) {
					gesamtpunktzahl += ferg.getPunkteNote();
					gesamtpunktzahl += ferg.getPunkteGelbrot();
					gesamtpunktzahl += ferg.getPunkteRot();
					gesamtpunktzahl += ferg.getPunkteTore();
				}
			}
		}
		
		// Ggf. noch Punktabzug f�r nicht besetzte Positionen
		if (anzahl_torhueter < 1) {
			int abzug = ((1 - anzahl_torhueter) * Konstanten.PUNKTE_POSITION_FEHLT);
			intFehlt = intFehlt + (1 - anzahl_torhueter);
			gesamtpunktzahl += abzug;
		}

		if (anzahl_verteidiger < 3) {
			int abzug = ((3 - anzahl_verteidiger) * Konstanten.PUNKTE_POSITION_FEHLT);
			intFehlt = intFehlt + (3 - anzahl_verteidiger);
			gesamtpunktzahl += abzug;
		}

		if (anzahl_mittelfeldspieler < 5) {
			int abzug = ((5 - anzahl_mittelfeldspieler) * Konstanten.PUNKTE_POSITION_FEHLT);
			intFehlt = intFehlt + (5 - anzahl_mittelfeldspieler);
			gesamtpunktzahl += abzug;
		}

		if (anzahl_stuermer < 2) {
			int abzug = ((2 - anzahl_stuermer) * Konstanten.PUNKTE_POSITION_FEHLT);
			intFehlt = intFehlt + (2 - anzahl_stuermer);
			gesamtpunktzahl += abzug;
		}

		PdfPCell cLeer = new PdfPCell(new Paragraph(""));
		cLeer.setColspan(2);
		t.addCell(cLeer);

		Paragraph p = new Paragraph("Es fehlen: " + new Integer(Math.abs(intFehlt)).toString() + " Spieler, Abzug "
				+ new Integer(-1 * Math.abs(intFehlt * Konstanten.PUNKTE_POSITION_FEHLT)).toString());
		PdfPCell cFehlt = new PdfPCell(p);
		cFehlt.setColspan(2);
		t.addCell(cFehlt);

		String s = "Tore: ";

		Enumeration<String> e = htTore.keys();
		while (e.hasMoreElements()) {
			String sSchuetze;
			Integer intTore;
			sSchuetze = e.nextElement().toString();
			intTore = htTore.get(sSchuetze);
			s = s + sSchuetze + " +" + intTore.toString() + "   ";
		}
		PdfPCell cTore = new PdfPCell(new Paragraph(s));
		cTore.setColspan(2);
		t.addCell(cTore);

		s = "Gelb-Rot: ";

		for (int j = 0; j < arGelbRot.size(); j++)
			s = s + arGelbRot.get(j) + " " + new Integer(Konstanten.PUNKTE_GELBROTE_KARTE).toString() + "   ";
		PdfPCell cGelbRot = new PdfPCell(new Paragraph(s));
		cGelbRot.setColspan(2);
		t.addCell(cGelbRot);

		s = "Rot: ";
		for (int j = 0; j < arRot.size(); j++)
			s = s + arRot.get(j) + " " + new Integer(Konstanten.PUNKTE_ROTE_KARTE).toString() + "   ";
		PdfPCell cRot = new PdfPCell(new Paragraph(s));
		cRot.setColspan(2);
		t.addCell(cRot);

		s = "Gesamtpunktzahl: " + new Integer(gesamtpunktzahl).toString();
		PdfPCell cErg = new PdfPCell(new Paragraph(s));
		cErg.setHorizontalAlignment(Element.ALIGN_RIGHT);
		cErg.setGrayFill(0.8f);
		cErg.setColspan(2);
		t.addCell(cErg);

		if (bolGesamtwertung) {

			t = null;

			boolean bolExists = false;

			for (int i = 0; i < ergGesamt.size(); i++) {
				if (ergGesamt.get(i).getMitspielername().equals(mitspielername)) {
					ergGesamt.get(i).setErgebnis(ergGesamt.get(i).getErgebnis() + gesamtpunktzahl);
					bolExists = true;
					break;
				}
			}
			if (!bolExists) {
				ergGesamt.add(new Ergebniseintrag(mitspielername, gesamtpunktzahl));
			}
		}
		else {
			arlPDFTables.add(t);
			ergTag.add(new Ergebniseintrag(mitspielername, gesamtpunktzahl));
		}
	}
}
