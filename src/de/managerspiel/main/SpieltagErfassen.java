package de.managerspiel.main;

import java.awt.Dimension;
import java.awt.Toolkit;
import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.net.MalformedURLException;
import java.net.URL;
import java.net.URLConnection;
import java.util.ArrayList;
import java.util.List;

import org.eclipse.swt.SWT;
import org.eclipse.swt.events.FocusEvent;
import org.eclipse.swt.events.FocusListener;
import org.eclipse.swt.events.SelectionEvent;
import org.eclipse.swt.events.SelectionListener;
import org.eclipse.swt.layout.GridData;
import org.eclipse.swt.layout.GridLayout;
import org.eclipse.swt.widgets.Button;
import org.eclipse.swt.widgets.Display;
import org.eclipse.swt.widgets.Shell;

import de.managerspiel.db.DBDelete;
import de.managerspiel.db.DBInsert;
import de.managerspiel.db.DBRead;
import de.managerspiel.exception.DBException;
import de.managerspiel.model.Auswertungseintrag;
import de.managerspiel.util.KickerKonst;
import de.managerspiel.util.Konstanten;
import de.managerspiel.util.SessionVariablen;
import de.managerspiel.util.SpieltagErfassenMaske;
import de.managerspiel.util.Util;

public class SpieltagErfassen {

	List<Auswertungseintrag> auswEintraege;

	/**
	 * Erstellt nach dem erfogreichen Laden das Auswertungsfenster
	 */
	public void createWindowMain(Display display) {

		final Shell shellAuswertung = Util.createShell(display, "Spieltag erfassen", Konstanten.WINDOW_AUSWERTUNG_BREITE,
				Konstanten.WINDOW_AUSWERTUNG_HOEHE);

		// Datenfelder erzeugen
		final SpieltagErfassenMaske maske = new SpieltagErfassenMaske(shellAuswertung);

		// Layout setzen
		GridLayout gridlAuswertung = new GridLayout();
		gridlAuswertung.horizontalSpacing = 20;
		gridlAuswertung.verticalSpacing = 10;
		gridlAuswertung.marginHeight = 20;
		gridlAuswertung.marginWidth = 20;
		gridlAuswertung.numColumns = 11;
		gridlAuswertung.makeColumnsEqualWidth = false;
		shellAuswertung.setLayout(gridlAuswertung);

		// Eingabefeld f�r URL
		(maske.getLbl_url()).setText("URL:");
		GridData grid_url = new GridData(GridData.HORIZONTAL_ALIGN_FILL);
		grid_url.horizontalSpan = 8;
		(maske.getTxt_url()).setLayoutData(grid_url);

		// Bei Klick in das Feld den bestehenden Eintrag l�schen
		(maske.getTxt_url()).addFocusListener(new FocusListener() {

			public void focusGained(FocusEvent arg0) {
				(maske.getTxt_url()).setText("");
			}

			public void focusLost(FocusEvent arg0) {}
		});

		// Auslesen-Button
		(maske.getButton_auslesen()).setText("Auslesen");
		GridData grid_button = new GridData(GridData.HORIZONTAL_ALIGN_FILL);
		grid_button.horizontalSpan = 2;
		(maske.getButton_auslesen()).setLayoutData(grid_button);

		(maske.getButton_auslesen()).addSelectionListener(new SelectionListener() {

			public void widgetSelected(SelectionEvent arg0) {
				// Keine URL eingetragen
				if ((maske.getTxt_url()).getText().equals("")) {
					Util.showMessage("Fehler", "Bitte geben Sie eine korrekte URL ein!");
					return;
				}

				try {
					URL url = new URL((maske.getTxt_url()).getText());
					URLConnection conn = url.openConnection();
					conn.setDoInput(true);
					InputStream in = conn.getInputStream();

					String strInput = "";

					BufferedReader br = new BufferedReader(new InputStreamReader(in));
					StringBuilder sb = new StringBuilder();
					String line = null;

					while ((line = br.readLine()) != null)
						sb.append(line + "\n");

					br.close();
					strInput = sb.toString();

					// F�r den Vergleich gro� schreiben
					strInput = strInput.toUpperCase();

					fuelleNoten(strInput, maske);
					fuelleTore(strInput, maske);
					fuelleGelbRot(strInput, maske);
					fuelleRot(strInput, maske);

					Util.showMessage("Verarbeitung fertig", "Alle verf�gbaren Informationen wurden eingelesen.");
				}
				catch (MalformedURLException e) {
					Util.showMessage("Fehler", "Die URL konnte nicht aufgel�st werden!");
					return;
				}
				catch (IOException e) {
					Util.showMessage("Fehler", "Die Verbindung ins Internet konnte nicht hergestellt werden!");
					return;
				}
			}

			public void widgetDefaultSelected(SelectionEvent arg0) {
				;
			}
		});

		// Vereine lesen
		List<String> vereine;
		try {
			vereine = DBRead.getVereine();
		}
		catch (DBException e) {
			e.handleError();
			return;
		}

		// Combobox f�llen
		for (String verein : vereine) {
			maske.getCom1().add(verein);
		}

		// Ersten Eintrag selektieren
		if ((maske.getCom1()).getItemCount() > 0) {

			try {
				String verein = (maske.getCom1()).getItem(0);
				SessionVariablen.getInstance().setVereinId(DBRead.getVereinID(verein));
				(maske.getCom1()).select(0);
			}
			catch (DBException e) {
				// do nothing, no preselection
			}

			try {
				fillAuswertungseintraege();
				fill_Maske(shellAuswertung, maske);
			}
			catch (DBException e) {
				e.handleError();
				return;
			}
		}

		(maske.getCom1()).addSelectionListener(new SelectionListener() {

			public void widgetSelected(SelectionEvent arg0) {
				// Daten speichern
				try {
					save_aktuellen_Eintrag(maske);
				}
				catch (DBException e1) {
					e1.handleError();
					return;
				}

				// Aktuellen Verein setzen
				try {
					SessionVariablen.getInstance().setVereinId(
							DBRead.getVereinID((maske.getCom1()).getItem((maske.getCom1()).getSelectionIndex())));
				}
				catch (DBException e) {
					e.handleError();
					return;
				}

				// Liste neu f�llen
				hide_Maske(shellAuswertung, maske);

				try {
					fillAuswertungseintraege();
					fill_Maske(shellAuswertung, maske);
				}
				catch (DBException e) {
					e.handleError();
					return;
				}

				shellAuswertung.redraw();
			}

			public void widgetDefaultSelected(SelectionEvent arg0) {
				;
			}
		});

		// Beenden-Button
		Button buttonSchliessen = new Button(shellAuswertung, SWT.FULL_SELECTION);
		buttonSchliessen.setText("Beenden");
		GridData gridSchliessen = new GridData(GridData.HORIZONTAL_ALIGN_FILL);
		gridSchliessen.horizontalSpan = 2;
		buttonSchliessen.setLayoutData(gridSchliessen);
		buttonSchliessen.addSelectionListener(new SelectionListener() {

			public void widgetSelected(SelectionEvent arg0) {
				Util.showMessage("Speichern", "Die eben erfassten Daten werden gespeichert.");

				try {
					save_aktuellen_Eintrag(maske);
				}
				catch (DBException e) {
					e.handleError();
				}

				shellAuswertung.close();
				(SessionVariablen.getInstance().getShell()).setVisible(true);
			}

			public void widgetDefaultSelected(SelectionEvent arg0) {
				;
			}
		});

		// Abbrechen-Button
		Button buttonAbbrechen = new Button(shellAuswertung, SWT.FULL_SELECTION);
		buttonAbbrechen.setText("Abbrechen");
		GridData gridAbbrechen = new GridData(GridData.HORIZONTAL_ALIGN_FILL);
		gridAbbrechen.horizontalSpan = 2;
		buttonAbbrechen.setLayoutData(gridAbbrechen);
		buttonAbbrechen.addSelectionListener(new SelectionListener() {

			public void widgetSelected(SelectionEvent arg0) {
				shellAuswertung.close();
				(SessionVariablen.getInstance().getShell()).setVisible(true);
			}

			public void widgetDefaultSelected(SelectionEvent arg0) {
				;
			}
		});

		// Position des Fensters festlegen
		Dimension d = Toolkit.getDefaultToolkit().getScreenSize();
		shellAuswertung.setLocation((d.width - Konstanten.WINDOW_AUSWERTUNG_BREITE) / 2,
				(d.height - Konstanten.WINDOW_AUSWERTUNG_HOEHE) / 2);

		// Fenster jetzt anzeigen
		(SessionVariablen.getInstance().getShell()).setVisible(false);
		shellAuswertung.open();
	}

	/**
	 * F�llt die Objekte Auswertungseintraege mit den Daten zum aktuellen Verein
	 */
	private void fillAuswertungseintraege() throws DBException {
		
		auswEintraege = new ArrayList<Auswertungseintrag>();

		// Fu�ballername zum Verein lesen
		List<String> fussballerList = DBRead.getAlleFussballerVerein();

		for (String fussballer : fussballerList) {
			Auswertungseintrag ausw = new Auswertungseintrag();
			
			ausw.setFussballername(fussballer);
			ausw.setFussballer_id(DBRead.getFussballerID(fussballer));
			ausw.setGespielt(false);
			ausw.setAnzahl_tore(0);
			ausw.setGelbrote_karte(false);
			ausw.setRote_karte(false);
			ausw.setNote(0.0);
			
			auswEintraege.add(ausw);
		}
	}

	/**
	 * F�llt die Maske mit den aktuellen Eintr�gen
	 */
	private void fill_Maske(Shell shell_auswertung, SpieltagErfassenMaske maske) throws DBException {

		List<String> fussballerList = DBRead.getAlleFussballerVerein();
		hideAllMaskComponents(maske);

		for (int i = 0; i < fussballerList.size(); i++) {
			maske.getLbl_name()[i].setVisible(true);
			maske.getLbl_gespielt()[i].setVisible(true);
			maske.getButt_gespielt()[i].setVisible(true);
			maske.getButt_gespielt()[i].setSelection(true);
			maske.getLbl_tore()[i].setVisible(true);
			maske.getCmb_tore()[i].setVisible(true);
			maske.getCmb_tore()[i].select(0);
			maske.getLbl_gelbrot()[i].setVisible(true);
			maske.getButt_gelbrot()[i].setVisible(true);
			maske.getButt_gelbrot()[i].setSelection(false);
			maske.getLbl_rot()[i].setVisible(true);
			maske.getButt_rot()[i].setVisible(true);
			maske.getButt_rot()[i].setSelection(false);
			maske.getLbl_note()[i].setVisible(true);
			maske.getCmb_note()[i].setVisible(true);
			maske.getCmb_note()[i].select(5);
		}

		// Elemente f�llen
		for (int i = 0; i < fussballerList.size(); i++) {

			Auswertungseintrag ausw = auswEintraege.get(i);
			
			maske.getLbl_name()[i].setText(ausw.getFussballername());

			// Mit blanks auff�llen, damit die Gr��e des Labels passt
			while (maske.getLbl_name()[i].getText().length() < 50)
				maske.getLbl_name()[i].setText(maske.getLbl_name()[i].getText() + " ");

			DBRead.setPunktekriterien(ausw);

			maske.getButt_gespielt()[i].setSelection(ausw.isGespielt());

			// ggf. Felder verstecken
			if (maske.getButt_gespielt()[i].getSelection() == false) {
				maske.getLbl_tore()[i].setVisible(false);
				maske.getCmb_tore()[i].setVisible(false);
				maske.getLbl_gelbrot()[i].setVisible(false);
				maske.getButt_gelbrot()[i].setVisible(false);
				maske.getLbl_rot()[i].setVisible(false);
				maske.getButt_rot()[i].setVisible(false);
				maske.getLbl_note()[i].setVisible(false);
				maske.getCmb_note()[i].setVisible(false);
			}

			// ACHTUNG: Die folgende Zeile funktioniert so nur, weil Inhalt=SelectionIndex
			maske.getCmb_tore()[i].select(ausw.getAnzahl_tore());
			maske.getButt_gelbrot()[i].setSelection(ausw.isGelbrote_karte());

			// ggf. Felder verstecken
			if (maske.getButt_gelbrot()[i].getSelection() == true) {
				maske.getLbl_rot()[i].setVisible(false);
				maske.getButt_rot()[i].setVisible(false);
			}

			maske.getButt_rot()[i].setSelection(ausw.isRote_karte());

			// ggf. Felder verstecken
			if (maske.getButt_rot()[i].getSelection() == true) {
				maske.getLbl_gelbrot()[i].setVisible(false);
				maske.getButt_gelbrot()[i].setVisible(false);
			}

			if (ausw.getNote() == 1.0)
				maske.getCmb_note()[i].select(0);
			else if (ausw.getNote() == 1.5)
				maske.getCmb_note()[i].select(1);
			else if (ausw.getNote() == 2.0)
				maske.getCmb_note()[i].select(2);
			else if (ausw.getNote() == 2.5)
				maske.getCmb_note()[i].select(3);
			else if (ausw.getNote() == 3.0)
				maske.getCmb_note()[i].select(4);
			else if (ausw.getNote() == 3.5)
				maske.getCmb_note()[i].select(5);
			else if (ausw.getNote() == 4.0)
				maske.getCmb_note()[i].select(6);
			else if (ausw.getNote() == 4.5)
				maske.getCmb_note()[i].select(7);
			else if (ausw.getNote() == 5.0)
				maske.getCmb_note()[i].select(8);
			else if (ausw.getNote() == 5.5)
				maske.getCmb_note()[i].select(9);
			else if (ausw.getNote() == 6.0)
				maske.getCmb_note()[i].select(10);
		}

		fuelle_selectionListeners(maske);
	}

	private void hideAllMaskComponents(SpieltagErfassenMaske maske) {

		// Initialisierungen
		for (int i = 0; i < Konstanten.MAX_EINTRAEGE_AUSWERTUNG; i++) {
			(maske.getLbl_name()[i]).setVisible(false);
			(maske.getLbl_gespielt()[i]).setVisible(false);
			(maske.getButt_gespielt()[i]).setVisible(false);
			(maske.getLbl_tore()[i]).setVisible(false);
			(maske.getCmb_tore()[i]).setVisible(false);
			(maske.getLbl_gelbrot())[i].setVisible(false);
			(maske.getButt_gelbrot()[i]).setVisible(false);
			(maske.getLbl_rot()[i]).setVisible(false);
			(maske.getButt_rot()[i]).setVisible(false);
			(maske.getLbl_note()[i]).setVisible(false);
			(maske.getCmb_note()[i]).setVisible(false);
		}
	}

	/**
	 * F�gt die Selection-Listeners auf die Buttons "gespielt"
	 */
	private void fuelle_selectionListeners(final SpieltagErfassenMaske maske) throws DBException {

		List<String> fussballerList = DBRead.getAlleFussballerVerein();

		for (int i = 0; i < fussballerList.size(); i++) {

			final int a = i;

			maske.getButt_gespielt()[i].addSelectionListener(new SelectionListener() {

				public void widgetSelected(SelectionEvent arg0) {

					if (maske.getButt_gespielt()[a].getSelection() == false) {
						maske.getLbl_tore()[a].setVisible(false);
						maske.getCmb_tore()[a].setVisible(false);
						maske.getLbl_gelbrot()[a].setVisible(false);
						maske.getButt_gelbrot()[a].setVisible(false);
						maske.getLbl_rot()[a].setVisible(false);
						maske.getButt_rot()[a].setVisible(false);
						maske.getLbl_note()[a].setVisible(false);
						maske.getCmb_note()[a].setVisible(false);
					}
					else {
						maske.getLbl_tore()[a].setVisible(true);
						maske.getCmb_tore()[a].setVisible(true);

						if (maske.getButt_rot()[a].getSelection() == false) {
							maske.getLbl_gelbrot()[a].setVisible(true);
							maske.getButt_gelbrot()[a].setVisible(true);
						}

						if (maske.getButt_gelbrot()[a].getSelection() == false) {
							maske.getLbl_rot()[a].setVisible(true);
							maske.getButt_rot()[a].setVisible(true);
						}

						maske.getLbl_note()[a].setVisible(true);
						maske.getCmb_note()[a].setVisible(true);

						maske.getCmb_note()[a].setFocus();
					}
				}

				public void widgetDefaultSelected(SelectionEvent arg0) {}
			});

			maske.getButt_gelbrot()[i].addSelectionListener(new SelectionListener() {

				public void widgetSelected(SelectionEvent arg0) {
					if (maske.getButt_gelbrot()[a].getSelection() == true) {
						maske.getLbl_rot()[a].setVisible(false);
						maske.getButt_rot()[a].setVisible(false);
					}
					else {
						maske.getLbl_rot()[a].setVisible(true);
						maske.getButt_rot()[a].setVisible(true);
					}
				}

				public void widgetDefaultSelected(SelectionEvent arg0) {}
			});

			maske.getButt_rot()[i].addSelectionListener(new SelectionListener() {

				public void widgetSelected(SelectionEvent arg0) {
					if (maske.getButt_rot()[a].getSelection() == true) {
						maske.getLbl_gelbrot()[a].setVisible(false);
						maske.getButt_gelbrot()[a].setVisible(false);
					}
					else {
						maske.getLbl_gelbrot()[a].setVisible(true);
						maske.getButt_gelbrot()[a].setVisible(true);
					}
				}

				public void widgetDefaultSelected(SelectionEvent arg0) {}
			});
		}
	}

	/**
	 * Versteckt die Felder auf der Maske
	 */
	private void hide_Maske(Shell shell_auswertung, SpieltagErfassenMaske maske) {
		// Initialisierungen
		for (int i = 0; i < Konstanten.MAX_EINTRAEGE_AUSWERTUNG; i++) {
			// Name des Spielers
			(maske.getLbl_name()[i]).setVisible(false);

			// Kennzeichen "gespielt"
			(maske.getLbl_gespielt()[i]).setVisible(false);
			(maske.getButt_gespielt()[i]).setVisible(false);

			// Anzahl Tore
			(maske.getLbl_tore()[i]).setVisible(false);
			(maske.getCmb_tore()[i]).setVisible(false);

			// Kennzeichen "gelb-rote Karte"
			(maske.getLbl_gelbrot()[i]).setVisible(false);
			(maske.getButt_gelbrot()[i]).setVisible(false);

			// Kennzeichen "rote Karte"
			(maske.getLbl_rot()[i]).setVisible(false);
			(maske.getButt_rot()[i]).setVisible(false);

			// Note
			(maske.getLbl_note()[i]).setVisible(false);
			(maske.getCmb_note()[i]).setVisible(false);
		}
	}

	/**
	 * Speichert die Daten zum akuellen Verein
	 */
	private void save_aktuellen_Eintrag(SpieltagErfassenMaske maske) throws DBException {
		
		// L�sche zun�chst alle Eintr�ge zu diesem Verein an diesem Spieltag
		DBDelete.deleteTable_Punktekriterien();

		List<String> fussballerList = DBRead.getAlleFussballerVerein();
		
		// Hole die Werte aus der Maske zur�ck
		for (int i = 0; i < fussballerList.size(); i++) {
			
			Auswertungseintrag ausw = auswEintraege.get(i);
			
			ausw.setGespielt((maske.getButt_gespielt()[i]).getSelection());

			// ACHTUNG: funktioniert nur, weil zuf�llig selectionIndex = Inhalt!
			ausw.setAnzahl_tore((maske.getCmb_tore()[i]).getSelectionIndex());
			ausw.setGelbrote_karte((maske.getButt_gelbrot()[i]).getSelection());
			ausw.setRote_karte((maske.getButt_rot()[i]).getSelection());

			int notenSchluessel = (maske.getCmb_note()[i]).getSelectionIndex();
			if (notenSchluessel == 0)
				ausw.setNote(1.0);
			else if (notenSchluessel == 1)
				ausw.setNote(1.5);
			else if (notenSchluessel == 2)
				ausw.setNote(2.0);
			else if (notenSchluessel == 3)
				ausw.setNote(2.5);
			else if (notenSchluessel == 4)
				ausw.setNote(3.0);
			else if (notenSchluessel == 5)
				ausw.setNote(3.5);
			else if (notenSchluessel == 6)
				ausw.setNote(4.0);
			else if (notenSchluessel == 7)
				ausw.setNote(4.5);
			else if (notenSchluessel == 8)
				ausw.setNote(5.0);
			else if (notenSchluessel == 9)
				ausw.setNote(5.5);
			else if (notenSchluessel == 10)
				ausw.setNote(6.0);
		}

		// Jetzt die aktuellen Eintr�ge speichern
		for (int i = 0; i < fussballerList.size(); i++) {
			Auswertungseintrag ausw = auswEintraege.get(i);
			DBInsert.insertTable_Punktekriterien(ausw);
		}
	}

	/**
	 * Diese Methode f�llt die Noten und die Kennzeichen, ob ein Spieler
	 * gespielt hat
	 */
	private void fuelleNoten(String seite, SpieltagErfassenMaske maske) {
		
		// Zun�chst den relevanten Teil der Seite holen
		String relevanteSeite = "";

		// Die Noten des Vereins stehen nach diesem String: <th class="first alignleft">
		// Aufpassen, dass es auch der richtige Verein ist!
		String aktVerein = (maske.getCom1().getText()).toUpperCase();
		int anfang = seite.indexOf(KickerKonst.SEITENSTART + aktVerein);
		int ende = seite.indexOf(KickerKonst.SEITENENDE, anfang + 28);

		if (anfang == -1 || ende == -1) {
			Util.showMessage("Fehler", "Die Notenerfassung konnte nicht durchgef�hrt werden,\r\n" + "da der Verein " + aktVerein
					+ " nicht gefunden werden konnte.");
			return;
		}

		relevanteSeite = seite.substring(anfang, ende);

		// Das Kriterium f�r das Spielen eines Fu�ballers ist:
		// "NAME</a>&nbsp;(" - Nach der Klammer kommt dann die Note (1- oder 3stellig)
		for (int i = 0; i < Konstanten.MAX_EINTRAEGE_AUSWERTUNG; i++) {
			
			// nur, wenn der Fu�baller zur aktuellen Mannschaft geh�rt
			if ((maske.getLbl_name()[i]).isVisible() == false)
				continue;

			String fussballer = (maske.getLbl_name()[i]).getText().trim();
			fussballer = fussballer.toUpperCase();
			// Blank durch "&nbsp;" ersetzen
			fussballer = fussballer.replaceAll(" ", KickerKonst.BLANK);

			// Wenn keiner mehr da ist -> abbrechen
			if (fussballer == null || "".equals(fussballer))
				break;

			// nachschauen, ob der Fu�baller vorkommt
			// vgl. mit "<", der Spieler "Ba" kommt �fter vor (z.B. im Wort "Fu�ball"
			int vorkommenStart = relevanteSeite.indexOf(fussballer + "<");

			if (vorkommenStart == -1)
				continue;

			// Hole die 6 Zeichen (= "</A> (" ) nach dem Namen
			int startIndex = vorkommenStart + fussballer.length();
			int endIndex = startIndex + 6;
			String nachName = relevanteSeite.substring(startIndex, endIndex);

			// Kontrolliere, ob das Kriterium erf�llt ist
			if (!KickerKonst.NACH_FUSSBALLER_NAME.equals(nachName))
				continue;

			// Spieler hat gespielt
			maske.getButt_gespielt()[i].setSelection(true);
			maske.getLbl_note()[i].setVisible(true);
			maske.getCmb_note()[i].setVisible(true);
			maske.getLbl_tore()[i].setVisible(true);
			maske.getCmb_tore()[i].setVisible(true);
			
			if (maske.getButt_rot()[i].getSelection() == false) {
				maske.getLbl_gelbrot()[i].setVisible(true);
				maske.getButt_gelbrot()[i].setVisible(true);
			}
			
			if (maske.getButt_gelbrot()[i].getSelection() == false) {
				maske.getLbl_rot()[i].setVisible(true);
				maske.getButt_rot()[i].setVisible(true);
			}

			// Hole die Note
			startIndex = vorkommenStart + fussballer.length() + 6;
			endIndex = startIndex + 3;
			String vorlaeufigeNote = relevanteSeite.substring(startIndex, endIndex);

			// Kontrolliere, ob Note einstellig ist
			String klammer = vorlaeufigeNote.substring(1, 2);

			// Note setzen
			if (")".equals(klammer)) {
				String einstelligeNote = vorlaeufigeNote.substring(0, 1);

				if ("1".equals(einstelligeNote))
					maske.getCmb_note()[i].select(0);
				else if ("2".equals(einstelligeNote))
					maske.getCmb_note()[i].select(2);
				else if ("3".equals(einstelligeNote))
					maske.getCmb_note()[i].select(4);
				else if ("4".equals(einstelligeNote))
					maske.getCmb_note()[i].select(6);
				else if ("5".equals(einstelligeNote))
					maske.getCmb_note()[i].select(8);
				else if ("6".equals(einstelligeNote))
					maske.getCmb_note()[i].select(10);
			}
			else {
				String dreistelligeNote = vorlaeufigeNote.substring(0, 3);

				if ("1,5".equals(dreistelligeNote))
					maske.getCmb_note()[i].select(1);
				else if ("2,5".equals(dreistelligeNote))
					maske.getCmb_note()[i].select(3);
				else if ("3,5".equals(dreistelligeNote))
					maske.getCmb_note()[i].select(5);
				else if ("4,5".equals(dreistelligeNote))
					maske.getCmb_note()[i].select(7);
				else if ("5,5".equals(dreistelligeNote))
					maske.getCmb_note()[i].select(9);
			}
		}
	}

	/**
	 * Diese Methode f�llt die Tore der Spieler
	 */
	private void fuelleTore(String seite, SpieltagErfassenMaske maske) {
		
		// Zun�chst den relevanten Teil der Seite holen
		String relevanteSeite = "";

		// Die Tore stehen zwischen diesen beiden Strings
		int anfang = seite.indexOf(KickerKonst.TORSCHUETZE_ANFANG);

		// Hole den End-String
		int ende = seite.indexOf(KickerKonst.ROTE_KARTEN_ANFANG);

		// Wenn nicht, dann gehe bis zu den gelb-roten Karten
		if (ende == -1)
			ende = seite.indexOf(KickerKonst.GELBROTE_KARTEN_ANFANG);
		// Wenn nicht, dann gehe bis zu den gelben Karten
		if (ende == -1)
			ende = seite.indexOf(KickerKonst.GELBE_KARTEN_ANFANG);
		// Wenn nicht, dann gehe bis Ansto�
		if (ende == -1)
			ende = seite.indexOf(KickerKonst.ANSTOSS_ANFANG);

		if (anfang == -1 || ende == -1) {
			Util.showMessage("Fehler", "Die Notenerfassung konnte nicht durchgef�hrt werden (Fehler 002).");
			return;
		}

		relevanteSeite = seite.substring(anfang, ende);

		// Das Kriterium f�r das Tor eines Fu�ballers ist:
		// "NAME</A> ("
		for (int i = 0; i < Konstanten.MAX_EINTRAEGE_AUSWERTUNG; i++) {
			// nur, wenn der Fu�baller zur aktuellen Mannschaft geh�rt
			if ((maske.getLbl_name()[i]).isVisible() == false)
				continue;

			String fussballer = (maske.getLbl_name()[i]).getText().trim();
			fussballer = fussballer.toUpperCase();
			// Blank durch "&nbsp;" ersetzen
			fussballer = fussballer.replaceAll(" ", KickerKonst.BLANK);

			// Wenn keiner mehr da ist -> abbrechen
			if (fussballer == null || "".equals(fussballer))
				break;

			// Der Fu�baller kann mehrfach vorkommen
			int anzahlTore = 0;
			String relevanteSeiteTemp = relevanteSeite;
			int vorkommenStart = relevanteSeiteTemp.indexOf(fussballer + "</A>");
			while (vorkommenStart != -1) {
				// Hole die 6 Zeichen (= "</A> (" ) nach dem Namen
				int startIndex = vorkommenStart + fussballer.length();
				int endIndex = startIndex + 6;
				String nachName = relevanteSeiteTemp.substring(startIndex, endIndex);

				// Kontrolliere, ob das Kriterium erf�llt ist
				if (KickerKonst.NACH_FUSSBALLER_NAME.equals(nachName) || KickerKonst.NACH_FUSSBALLER_NAME_2.equals(nachName)) {
					// Fu�baller hat ein Tor geschossen
					// Kontrolliere jetzt, ob es ein Eigentor war!
					String eigentor = relevanteSeiteTemp.substring(startIndex, startIndex + 30);

					if (eigentor.indexOf(KickerKonst.EIGENTOR) == -1)
						anzahlTore++;
				}

				// Seite k�rzen
				relevanteSeiteTemp = relevanteSeiteTemp.substring(endIndex);

				// wieder nachschauen, ob der Fu�baller vorkommt
				vorkommenStart = relevanteSeiteTemp.indexOf(fussballer);
			}

			// Anzahl der Tore setzen
			(maske.getCmb_tore()[i]).select(anzahlTore);
		}
	}

	/**
	 * Diese Methode f�llt die Gelb-Roten Karten der Spieler
	 */
	private void fuelleGelbRot(String seite, SpieltagErfassenMaske maske) {
		// Zun�chst den relevanten Teil der Seite holen
		String relevanteSeite = "";

		// Die Noten stehen zwischen diesen beiden Strings
		int anfang = seite.indexOf(KickerKonst.GELBROTE_KARTEN_ANFANG);
		// Wenn es keine gelb-rote Karten gegeben hat, zur�ck
		if (anfang == -1)
			return;

		// Hole Ende des Strings
		int ende = seite.indexOf(KickerKonst.GELBE_KARTEN_ANFANG);
		// Wenn nicht, dann bis zu "Ansto�"
		if (ende == -1)
			ende = seite.indexOf(KickerKonst.ANSTOSS_ANFANG);

		relevanteSeite = seite.substring(anfang, ende);

		// Das Kriterium f�r eine gelb-rote Karte ist das Vorhandensein im relevanten Teil der Seite
		for (int i = 0; i < Konstanten.MAX_EINTRAEGE_AUSWERTUNG; i++) {
			// nur, wenn der Fu�baller zur aktuellen Mannschaft geh�rt
			if ((maske.getLbl_name()[i]).isVisible() == false)
				continue;

			String fussballer = (maske.getLbl_name()[i]).getText().trim();
			fussballer = fussballer.toUpperCase();
			// Blank durch "&nbsp;" ersetzen
			fussballer = fussballer.replaceAll(" ", KickerKonst.BLANK);

			// Wenn keiner mehr da ist -> abbrechen
			if (fussballer == null || "".equals(fussballer))
				break;

			int vorkommen = relevanteSeite.indexOf(fussballer + KickerKonst.NACH_FUSSBALLER_NAME_BEI_GELBROT);
			// Fu�baller kommt vor
			if (vorkommen != -1) {
				// ausw�hlen
				(maske.getButt_gelbrot()[i]).setSelection(true);
				(maske.getButt_rot()[i]).setSelection(false);

				// anzeigen
				(maske.getLbl_gelbrot()[i]).setVisible(true);
				(maske.getButt_gelbrot()[i]).setVisible(true);

				// nicht anzeigen
				(maske.getLbl_rot()[i]).setVisible(false);
				(maske.getButt_rot()[i]).setVisible(false);
			}
		}
	}

	/**
	 * Diese Methode f�llt die Gelb-Roten Karten der Spieler
	 */
	private void fuelleRot(String seite, SpieltagErfassenMaske maske) {
		// Zun�chst den relevanten Teil der Seite holen
		String relevanteSeite = "";

		// Die Noten stehen zwischen diesem String und den gelben Karten oder
		// den Spielinfos
		int anfang = seite.indexOf(KickerKonst.ROTE_KARTEN_ANFANG);
		// Wenn es keine rote Karten gegeben hat, zur�ck
		if (anfang == -1)
			return;

		int ende = seite.indexOf(KickerKonst.GELBE_KARTEN_ANFANG);
		// Wenn nicht, dann bis zu "Ansto�"
		if (ende == -1)
			ende = seite.indexOf(KickerKonst.ANSTOSS_ANFANG);

		relevanteSeite = seite.substring(anfang, ende);

		// Das Kriterium f�r eine rote Karte ist das Vorhandensein im relevanten
		// Teil der Seite
		for (int i = 0; i < Konstanten.MAX_EINTRAEGE_AUSWERTUNG; i++) {
			// nur, wenn der Fu�baller zur aktuellen Mannschaft geh�rt
			if ((maske.getLbl_name()[i]).isVisible() == false)
				continue;

			String fussballer = (maske.getLbl_name()[i]).getText().trim();
			fussballer = fussballer.toUpperCase();
			// Blank durch "&nbsp;" ersetzen
			fussballer = fussballer.replaceAll(" ", KickerKonst.BLANK);

			// Wenn keiner mehr da ist -> abbrechen
			if (fussballer == null || "".equals(fussballer))
				break;

			int vorkommen = relevanteSeite.indexOf(fussballer + KickerKonst.NACH_FUSSBALLER_NAME_BEI_ROT);
			// Fu�baller kommt vor
			if (vorkommen != -1) {
				// ausw�hlen
				(maske.getButt_rot()[i]).setSelection(true);
				(maske.getButt_gelbrot()[i]).setSelection(false);

				// anzeigen
				(maske.getLbl_rot()[i]).setVisible(true);
				(maske.getButt_rot()[i]).setVisible(true);

				// nicht anzeigen
				(maske.getLbl_gelbrot()[i]).setVisible(false);
				(maske.getButt_gelbrot()[i]).setVisible(false);
			}
		}
	}
}
