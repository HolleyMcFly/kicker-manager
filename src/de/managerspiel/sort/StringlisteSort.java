package de.managerspiel.sort;

import java.util.Comparator;

public class StringlisteSort implements Comparator<String> {

	public static enum SortDirection { ASCENDING, DESCENDING };

	private SortDirection direction;

	public int compare(String p1, String p2) {

		int rc = p1.compareTo(p2);

		if (direction == SortDirection.DESCENDING) {
			rc = -rc;
		}

		return rc;
	}

	public void setDirection(SortDirection direction) {
		this.direction = direction;
	}

	public void reverseDirection() {
		if (direction == SortDirection.ASCENDING)
			direction = SortDirection.DESCENDING;
		else
			direction = SortDirection.ASCENDING;
	}
}
