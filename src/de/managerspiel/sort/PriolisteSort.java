package de.managerspiel.sort;

import java.util.Comparator;

import de.managerspiel.model.Prioritaeteneintrag;

public class PriolisteSort implements Comparator<Prioritaeteneintrag> {

	public static enum SortColumn { NAME, PRIORITAET };
	public static enum SortDirection { ASCENDING, DESCENDING };

	private SortDirection direction;
	private SortColumn column;

	public int compare(Prioritaeteneintrag p1, Prioritaeteneintrag p2) {

		int rc = 0;

		// Determine which field to sort on, then sort on that field
		switch (column) {
			case NAME:
				rc = p1.getFussballername().compareTo(p2.getFussballername());
				break;
			case PRIORITAET:
				rc = (p1.getPrioritaet() < p2.getPrioritaet()) ? -1 : 1;
				break;
		}

		// Check the direction for sort and flip the sign if appropriate
		if (direction == SortDirection.DESCENDING)
			rc = -rc;

		return rc;
	}

	public void setDirection(SortDirection direction) {
		this.direction = direction;
	}

	public void reverseDirection() {
		if (direction == SortDirection.ASCENDING)
			direction = SortDirection.DESCENDING;
		else
			direction = SortDirection.ASCENDING;
	}

	public void setColumn(SortColumn column) {
		this.column = column;
	}
}
