package de.managerspiel.sort;

import java.util.Comparator;

import de.managerspiel.model.Ergebniseintrag;

public class ErgebnisseSort implements Comparator<Ergebniseintrag> {

	public int compare(Ergebniseintrag e1, Ergebniseintrag e2) {

		if (e1.getErgebnis() < e2.getErgebnis())
			return 1;

		return -1;
	}
}
