package de.managerspiel.sort;

import java.util.Comparator;

import de.managerspiel.model.FussballerErgebnis;

public class FussballerErgebnisComparator implements Comparator<FussballerErgebnis> {

	@Override
	public int compare(FussballerErgebnis o1, FussballerErgebnis o2) {
		
		int value1 = o1.getSpielposition().compareTo(o2.getSpielposition());
		if (value1 == 0) {
			return o1.getPrioritaet().compareTo(o2.getPrioritaet());
		}
		return value1;
	}
}
