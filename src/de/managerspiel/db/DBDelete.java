package de.managerspiel.db;

import java.sql.ResultSet;
import java.sql.SQLException;

import de.managerspiel.exception.DBException;
import de.managerspiel.util.Konstanten;
import de.managerspiel.util.SessionVariablen;

public class DBDelete {

	/**
	 * L�scht einen Eintrag aus der Tabelle Verein
	 */
	public static void deleteTable_Verein(int index) throws DBException {

		String befehl = null;
		try {
			befehl = "DELETE FROM verein WHERE verein_id = " + index + ";";
			(SessionVariablen.getInstance().getStmt()).executeUpdate(befehl);
		}
		catch (SQLException e) {
			throw new DBException(befehl, e.getMessage());
		}
	}

	/**
	 * L�scht einen Eintrag aus der Tabelle Tippspiel
	 */
	public static void deleteTable_Tippspiel(int index) throws DBException {

		String befehl = null;
		try {
			befehl = "DELETE FROM tippspiel WHERE tippspiel_id = " + index + ";";
			(SessionVariablen.getInstance().getStmt()).executeUpdate(befehl);
		}
		catch (SQLException e) {
			throw new DBException(befehl, e.getMessage());
		}
	}

	/**
	 * L�scht einen Eintrag aus der Tabelle Mitspieler
	 */
	public static void deleteTable_Mitspieler(int index) throws DBException {

		String befehl = null;
		try {
			befehl = "DELETE FROM mitspieler WHERE mitspieler_id = " + index + ";";
			(SessionVariablen.getInstance().getStmt()).executeUpdate(befehl);
		}
		catch (SQLException e) {
			throw new DBException(befehl, e.getMessage());
		}
	}

	/**
	 * L�scht einen Eintrag aus der Tabelle R_MS_FB (anhand der Fussballer_id)
	 */
	public static void deleteTable_R_MS_FB_FB(int index) throws DBException {

		String befehl = null;
		try {
			befehl = "DELETE FROM R_MS_FB WHERE fussballer_id = " + index + ";";
			(SessionVariablen.getInstance().getStmt()).executeUpdate(befehl);
		}
		catch (SQLException e) {
			throw new DBException(befehl, e.getMessage());
		}
	}

	/**
	 * L�scht einen Eintrag aus der Tabelle Fussballer
	 */
	public static void deleteTable_Fussballer(int index) throws DBException {

		String befehl = null;
		try {
			befehl = "DELETE FROM fussballer WHERE fussballer_id = " + index + ";";
			(SessionVariablen.getInstance().getStmt()).executeUpdate(befehl);
		}
		catch (SQLException e) {
			throw new DBException(befehl, e.getMessage());
		}
	}

	/**
	 * L�scht die Eintr�ge aus der Tabelle Punktekriterien zum aktuellen Verein und Spieltag
	 */
	public static void deleteTable_Punktekriterien() throws DBException {

		String befehl = null;
		try {
			int vereinId = SessionVariablen.getInstance().getVereinId();
			int spieltag = SessionVariablen.getInstance().getSpieltag();

			// Zun�chst alle Fu�baller zu diesem Verein holen
			befehl = "SELECT * FROM Fussballer WHERE verein_id = " + vereinId + ";";

			ResultSet rs = (SessionVariablen.getInstance().getStmt()).executeQuery(befehl);

			while (!rs.isLast()) {
				rs.next();
				int fussballerId = rs.getInt(1);

				// Jetzt zugeh�rigen Eintrag in Punktekriterien l�schen
				befehl = "DELETE FROM Punktekriterien WHERE fussballer_id = " + fussballerId + " AND spieltag = " + spieltag + ";";

				(SessionVariablen.getInstance().getStmt()).executeUpdate(befehl);
			}
		}
		catch (SQLException e) {
			if (e.getErrorCode() != Konstanten.DB_ERROR_TABLE_EMPTY)
				throw new DBException(befehl, e.getMessage());
		}
	}
}
