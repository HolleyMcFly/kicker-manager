package de.managerspiel.db;

import java.sql.SQLException;
import java.sql.Statement;

import de.managerspiel.exception.DBException;
import de.managerspiel.util.Konstanten;

public class DBCreate {

	/**
	 * Erstellt die Tabelle "Tippspiel"
	 */
	public static void createTable_Tippspiel(Statement stmt) throws DBException {

		String befehl = null;
		try {
			befehl = "CREATE TABLE Tippspiel (tippspiel_id INTEGER IDENTITY NOT NULL, tippspiel_name CHAR (100) NOT NULL, UNIQUE (tippspiel_name));";
			stmt.executeUpdate(befehl);
		}
		catch (SQLException e) {
			if (e.getErrorCode() != Konstanten.DB_ERROR_TABLE_EXISTS)
				throw new DBException(befehl, e.getMessage());
		}
	}

	/**
	 * Erstellt die Tabelle "Mitspieler"
	 */
	public static void createTable_Mitspieler(Statement stmt) throws DBException {

		String befehl = null;
		try {
			befehl = "CREATE TABLE Mitspieler (mitspieler_id INTEGER IDENTITY NOT NULL, tippspiel_id INTEGER NOT NULL, mitspieler_name CHAR (100) NOT NULL, mitspieler_email CHAR (100), FOREIGN KEY (tippspiel_id) REFERENCES tippspiel(tippspiel_id), UNIQUE(mitspieler_name))";
			stmt.executeUpdate(befehl);
		}
		catch (SQLException e) {
			if (e.getErrorCode() != Konstanten.DB_ERROR_TABLE_EXISTS)
				throw new DBException(befehl, e.getMessage());
		}
	}

	/**
	 * Erstellt die Tabelle "Fussballer"
	 */
	public static void createTable_Fussballer(Statement stmt) throws DBException {

		String befehl = null;
		try {
			befehl = "CREATE TABLE Fussballer (fussballer_id INTEGER IDENTITY NOT NULL, fussballer_name CHAR (100) NOT NULL, spielposition INTEGER NOT NULL, verein_id INTEGER NOT NULL, verein_id_neu INTEGER, wechsel_spieltag INTEGER, FOREIGN KEY (verein_id) REFERENCES verein(verein_id), FOREIGN KEY (verein_id_neu) REFERENCES verein(verein_id), UNIQUE(fussballer_name))";
			stmt.executeUpdate(befehl);
		}
		catch (SQLException e) {
			if (e.getErrorCode() != Konstanten.DB_ERROR_TABLE_EXISTS)
				throw new DBException(befehl, e.getMessage());
		}
	}

	/**
	 * Erstellt die Tabelle "Punktekriterien"
	 */
	public static void createTable_Punktekriterien(Statement stmt) throws DBException {

		String befehl = null;
		try {
			befehl = "CREATE TABLE Punktekriterien (fussballer_id INTEGER NOT NULL, spieltag INTEGER NOT NULL, anzahl_tore INTEGER NOT NULL, gelbrote_karte BOOLEAN NOT NULL, rote_karte BOOLEAN NOT NULL, note DOUBLE NOT NULL, gespielt BOOLEAN NOT NULL, PRIMARY KEY (fussballer_id, spieltag), FOREIGN KEY (fussballer_id) REFERENCES fussballer(fussballer_id))";
			stmt.executeUpdate(befehl);
		}
		catch (SQLException e) {
			if (e.getErrorCode() != Konstanten.DB_ERROR_TABLE_EXISTS)
				throw new DBException(befehl, e.getMessage());
		}
	}

	/**
	 * Erstellt die Tabelle "Verein"
	 */
	public static void createTable_Verein(Statement stmt) throws DBException {

		String befehl = null;
		try {
			befehl = "CREATE TABLE Verein (verein_id INTEGER IDENTITY NOT NULL, verein_name CHAR (100) NOT NULL, UNIQUE(verein_name))";
			stmt.executeUpdate(befehl);

			// Initialbestand
			stmt.executeUpdate("INSERT INTO Verein (verein_id, verein_name) VALUES (-1, 'Ausland')");
		}
		catch (SQLException e) {
			if (e.getErrorCode() != Konstanten.DB_ERROR_TABLE_EXISTS)
				throw new DBException(befehl, e.getMessage());
		}
	}

	/**
	 * Erstellt die Tabelle "R_MS_FB" (Referenz Mitspieler - Fussballer)
	 */
	public static void createTable_R_MS_FB(Statement stmt) throws DBException {

		String befehl = null;
		try {
			befehl = "CREATE TABLE R_MS_FB (mitspieler_id INTEGER NOT NULL, fussballer_id INTEGER NOT NULL, PRIMARY KEY (mitspieler_id, fussballer_id), FOREIGN KEY (mitspieler_id) REFERENCES mitspieler(mitspieler_id), FOREIGN KEY (fussballer_id) REFERENCES fussballer(fussballer_id))";
			stmt.executeUpdate(befehl);
		}
		catch (SQLException e) {
			if (e.getErrorCode() != Konstanten.DB_ERROR_TABLE_EXISTS)
				throw new DBException(befehl, e.getMessage());
		}
	}

	/**
	 * Erstellt die Tabelle "Prioritaeten"
	 */
	public static void createTable_Prioritaeten(Statement stmt) throws DBException {

		String befehl = null;
		try {
			befehl = "CREATE TABLE Prioritaeten (mitspieler_id INTEGER NOT NULL, fussballer_id INTEGER NOT NULL, spieltag INTEGER NOT NULL, prioritaet INTEGER NOT NULL, PRIMARY KEY (mitspieler_id, fussballer_id, spieltag), FOREIGN KEY (mitspieler_id) REFERENCES mitspieler(mitspieler_id), FOREIGN KEY (fussballer_id) REFERENCES fussballer(fussballer_id))";
			stmt.executeUpdate(befehl);
		}
		catch (SQLException e) {
			if (e.getErrorCode() != Konstanten.DB_ERROR_TABLE_EXISTS)
				throw new DBException(befehl, e.getMessage());
		}
	}

	/**
	 * Erstellt die Tabelle "Einstellungen"
	 */
	public static void createTable_Einstellungen(Statement stmt) throws DBException {

		String befehl = null;
		try {
			befehl = "CREATE TABLE Einstellungen (einstellung_id CHAR (100) NOT NULL, einstellung_wert CHAR (1000) NOT NULL, UNIQUE(einstellung_id))";
			stmt.executeUpdate(befehl);
		}
		catch (SQLException e) {
			if (e.getErrorCode() != Konstanten.DB_ERROR_TABLE_EXISTS)
				throw new DBException(befehl, e.getMessage());
		}
	}
}