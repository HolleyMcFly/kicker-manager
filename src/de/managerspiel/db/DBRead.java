package de.managerspiel.db;

import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;

import de.managerspiel.exception.DBException;
import de.managerspiel.model.Auswertungseintrag;
import de.managerspiel.util.SessionVariablen;

public class DBRead {

	/**
	 * Liest einen Eintrag aus der Tabelle Tippspiel. TippspielId und TippspielName werden in SessionVariablen gespeichert.
	 */
	public static void setTippspielData(int tippspielId) throws DBException {

		String befehl = null;
		try {
			befehl = "SELECT * FROM Tippspiel WHERE tippspiel_id = " + tippspielId + ";";
			ResultSet rs = (SessionVariablen.getInstance().getStmt()).executeQuery(befehl);

			if (rs.next()) {
				SessionVariablen.getInstance().setTippspielId(rs.getInt(1));
				SessionVariablen.getInstance().setTippspielname(rs.getString(2));
			}
		}
		catch (SQLException e) {
			throw new DBException(befehl, e.getMessage());
		}
	}

	/**
	 * Liefert alle Eintr�ge aus der Tabelle Tippspiel
	 */
	public static List<String> getTippspiele() throws DBException {

		String befehl = null;
		try {
			befehl = "SELECT * FROM Tippspiel;";
			ResultSet rs = (SessionVariablen.getInstance().getStmt()).executeQuery(befehl);

			List<String> tippspiele = new ArrayList<String>();
			while (rs.next())
				tippspiele.add(rs.getString(2));

			return tippspiele;
		}
		catch (SQLException e) {
			throw new DBException(befehl, e.getMessage());
		}
	}

	/**
	 * Liefert die ID zu einem Eintrag aus der Tabelle Tippspiel
	 */
	public static int getTippspielID(String tippspielName) throws DBException {

		String befehl = null;
		try {
			befehl = "SELECT * FROM Tippspiel WHERE tippspiel_name = '" + tippspielName + "';";
			ResultSet rs = (SessionVariablen.getInstance().getStmt()).executeQuery(befehl);

			if (rs.next()) {
				int id = rs.getInt(1);
				return id;
			}

			throw new DBException("No data in Tippspiel for tippspiel_name " + tippspielName);
		}
		catch (SQLException e) {
			throw new DBException(befehl, e.getMessage());
		}
	}

	/**
	 * Liefert einen Eintrag aus der Tabelle Verein Verein_id wird in SessionVariablen gespeichert.
	 */
	public static String getVerein(int vereinId) throws DBException {

		String befehl = null;
		try {
			befehl = "SELECT * FROM Verein WHERE verein_id = " + vereinId + ";";
			ResultSet rs = (SessionVariablen.getInstance().getStmt()).executeQuery(befehl);

			if (rs.next()) {
				SessionVariablen.getInstance().setVereinId(rs.getInt(1));
				String vereinsname = rs.getString(2);
				return vereinsname;
			}

			throw new DBException("No data in Verein vor verein_id " + vereinId);
		}
		catch (SQLException e) {
			throw new DBException(befehl, e.getMessage());
		}
	}

	/**
	 * Liefert alle Eintr�ge aus der Tabelle Verein
	 */
	public static List<String> getVereine() throws DBException {

		String befehl = null;
		try {
			befehl = "SELECT * FROM Verein WHERE verein_id > -1;";
			ResultSet rs = (SessionVariablen.getInstance().getStmt()).executeQuery(befehl);

			List<String> vereine = new ArrayList<String>();
			while (rs.next())
				vereine.add(rs.getString(2));

			return vereine;
		}
		catch (SQLException e) {
			throw new DBException(befehl, e.getMessage());
		}
	}

	/**
	 * Liefert die ID zu einem Eintrag aus der Tabelle Verein
	 */
	public static int getVereinID(String vereinName) throws DBException {

		String befehl = null;
		try {
			befehl = "SELECT * FROM Verein WHERE verein_name = '" + vereinName + "';";
			ResultSet rs = (SessionVariablen.getInstance().getStmt()).executeQuery(befehl);

			if (rs.next()) {
				int id = rs.getInt(1);
				return id;
			}

			throw new DBException("No data in Verein for verein_name " + vereinName);
		}
		catch (SQLException e) {
			throw new DBException(befehl, e.getMessage());
		}
	}

	/**
	 * Liefert einen Eintrag aus der Tabelle Mitspieler
	 */
	public static String getMitspieler(int mitspielerID) throws DBException {

		String befehl = null;
		try {
			befehl = "SELECT * FROM Mitspieler WHERE mitspieler_id = " + mitspielerID + ";";

			ResultSet rs = (SessionVariablen.getInstance().getStmt()).executeQuery(befehl);
			if (rs.next()) {
				String mitspielername = rs.getString(3);
				return mitspielername;
			}

			throw new DBException("No data in Mitspieler for mitspieler_id " + mitspielerID);
		}
		catch (SQLException e) {
			throw new DBException(befehl, e.getMessage());
		}
	}

	/**
	 * Liefert alle Eintr�ge aus der Tabelle Mitspieler zum aktuellen Tippspiel
	 */
	public static List<String> getAlleMitspieler() throws DBException {

		String befehl = null;
		try {
			int tippspielId = SessionVariablen.getInstance().getTippspielId();
			befehl = "SELECT * FROM Mitspieler WHERE tippspiel_id = " + tippspielId + ";";

			ResultSet rs = (SessionVariablen.getInstance().getStmt()).executeQuery(befehl);

			List<String> mitspieler = new ArrayList<String>();
			while (rs.next())
				mitspieler.add(rs.getString(3));

			return mitspieler;
		}
		catch (SQLException e) {
			throw new DBException(befehl, e.getMessage());
		}
	}

	/**
	 * Liefert alle aktuellen Eintr�ge aus der Tabelle Fu�baller zum aktuellen Mitspieler
	 */
	public static List<String> getAlleFussballerMS() throws DBException {

		String befehl = null;
		try {
			int mitspielerId = SessionVariablen.getInstance().getMitspielerId();
			befehl = "SELECT * FROM R_MS_FB WHERE mitspieler_id = " + mitspielerId + ";";
			ResultSet rs = (SessionVariablen.getInstance().getStmt()).executeQuery(befehl);

			List<String> fussballer = new ArrayList<String>();
			while (rs.next()) {
				int fussballerId = rs.getInt(2);
				if (isFussballerAktuell(fussballerId))
					fussballer.add(DBRead.getFussballer(fussballerId));
			}
			return fussballer;
		}
		catch (SQLException e) {
			throw new DBException(befehl, e.getMessage());
		}
	}

	/**
	 * Liefert alle aktuellen Eintr�ge aus der Tabelle Fu�baller zum aktuellen Verein
	 */
	public static List<String> getAlleFussballerVerein() throws DBException {

		String befehl = null;
		try {
			int vereinId = SessionVariablen.getInstance().getVereinId();

			// Nicht gewechselte Fu�baller
			befehl = "SELECT * FROM FUSSBALLER WHERE verein_id = " + vereinId + ";";
			ResultSet rs = (SessionVariablen.getInstance().getStmt()).executeQuery(befehl);

			List<String> fussballer = new ArrayList<String>();
			while (rs.next()) {
				int fussballerId = rs.getInt(1);

				if (isFussballerAktuell(fussballerId))
					fussballer.add(rs.getString(2));
			}

			// Im Inland gewechselte Fu�baller
			befehl = "SELECT * FROM FUSSBALLER WHERE verein_id_neu = " + vereinId + " AND wechsel_spieltag < "
					+ SessionVariablen.getInstance().getSpieltag() + ";";

			ResultSet rs2 = (SessionVariablen.getInstance().getStmt()).executeQuery(befehl);
			while (rs2.next())
				fussballer.add(rs2.getString(2));

			return fussballer;
		}
		catch (SQLException e) {
			throw new DBException(befehl, e.getMessage());
		}
	}

	/**
	 * Liefert alle Eintr�ge aus der Tabelle Fu�baller
	 */
	public static List<String> getAlleFussballer() throws DBException {

		String befehl = null;
		try {
			befehl = "SELECT * FROM R_MS_FB;";
			ResultSet rs = (SessionVariablen.getInstance().getStmt()).executeQuery(befehl);

			List<String> fussballer = new ArrayList<String>();
			while (rs.next()) {
				int fussballerId = rs.getInt(2);
				fussballer.add(DBRead.getFussballer(fussballerId));
			}
			return fussballer;
		}
		catch (SQLException e) {
			throw new DBException(befehl, e.getMessage());
		}
	}

	/**
	 * Liefert die ID zu einem Eintrag aus der Tabelle Fussballer
	 */
	public static int getFussballerID(String fussballerName) throws DBException {

		String befehl = null;
		try {
			befehl = "SELECT * FROM Fussballer WHERE fussballer_name = '" + fussballerName + "';";
			ResultSet rs = (SessionVariablen.getInstance().getStmt()).executeQuery(befehl);

			if (rs.next()) {
				int id = rs.getInt(1);
				return id;
			}

			throw new DBException("No data in Fussballer for fussballer_name " + fussballerName);
		}
		catch (SQLException e) {
			throw new DBException(befehl, e.getMessage());
		}
	}

	/**
	 * Liefert die Position eines Fu�ballers zur �bergebenen ID
	 */
	public static int getFussballerpositionID(int fussballerId) throws DBException {

		String befehl = null;
		try {
			befehl = "SELECT * FROM Fussballer WHERE fussballer_id = " + fussballerId + ";";
			ResultSet rs = (SessionVariablen.getInstance().getStmt()).executeQuery(befehl);

			if (rs.next()) {
				fussballerId = rs.getInt(3);
				return fussballerId;
			}

			throw new DBException("No data in Fussballer for fussballer_id " + fussballerId);
		}
		catch (SQLException e) {
			throw new DBException(befehl, e.getMessage());
		}
	}

	/**
	 * Liefert einen Eintrag aus der Tabelle Fussballer
	 */
	public static String getFussballer(int fussballerId) throws DBException {

		String befehl = null;
		try {
			befehl = "SELECT * FROM Fussballer WHERE fussballer_id = " + fussballerId + ";";
			ResultSet rs = (SessionVariablen.getInstance().getStmt()).executeQuery(befehl);

			if (rs.next()) {
				String fussballername = rs.getString(2);
				return fussballername;
			}

			throw new DBException("No data in Fussballer for fussballer_id " + fussballerId);
		}
		catch (SQLException e) {
			throw new DBException(befehl, e.getMessage());
		}
	}

	/**
	 * Liefert die Spielposition aus der Tabelle Fussballer
	 */
	public static int getSpielposition(int fussballerId) throws DBException {

		String befehl = null;
		try {
			befehl = "SELECT * FROM Fussballer WHERE fussballer_id = " + fussballerId + ";";
			ResultSet rs = (SessionVariablen.getInstance().getStmt()).executeQuery(befehl);

			if (rs.next())
				return (rs.getInt(3));

			throw new DBException("No data in Fussballer for fussballer_id " + fussballerId);
		}
		catch (SQLException e) {
			throw new DBException(befehl, e.getMessage());
		}
	}

	/**
	 * Liefert die VereinId aus der Tabelle Fussballer
	 */
	public static int getVereinIdFS(int fussballerId) throws DBException {

		String befehl = null;
		try {
			befehl = "SELECT * FROM Fussballer WHERE fussballer_id = " + fussballerId + ";";
			ResultSet rs = (SessionVariablen.getInstance().getStmt()).executeQuery(befehl);

			if (rs.next()) {
				if (rs.getInt(5) == 0)
					return (rs.getInt(4));
				else
					return (rs.getInt(5));
			}

			throw new DBException("No data in Fussballer for fussballer_id " + fussballerId);
		}
		catch (SQLException e) {
			throw new DBException(befehl, e.getMessage());
		}
	}

	/**
	 * Liefert die ID zu einem Eintrag aus der Tabelle Mitspieler
	 */
	public static int getMitspielerID(String mitspieler) throws DBException {

		String befehl = null;
		try {
			befehl = "SELECT * FROM Mitspieler WHERE mitspieler_name = '" + mitspieler + "';";
			ResultSet rs = (SessionVariablen.getInstance().getStmt()).executeQuery(befehl);

			if (rs.next()) {
				int id = rs.getInt(1);
				return id;
			}

			throw new DBException("No data in Mitspieler for mitspieler_name " + mitspieler);
		}
		catch (SQLException e) {
			throw new DBException(befehl, e.getMessage());
		}
	}

	/**
	 * Kontrolliert, ob die TippspielId in der Tabelle Mitspieler verwendet wird
	 */
	public static boolean isTippspielIdInMitspieler(int tippspielId) throws DBException {

		String befehl = null;
		try {
			befehl = "SELECT COUNT (*) FROM Mitspieler WHERE tippspiel_id = " + tippspielId + ";";
			ResultSet rs = (SessionVariablen.getInstance().getStmt()).executeQuery(befehl);

			if (rs.next()) {
				int anzahl = rs.getInt(1);

				if (anzahl == 0)
					return false;
				else
					return true;
			}

			throw new DBException("No data in Mitspieler for tippspiel_id " + tippspielId);
		}
		catch (SQLException e) {
			throw new DBException(befehl, e.getMessage());
		}
	}

	/**
	 * Kontrolliert, ob die MitspielerId in der Tabelle R_MS_FB verwendet wird
	 */
	public static boolean isMitspielerIdInR_MS_FB(int mitspielerId) throws DBException {

		String befehl = null;
		try {
			befehl = "SELECT COUNT (*) FROM R_MS_FB WHERE mitspieler_id = " + mitspielerId + ";";
			ResultSet rs = (SessionVariablen.getInstance().getStmt()).executeQuery(befehl);

			if (rs.next()) {
				int anzahl = rs.getInt(1);

				if (anzahl == 0)
					return false;
				else
					return true;
			}

			throw new DBException("No data in R_MS_FB for mitspieler_id " + mitspielerId);
		}
		catch (SQLException e) {
			throw new DBException(befehl, e.getMessage());
		}
	}

	/**
	 * Liefert alle aktuellen Spieler auf einer bestimmten Position aus der Tabelle Fu�baller zum aktuellen Mitspieler
	 */
	public static List<String> getFussballer_MS_SP(int spielposition) throws DBException {

		String befehl = null;
		try {
			List<String> fussballer = new ArrayList<String>();

			int mitspielerId = SessionVariablen.getInstance().getMitspielerId();
			befehl = "SELECT * FROM R_MS_FB WHERE mitspieler_id = " + mitspielerId + ";";
			ResultSet rs = (SessionVariablen.getInstance().getStmt()).executeQuery(befehl);

			while (rs.next()) {
				int fussballerId = rs.getInt(2);

				// Nur eintragen, wenn der Fu�baller auf der richtigen Position spielt
				int fussballerPosition = DBRead.getFussballerpositionID(fussballerId);
				if (fussballerPosition == spielposition) {
					if (isFussballerAktuell(fussballerId))
						fussballer.add(DBRead.getFussballer(fussballerId));
				}
			}

			return fussballer;
		}
		catch (SQLException e) {
			throw new DBException(befehl, e.getMessage());
		}
	}

	/**
	 * Liefert die Anzahl an Priorit�ten f�r den aktuellen Mitspieler f�r den aktuellen Spieltag
	 */
	public static int getNumberPrios() throws DBException {

		String befehl = null;
		try {
			int mitspielerId = SessionVariablen.getInstance().getMitspielerId();
			int spieltag = SessionVariablen.getInstance().getSpieltag();

			befehl = "SELECT COUNT (*) FROM prioritaeten WHERE mitspieler_id = " + mitspielerId + " AND spieltag = " + spieltag + ";";
			ResultSet rs = (SessionVariablen.getInstance().getStmt()).executeQuery(befehl);

			int anzahl = 0;
			while (rs.next())
				anzahl = rs.getInt(1);

			return anzahl;
		}
		catch (SQLException e) {
			throw new DBException(befehl, e.getMessage());
		}
	}

	/**
	 * Liefert die Priorit�t zum aktuellen Mitspieler, aktuellen Fu�baller und aktuellen Spieltag
	 */
	public static int getPrioritaet() throws DBException {

		String befehl = null;
		try {
			int mitspieler_id = SessionVariablen.getInstance().getMitspielerId();
			int fussballer_id = SessionVariablen.getInstance().getFussballerId();
			int spieltag = SessionVariablen.getInstance().getSpieltag();

			befehl = "SELECT prioritaet FROM prioritaeten WHERE mitspieler_id = " + mitspieler_id + " AND fussballer_id = " + fussballer_id
					+ " AND spieltag = " + spieltag + ";";
			ResultSet rs = (SessionVariablen.getInstance().getStmt()).executeQuery(befehl);

			int prio = 0;
			while (rs.next())
				prio = rs.getInt(1);
			
			return prio;
		}
		catch (SQLException e) {
			throw new DBException(befehl, e.getMessage());
		}
	}

	/**
	 * Liefert die Anzahl der Prioritaeten zum aktuellen Mitspieler und Spieltag
	 */
	public static int getAnzahlPrioritaeten() throws DBException {

		String befehl = null;
		try {
			int mitspieler_id = SessionVariablen.getInstance().getMitspielerId();
			int spieltag = SessionVariablen.getInstance().getSpieltag();

			befehl = "SELECT COUNT (*) FROM prioritaeten WHERE mitspieler_id = " + mitspieler_id + " AND spieltag = " + spieltag + ";";
			ResultSet rs = (SessionVariablen.getInstance().getStmt()).executeQuery(befehl);

			int anzahl = 0;
			while (rs.next())
				anzahl = rs.getInt(1);

			return anzahl;
		}
		catch (SQLException e) {
			throw new DBException(befehl, e.getMessage());
		}
	}

	/**
	 * Liefert die Anzahl der aktuellen Fu�baller zum aktuellen Mitspieler
	 */
	public static int getAnzahlFussballer() throws DBException {

		String befehl = null;
		try {
			int mitspielerId = SessionVariablen.getInstance().getMitspielerId();
			befehl = "SELECT * FROM R_MS_FB WHERE mitspieler_id = " + mitspielerId + ";";
			ResultSet rs = (SessionVariablen.getInstance().getStmt()).executeQuery(befehl);

			int anzahl = 0;
			while (rs.next()) {

				int fussballerId = rs.getInt(2);
				if (isFussballerAktuell(fussballerId))
					anzahl++;
			}
			return anzahl;
		}
		catch (SQLException e) {
			throw new DBException(befehl, e.getMessage());
		}
	}

	/**
	 * Holt die Anzahl der Mitspieler zu einem Tippspiel
	 */
	public static int getAnzahlMitspieler() throws DBException {

		String befehl = null;
		try {
			int anzahl = 0;
			befehl = "SELECT COUNT (*) FROM mitspieler WHERE tippspiel_id = " + SessionVariablen.getInstance().getTippspielId() + ";";
			ResultSet rs = (SessionVariablen.getInstance().getStmt()).executeQuery(befehl);

			while (rs.next())
				anzahl = rs.getInt(1);
			
			return anzahl;
		}
		catch (SQLException e) {
			throw new DBException(befehl, e.getMessage());
		}
	}

	/**
	 * Liefert einen Eintrag aus der Tabelle Punktekriterien
	 */
	public static void setPunktekriterien(Auswertungseintrag ausw) throws DBException {

		String befehl = null;
		try {
			int fussballerId = DBRead.getFussballerID(ausw.getFussballername());
			int spieltag = SessionVariablen.getInstance().getSpieltag();
			
			befehl = "SELECT * FROM Punktekriterien WHERE fussballer_id = " + fussballerId + " AND spieltag = " + spieltag + ";";
			ResultSet rs = (SessionVariablen.getInstance().getStmt()).executeQuery(befehl);

			if (rs.next()) {
				ausw.setGespielt(rs.getBoolean(7));
				ausw.setAnzahl_tore(rs.getInt(3));
				ausw.setGelbrote_karte(rs.getBoolean(4));
				ausw.setRote_karte(rs.getBoolean(5));
				ausw.setNote(rs.getDouble(6));
			}
		}
		catch (SQLException e) {
			throw new DBException(befehl, e.getMessage());
		}
	}

	/**
	 * Liefert die Anzahl der Tore aus der Tabelle Punktekriterien
	 */
	public static int getAnzahl_Tore(int fussballerId) throws DBException {

		String befehl = null;
		try {
			befehl = "SELECT * FROM Punktekriterien WHERE fussballer_id = " + fussballerId + " AND spieltag = "
					+ SessionVariablen.getInstance().getSpieltag() + ";";
			ResultSet rs = (SessionVariablen.getInstance().getStmt()).executeQuery(befehl);
			
			if (rs.next())
				return (rs.getInt(3));

			throw new DBException("No data in Punktekriterien for fussballer_id " + fussballerId);
		}
		catch (SQLException e) {
			throw new DBException(befehl, e.getMessage());
		}
	}

	/**
	 * Liefert das Flag "gespielt" aus der Tabelle Punktekriterien
	 */
	public static boolean getGespielt(int fussballerId) throws DBException {

		String befehl = null;
		try {
			befehl = "SELECT * FROM Punktekriterien WHERE fussballer_id = " + fussballerId + " AND spieltag = "
					+ SessionVariablen.getInstance().getSpieltag() + ";";

			ResultSet rs = (SessionVariablen.getInstance().getStmt()).executeQuery(befehl);
			if (rs.next())
				return (rs.getBoolean(7));

			throw new DBException("No data in Punktekriterien for fussballer_id " + fussballerId);
		}
		catch (SQLException e) {
			throw new DBException(befehl, e.getMessage());
		}
	}

	/**
	 * Liefert das Flag "gelbrote Karte" aus der Tabelle Punktekriterien
	 */
	public static boolean getGelbRot(int fussballerId) throws DBException {

		String befehl = null;
		try {
			befehl = "SELECT * FROM Punktekriterien WHERE fussballer_id = " + fussballerId + " AND spieltag = "
					+ SessionVariablen.getInstance().getSpieltag() + ";";
			ResultSet rs = (SessionVariablen.getInstance().getStmt()).executeQuery(befehl);
			
			if (rs.next())
				return (rs.getBoolean(4));

			throw new DBException("No data in Punktekriterien for fussballer_id " + fussballerId);
		}
		catch (SQLException e) {
			throw new DBException(befehl, e.getMessage());
		}
	}

	/**
	 * Liefert das Flag "rote Karte" aus der Tabelle Punktekriterien
	 */
	public static boolean getRot(int fussballerId) throws DBException {

		String befehl = null;
		try {
			befehl = "SELECT * FROM Punktekriterien WHERE fussballer_id = " + fussballerId + " AND spieltag = "
					+ SessionVariablen.getInstance().getSpieltag() + ";";
			ResultSet rs = (SessionVariablen.getInstance().getStmt()).executeQuery(befehl);
			
			if (rs.next())
				return (rs.getBoolean(5));

			throw new DBException("No data in Punktekriterien for fussballer_id " + fussballerId);
		}
		catch (SQLException e) {
			throw new DBException(befehl, e.getMessage());
		}
	}

	/**
	 * Liefert die Note aus der Tabelle Punktekriterien
	 */
	public static double getNote(int fussballerId) throws DBException {

		String befehl = null;
		try {
			befehl = "SELECT * FROM Punktekriterien WHERE fussballer_id = " + fussballerId + " AND spieltag = "
					+ SessionVariablen.getInstance().getSpieltag() + ";";
			ResultSet rs = (SessionVariablen.getInstance().getStmt()).executeQuery(befehl);
			
			if (rs.next())
				return (rs.getDouble(6));

			throw new DBException("No data in Punktekriterien for fussballer_id " + fussballerId);
		}
		catch (SQLException e) {
			throw new DBException(befehl, e.getMessage());
		}
	}

	/**
	 * Liefert die Fu�baller-ids aus der Priorit�tenliste f�r den aktuellen Mitspieler und Spieltag
	 */
	public static List<Integer> getFussballer_Prioritaet() throws DBException {

		String befehl = null;
		try {
			List<Integer> fussballerIDs = new ArrayList<Integer>();
			befehl = "SELECT * FROM Prioritaeten WHERE mitspieler_id = " + SessionVariablen.getInstance().getMitspielerId()
					+ " AND spieltag = " + SessionVariablen.getInstance().getSpieltag() + ";";
			ResultSet rs = (SessionVariablen.getInstance().getStmt()).executeQuery(befehl);

			while (rs.next())
				fussballerIDs.add(rs.getInt(2));

			return fussballerIDs;
		}
		catch (SQLException e) {
			throw new DBException(befehl, e.getMessage());
		}
	}

	/**
	 * Liefert die Anzahl der Punktekriterien zum aktuellen Spieltag
	 */
	public static int getAnzahlPunktekriterien() throws DBException {

		String befehl = null;
		try {
			int spieltag = SessionVariablen.getInstance().getSpieltag();
			befehl = "SELECT COUNT (*) FROM punktekriterien WHERE spieltag = " + spieltag + ";";
			ResultSet rs = (SessionVariablen.getInstance().getStmt()).executeQuery(befehl);

			int anzahl = 0;
			while (rs.next())
				anzahl = rs.getInt(1);

			return anzahl;
		}
		catch (SQLException e) {
			throw new DBException(befehl, e.getMessage());
		}
	}

	/**
	 * Kontrolliert, ob der Fu�baller noch aktuell ist (kein Wechsel ins Ausland)
	 */
	public static boolean isFussballerAktuell(int fussballer_id) throws DBException {

		String befehl = null;
		try {
			int spieltag = SessionVariablen.getInstance().getSpieltag();
			befehl = "SELECT wechsel_spieltag, verein_id_neu FROM fussballer WHERE fussballer_id = " + fussballer_id + ";";
			ResultSet rs = (SessionVariablen.getInstance().getStmt()).executeQuery(befehl);

			int wechselspieltag = 0;
			int neuer_verein = 0;
			while (rs.next()) {
				wechselspieltag = rs.getInt(1);
				neuer_verein = rs.getInt(2);
			}

			// Spieler ist ins Ausland gewechselt, wenn als neue VereinsId -1 eingetragen ist,
			// und wenn der Wechsel vor dem aktuellen Spieltag stattfand
			if (neuer_verein == -1 && spieltag > wechselspieltag)
				return false;
			else
				return true;
		}
		catch (SQLException e) {
			throw new DBException(befehl, e.getMessage());
		}
	}

	/**
	 * Setzt alle gespeicherten Einstellungen in die SessionVariablen
	 */
	public static void setAllEinstellungen() throws DBException {

		String befehl = null;
		try {
			befehl = "SELECT einstellung_id, einstellung_wert FROM Einstellungen;";
			ResultSet rs = (SessionVariablen.getInstance().getStmt()).executeQuery(befehl);

			while (rs.next()) {
				String key = rs.getString(1);
				String value = rs.getString(2);

				value = value.replaceAll("--SEP--", "\\/");
				SessionVariablen.getInstance().addEinstellung(key, value);
			}
		}
		catch (SQLException e) {
			throw new DBException(befehl, e.getMessage());
		}
	}
}
