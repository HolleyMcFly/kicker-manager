package de.managerspiel.db;

import java.sql.DriverManager;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.List;

import de.managerspiel.exception.DBException;
import de.managerspiel.util.SessionVariablen;

public class DBUtil {

	/**
	 * �ffnet die Datenbank
	 */
	public static void open() {

		// Datenbanktreiber laden
		try {
			Class.forName("org.hsqldb.jdbcDriver");
		}
		catch (Exception e) {
			System.err.println("Fehler beim Laden des Datenbanktreibers");
			System.err.println(e);
		}

		// Verbindung zur Datenbank herstellen
		try {
			SessionVariablen.getInstance().setCon(DriverManager.getConnection("jdbc:hsqldb:file:ManagerDB", "SA", ""));
			SessionVariablen.getInstance().setStmt((SessionVariablen.getInstance().getCon()).createStatement());
		}
		catch (SQLException e) {
			System.err.println("Fehler beim Erstellen der Verbindung zur Datenbank");
			System.err.println(e);
		}
	}

	/**
	 * Schlie�t die Datenbank
	 */
	public static void close() {
		try {
			(SessionVariablen.getInstance().getStmt()).executeQuery("SHUTDOWN");
			(SessionVariablen.getInstance().getStmt()).close();
			(SessionVariablen.getInstance().getCon()).close();
		}
		catch (SQLException e) {
			System.err.println("Fehler beim Schlie�en der Datenbankverbindung");
			System.err.println(e);
		}
	}

	/**
	 * Holt den zuletzt eingef�gten Key aus der angegebenen Tabelle
	 */
	public static int getNewKey() {
		
		int newkey = 0;
		try {
			String befehl = "CALL IDENTITY ();";

			ResultSet rs = (SessionVariablen.getInstance().getStmt()).executeQuery(befehl);

			rs.next();
			newkey = rs.getInt(1);
		}
		catch (SQLException e) {
			System.err.println("Fehler beim Lesen des neuen Keys!");
			System.err.println(e);
		}
		return newkey;
	}

	/**
	 * Kopiert die Priorit�ten des letzten Spieltags in den aktuellen
	 */
	public static void copyPrioritaeten() throws DBException {

		// Wenn es der erste Spieltag ist, kann nichts kopiert werden
		if (SessionVariablen.getInstance().getSpieltag() == 1)
			return;

		// initialisieren
		SessionVariablen.getInstance().setSpieltag(SessionVariablen.getInstance().getSpieltag() - 1);
		List<Integer> geleseneIDs;
		try {
			geleseneIDs = DBRead.getFussballer_Prioritaet();
		}
		catch (DBException e) {
			e.handleError();
			throw new RuntimeException(e.getError());
		}
		
		SessionVariablen.getInstance().setSpieltag(SessionVariablen.getInstance().getSpieltag() + 1);

		for (Integer geleseneID : geleseneIDs) {
			SessionVariablen.getInstance().setFussballerId(geleseneID);

			SessionVariablen.getInstance().setSpieltag(SessionVariablen.getInstance().getSpieltag() - 1);
			int prioritaet;
			try {
				prioritaet = DBRead.getPrioritaet();
			}
			catch (DBException e) {
				e.handleError();
				throw new RuntimeException(e.getError());
			}
			SessionVariablen.getInstance().setSpieltag(SessionVariablen.getInstance().getSpieltag() + 1);

			// Eintrag erstellen
			DBInsert.insertTable_Prioritaeten(SessionVariablen.getInstance().getFussballerId(), prioritaet, SessionVariablen.getInstance()
					.getSpieltag());
		}
	}
}
