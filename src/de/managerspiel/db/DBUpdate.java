package de.managerspiel.db;

import java.sql.SQLException;

import de.managerspiel.exception.DBException;
import de.managerspiel.util.SessionVariablen;

public class DBUpdate {

	/**
	 * Aktualisiert eine Zeile in der Tabelle Verein
	 */
	public static void updateTable_Verein(int id, String name) throws DBException {
		
		String befehl = null;
		try {
			befehl = "UPDATE verein SET verein_name = '" + name + "' WHERE verein_id = " + id + ";";
			(SessionVariablen.getInstance().getStmt()).executeUpdate(befehl);
		}
		catch (SQLException e) {
			throw new DBException(befehl, e.getMessage());
		}
	}

	/**
	 * Aktualisiert eine Zeile in der Tabelle Tippspiel
	 */
	public static void updateTable_Tippspiel(int id, String name) throws DBException {
		
		String befehl = null;
		try {
			befehl = "UPDATE tippspiel SET tippspiel_name = '" + name + "' WHERE tippspiel_id = " + id + ";";
			(SessionVariablen.getInstance().getStmt()).executeUpdate(befehl);
		}
		catch (SQLException e) {
			throw new DBException(befehl, e.getMessage());
		}
	}

	/**
	 * Aktualisiert eine Zeile in der Tabelle Mitspieler
	 */
	public static void updateTable_Mitspieler(int id, String name) throws DBException {
		
		String befehl = null;
		try {
			befehl = "UPDATE mitspieler SET mitspieler_name = '" + name + "' WHERE mitspieler_id = " + id + ";";
			(SessionVariablen.getInstance().getStmt()).executeUpdate(befehl);
		}
		catch (SQLException e) {
			throw new DBException(befehl, e.getMessage());
		}
	}

	/**
	 * Aktualisiert eine Zeile in der Tabelle Fußballer
	 */
	public static void updateTable_Fussballer(int id, String name, int position) throws DBException {
		
		String befehl = null;
		try {
			befehl = "UPDATE fussballer SET fussballer_name = '" + name + "' WHERE fussballer_id = " + id + ";";
			(SessionVariablen.getInstance().getStmt()).executeUpdate(befehl);

			befehl = "UPDATE fussballer SET spielposition = " + position + " WHERE fussballer_id = " + id + ";";
			(SessionVariablen.getInstance().getStmt()).executeUpdate(befehl);
		}
		catch (SQLException e) {
			throw new DBException(befehl, e.getMessage());
		}
	}

	/**
	 * Aktualisiert eine Zeile in der Tabelle Fußballer (Fußballerwechsel ins Ausland)
	 */
	public static void updateTable_Fussballer_VereinAusland(String fussballername, int spieltag) throws DBException {
		
		String befehl = null;
		try {
			int id = DBRead.getFussballerID(fussballername);
			befehl = "UPDATE fussballer SET verein_id_neu = -1 WHERE fussballer_id = " + id + ";";
			(SessionVariablen.getInstance().getStmt()).executeUpdate(befehl);

			befehl = "UPDATE fussballer SET wechsel_spieltag = " + spieltag + " WHERE fussballer_id = " + id + ";";
			(SessionVariablen.getInstance().getStmt()).executeUpdate(befehl);
		}
		catch (SQLException e) {
			throw new DBException(befehl, e.getMessage());
		}
	}

	/**
	 * Aktualisiert eine Zeile in der Tabelle Fußballer (Fußballerwechsel im Inland)
	 */
	public static void updateTable_Fussballer_VereinInland(String fussballername, int vereinIdNeu, int spieltag) throws DBException {
		
		String befehl = null;
		try {
			int id = DBRead.getFussballerID(fussballername);
			befehl = "UPDATE fussballer SET verein_id_neu = " + vereinIdNeu + " WHERE fussballer_id = " + id + ";";
			(SessionVariablen.getInstance().getStmt()).executeUpdate(befehl);

			befehl = "UPDATE fussballer SET wechsel_spieltag = " + spieltag + " WHERE fussballer_id = " + id + ";";
			(SessionVariablen.getInstance().getStmt()).executeUpdate(befehl);
		}
		catch (SQLException e) {
			throw new DBException(befehl, e.getMessage());
		}
	}

	/**
	 * Aktualisiert eine Zeile in der Tabelle Prioritaeten
	 */
	public static void updateTable_Prioritaeten(int prio) throws DBException {
		
		String befehl = null;
		try {
			int mitspielerId = SessionVariablen.getInstance().getMitspielerId();
			int fussballerId = SessionVariablen.getInstance().getFussballerId();
			int spieltag = SessionVariablen.getInstance().getSpieltag();
			
			befehl = "UPDATE prioritaeten SET prioritaet = " + prio + " WHERE mitspieler_id = " + mitspielerId
					+ " AND fussballer_id = " + fussballerId + " AND spieltag = " + spieltag + ";";
			(SessionVariablen.getInstance().getStmt()).executeUpdate(befehl);
		}
		catch (SQLException e) {
			throw new DBException(befehl, e.getMessage());
		}
	}

	/**
	 * Aktualisiert eine Zeile in der Tabelle Einstellungen
	 */
	public static void updateTable_Einstellung(String key, String value) throws DBException {
		
		String befehl = null;
		try {
			befehl = "UPDATE Einstellungen SET einstellung_wert = '" + value + "' WHERE einstellung_id = '" + key + "';";
			(SessionVariablen.getInstance().getStmt()).executeUpdate(befehl);
		}
		catch (SQLException e) {
			throw new DBException(befehl, e.getMessage());
		}
	}
}