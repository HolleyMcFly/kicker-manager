package de.managerspiel.db;

import java.sql.SQLException;

import org.eclipse.swt.widgets.Shell;

import de.managerspiel.exception.DBException;
import de.managerspiel.model.Auswertungseintrag;
import de.managerspiel.util.SessionVariablen;

public class DBInsert {

	/**
	 * F�gt eine Zeile in die Tabelle Tippspiel ein
	 */
	public static void insertTable_Tippspiel(String name, Shell shell) throws DBException {

		String befehl = null;
		try {
			befehl = "INSERT INTO tippspiel (tippspiel_name) VALUES ('" + name + "')";
			(SessionVariablen.getInstance().getStmt()).executeUpdate(befehl);

			// Wenn neu angelegt, ID f�r weitere Verarbeitung merken
			int key = DBUtil.getNewKey();
			SessionVariablen.getInstance().setTippspielId(key);
		}
		catch (SQLException e) {
			throw new DBException(befehl, e.getMessage());
		}
	}

	/**
	 * F�gt eine Zeile in die Tabelle Mitspieler ein
	 */
	public static void insertTable_Mitspieler(String name, String email, Shell shell) throws DBException {

		String befehl = null;
		try {
			int tippspielId = SessionVariablen.getInstance().getTippspielId();
			befehl = "INSERT INTO mitspieler (tippspiel_id, mitspieler_name, mitspieler_email) VALUES (" + tippspielId + ", '" + name
					+ "', '" + email + "')";
			(SessionVariablen.getInstance().getStmt()).executeUpdate(befehl);
		}
		catch (SQLException e) {
			throw new DBException(befehl, e.getMessage());
		}
	}

	/**
	 * F�gt eine Zeile in die Tabelle Verein ein
	 */
	public static void insertTable_Verein(String name, Shell shell) throws DBException {

		String befehl = null;
		try {
			befehl = "INSERT INTO verein (verein_name) VALUES ('" + name + "')";
			(SessionVariablen.getInstance().getStmt()).executeUpdate(befehl);

			// Wenn neu angelegt, ID f�r weitere Verarbeitung merken
			int key = DBUtil.getNewKey();
			SessionVariablen.getInstance().setVereinId(key);
		}
		catch (SQLException e) {
			throw new DBException(befehl, e.getMessage());
		}
	}

	/**
	 * F�gt eine Zeile in die Tabelle Fussballer ein
	 */
	public static void insertTable_Fussballer(String name, Shell shell, int position) throws DBException {

		String befehl = null;
		try {
			int vereinId = SessionVariablen.getInstance().getVereinId();
			int mitspielerId = SessionVariablen.getInstance().getMitspielerId();

			befehl = "INSERT INTO fussballer (fussballer_name, verein_id, spielposition) VALUES ('" + name + "', " + vereinId + ", "
					+ position + ")";
			(SessionVariablen.getInstance().getStmt()).executeUpdate(befehl);

			// Wenn neu angelegt, ID f�r weitere Verarbeitung merken
			int key = DBUtil.getNewKey();
			SessionVariablen.getInstance().setFussballerId(key);

			// Eintrag in Relationentabelle R_MS_FB erstellen
			befehl = "INSERT INTO R_MS_FB (mitspieler_id, fussballer_id) VALUES (" + mitspielerId + ", " + key + ");";
			(SessionVariablen.getInstance().getStmt()).executeUpdate(befehl);
		}
		catch (SQLException e) {
			throw new DBException(befehl, e.getMessage());
		}
	}

	/**
	 * F�gt eine Zeile in die Tabelle Priorit�ten ein
	 */
	public static void insertTable_Prioritaeten(int fussballerId, int prio, int spieltag) throws DBException {

		String befehl = null;
		try {
			int mitspielerId = SessionVariablen.getInstance().getMitspielerId();
			befehl = "INSERT INTO prioritaeten (mitspieler_id, fussballer_id, spieltag, prioritaet) VALUES (" + mitspielerId + ", "
					+ fussballerId + ", " + spieltag + ", " + prio + ")";
			(SessionVariablen.getInstance().getStmt()).executeUpdate(befehl);
		}
		catch (SQLException e) {
			throw new DBException(befehl, e.getMessage());
		}
	}

	/**
	 * F�gt eine Zeile in die Tabelle Punktekriterien ein
	 */
	public static void insertTable_Punktekriterien(Auswertungseintrag ausw) throws DBException {

		String befehl = null;
		try {
			int fussballerId = DBRead.getFussballerID(ausw.getFussballername());
			int spieltag = SessionVariablen.getInstance().getSpieltag();

			befehl = "INSERT INTO punktekriterien (fussballer_id, spieltag, anzahl_tore, gelbrote_karte, rote_karte, note, gespielt) VALUES ("
					+ fussballerId
					+ ", "
					+ spieltag
					+ ", "
					+ ausw.getAnzahl_tore()
					+ ", "
					+ ausw.isGelbrote_karte()
					+ ", "
					+ ausw.isRote_karte() + ", " + ausw.getNote() + ", " + ausw.isGespielt() + ")";
			(SessionVariablen.getInstance().getStmt()).executeUpdate(befehl);
		}
		catch (SQLException e) {
			throw new DBException(befehl, e.getMessage());
		}
	}

	/**
	 * F�gt einen Eintrag in die Tabelle Einstellungen ein.
	 */
	public static void insertTable_Einstellungen(String key, String value) throws DBException {

		String befehl = null;
		try {
			befehl = "INSERT INTO Einstellungen (einstellung_id, einstellung_wert) VALUES ('" + key + "', '" + value + "')";
			(SessionVariablen.getInstance().getStmt()).executeUpdate(befehl);
		}
		catch (SQLException e) {
			throw new DBException(befehl, e.getMessage());
		}
	}
}
