package de.managerspiel.windows;

import java.awt.Dimension;
import java.awt.Toolkit;

import org.eclipse.swt.SWT;
import org.eclipse.swt.events.SelectionEvent;
import org.eclipse.swt.events.SelectionListener;
import org.eclipse.swt.layout.GridData;
import org.eclipse.swt.layout.GridLayout;
import org.eclipse.swt.widgets.Button;
import org.eclipse.swt.widgets.Display;
import org.eclipse.swt.widgets.Label;
import org.eclipse.swt.widgets.Shell;

import de.managerspiel.util.Konstanten;
import de.managerspiel.util.SessionVariablen;
import de.managerspiel.util.Util;

/**
 * Erstellt den Dialog f�r den Men�punkt "Info --> �ber..."
 */
public class WinInfo {

	public void createWindowInfo(Display display) {

		final Shell shellInfo = Util.createShell(display, "�ber...", Konstanten.WINDOW_INFO_BREITE, Konstanten.WINDOW_INFO_HOEHE);

		// Layout setzen
		GridLayout gridlInfo = new GridLayout();
		gridlInfo.verticalSpacing = 10;
		gridlInfo.marginHeight = 20;
		gridlInfo.marginWidth = 20;
		gridlInfo.numColumns = 2;
		gridlInfo.makeColumnsEqualWidth = false;
		shellInfo.setLayout(gridlInfo);

		// Beschriftung f�r Programmname
		Label labelInfo = new Label(shellInfo, SWT.LEFT);
		labelInfo.setText("Programm:");
		labelInfo.setLayoutData(new GridData(GridData.HORIZONTAL_ALIGN_FILL));
		labelInfo = new Label(shellInfo, SWT.LEFT);
		labelInfo.setText("Kicker - Managerspiel");
		labelInfo.setLayoutData(new GridData(GridData.HORIZONTAL_ALIGN_FILL));

		// Beschriftung f�r Programmversion
		labelInfo = new Label(shellInfo, SWT.LEFT);
		labelInfo.setText("Version:");
		labelInfo.setLayoutData(new GridData(GridData.HORIZONTAL_ALIGN_FILL));
		labelInfo = new Label(shellInfo, SWT.LEFT);
		labelInfo.setText("V. 1.1");
		labelInfo.setLayoutData(new GridData(GridData.HORIZONTAL_ALIGN_FILL));

		// Beschriftung f�r Autoren
		labelInfo = new Label(shellInfo, SWT.LEFT);
		labelInfo.setText("Autoren:");
		labelInfo.setLayoutData(new GridData(GridData.HORIZONTAL_ALIGN_FILL));
		labelInfo = new Label(shellInfo, SWT.LEFT);
		labelInfo.setText("Holger Herrmann");
		labelInfo.setLayoutData(new GridData(GridData.HORIZONTAL_ALIGN_END));

		GridData gridAutor2 = new GridData(GridData.HORIZONTAL_ALIGN_END);
		gridAutor2.horizontalSpan = 2;
		labelInfo = new Label(shellInfo, SWT.LEFT);
		labelInfo.setText(" Jochen Herrmann");
		labelInfo.setLayoutData(gridAutor2);

		// Fenster-Schlie�en-Button
		Button buttonSchliessen = new Button(shellInfo, SWT.FULL_SELECTION);
		buttonSchliessen.setText("OK");
		GridData gridNeu = new GridData(GridData.HORIZONTAL_ALIGN_FILL);
		gridNeu.horizontalSpan = 2;
		buttonSchliessen.setLayoutData(gridNeu);
		buttonSchliessen.addSelectionListener(new SelectionListener() {

			public void widgetSelected(SelectionEvent arg0) {
				shellInfo.close();
				(SessionVariablen.getInstance().getShell()).setVisible(true);
			}

			public void widgetDefaultSelected(SelectionEvent arg0) {}
		});

		// Position des Fensters festlegen
		Dimension d = Toolkit.getDefaultToolkit().getScreenSize();
		shellInfo.setLocation((d.width - Konstanten.WINDOW_INFO_BREITE) / 2, (d.height - Konstanten.WINDOW_INFO_HOEHE) / 2);

		// Fenster anzeigen
		(SessionVariablen.getInstance().getShell()).setVisible(false);
		shellInfo.open();
	}
}
