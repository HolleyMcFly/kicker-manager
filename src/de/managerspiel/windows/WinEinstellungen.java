package de.managerspiel.windows;

import java.awt.Dimension;
import java.awt.Toolkit;

import org.eclipse.swt.SWT;
import org.eclipse.swt.events.SelectionEvent;
import org.eclipse.swt.events.SelectionListener;
import org.eclipse.swt.layout.GridData;
import org.eclipse.swt.layout.GridLayout;
import org.eclipse.swt.layout.RowData;
import org.eclipse.swt.layout.RowLayout;
import org.eclipse.swt.widgets.Button;
import org.eclipse.swt.widgets.DirectoryDialog;
import org.eclipse.swt.widgets.Display;
import org.eclipse.swt.widgets.Group;
import org.eclipse.swt.widgets.Label;
import org.eclipse.swt.widgets.Shell;

import de.managerspiel.db.DBInsert;
import de.managerspiel.db.DBUpdate;
import de.managerspiel.exception.DBException;
import de.managerspiel.util.Konstanten;
import de.managerspiel.util.SessionVariablen;
import de.managerspiel.util.Util;

public class WinEinstellungen {

	/**
	 * Erstellt das Fenster f�r den Men�punkt "Info --> Einstellungen"
	 */
	public void createWindowEinstellungen(Display display) {

		// Neues Fenster erstellen
		final Shell shellEinstellungen = Util.createShell(display, "Einstellungen", Konstanten.WINDOW_EINSTELLUNGEN_BREITE,
				Konstanten.WINDOW_EINSTELLUNGEN_HOEHE);

		// Layout setzen
		GridLayout gridlEinstellungen = new GridLayout();
		gridlEinstellungen.horizontalSpacing = 10;
		gridlEinstellungen.verticalSpacing = 10;
		gridlEinstellungen.marginHeight = 20;
		gridlEinstellungen.marginWidth = 20;
		gridlEinstellungen.numColumns = 2;
		gridlEinstellungen.makeColumnsEqualWidth = false;
		shellEinstellungen.setLayout(gridlEinstellungen);

		GridData grd2 = new GridData(GridData.HORIZONTAL_ALIGN_FILL);
		grd2.horizontalSpan = 2;

		// Gruppe f�r die Ausgabedatei
		Group grPath = new Group(shellEinstellungen, SWT.LEFT);
		grPath.setLayout(new RowLayout());
		grPath.setText("Ausgabepfad:");
		grPath.setLayoutData(grd2);

		// Anzeige des Ausgabepfades
		String ausgabePfad = SessionVariablen.getInstance().getEinstellung(Konstanten.AUSGABEPFAD);
		if (ausgabePfad == null)
			ausgabePfad = "nicht gesetzt";

		final Label pathLabel = new Label(grPath, SWT.LEFT);
		pathLabel.setText(ausgabePfad);
		pathLabel.setLayoutData(new RowData(310, 20));

		// Button, um den Pfad f�r die Ausgabedatei zu �ndern
		Button btnPath = new Button(grPath, SWT.PUSH);
		btnPath.setText("�ndern");
		btnPath.addSelectionListener(new SelectionListener() {

			public void widgetSelected(SelectionEvent arg0) {
				String strPath = "";

				DirectoryDialog objDir = new DirectoryDialog(shellEinstellungen);
				objDir.setMessage("Ausgabeverzeichnis ausw�hlen");
				strPath = objDir.open();

				if (strPath == null)
					return;

				if (!strPath.endsWith("\\"))
					strPath = strPath + "\\";
				pathLabel.setText(strPath);
			}

			public void widgetDefaultSelected(SelectionEvent arg0) {

			}
		});

		// Speichern-Button
		Button buttonSpeichern = new Button(shellEinstellungen, SWT.FULL_SELECTION);
		buttonSpeichern.setText("Speichern");
		buttonSpeichern.setLayoutData(new GridData(GridData.HORIZONTAL_ALIGN_FILL));
		buttonSpeichern.addSelectionListener(new SelectionListener() {

			public void widgetSelected(SelectionEvent arg0) {

				String neuerPfad = pathLabel.getText();

				try {
					if (Util.einstellungExists(Konstanten.AUSGABEPFAD))
						DBUpdate.updateTable_Einstellung(Konstanten.AUSGABEPFAD, neuerPfad);
					else
						DBInsert.insertTable_Einstellungen(Konstanten.AUSGABEPFAD, neuerPfad);
				}
				catch (DBException e) {
					e.handleError();
					return;
				}

				SessionVariablen.getInstance().addEinstellung(Konstanten.AUSGABEPFAD, neuerPfad);

				shellEinstellungen.close();
				(SessionVariablen.getInstance().getShell()).setVisible(true);
			}

			public void widgetDefaultSelected(SelectionEvent arg0) {
				;
			}
		});

		// Position des Fensters festlegen
		Dimension d = Toolkit.getDefaultToolkit().getScreenSize();
		shellEinstellungen.setLocation((d.width - Konstanten.WINDOW_EINSTELLUNGEN_BREITE) / 2,
				(d.height - Konstanten.WINDOW_EINSTELLUNGEN_HOEHE) / 2);

		// Fenster jetzt anzeigen
		(SessionVariablen.getInstance().getShell()).setVisible(false);
		shellEinstellungen.open();
	}
}
