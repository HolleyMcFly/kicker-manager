package de.managerspiel.windows;

import java.awt.Dimension;
import java.awt.Toolkit;

import org.eclipse.swt.SWT;
import org.eclipse.swt.events.SelectionEvent;
import org.eclipse.swt.events.SelectionListener;
import org.eclipse.swt.layout.GridData;
import org.eclipse.swt.layout.GridLayout;
import org.eclipse.swt.widgets.Button;
import org.eclipse.swt.widgets.Display;
import org.eclipse.swt.widgets.Group;
import org.eclipse.swt.widgets.Label;
import org.eclipse.swt.widgets.List;
import org.eclipse.swt.widgets.Shell;
import org.eclipse.swt.widgets.Text;

import de.managerspiel.db.DBInsert;
import de.managerspiel.db.DBRead;
import de.managerspiel.exception.DBException;
import de.managerspiel.util.Konstanten;
import de.managerspiel.util.SessionVariablen;
import de.managerspiel.util.Util;

public class WinNeu {

	/**
	 * Erstellt das Fenster f�r den Men�punkt "Neu --> Mitspieler"
	 */
	public void createWindowNeuMitspieler(Display display) {

		final Shell shellNeuMitspieler = Util.createShell(display, "Neuen Mitspieler aufnehmen", Konstanten.WINDOW_NEU_MITSPIELER_BREITE,
				Konstanten.WINDOW_NEU_MITSPIELER_HOEHE);

		// Layout setzen
		GridLayout gridlNeuMitspieler = new GridLayout();
		gridlNeuMitspieler.verticalSpacing = 10;
		gridlNeuMitspieler.marginHeight = 20;
		gridlNeuMitspieler.marginWidth = 20;
		gridlNeuMitspieler.numColumns = 2;
		gridlNeuMitspieler.makeColumnsEqualWidth = false;
		shellNeuMitspieler.setLayout(gridlNeuMitspieler);

		// Eingabefeld f�r Mitspieler-Name
		Label labelName = new Label(shellNeuMitspieler, SWT.LEFT);
		labelName.setText("Name des neuen Mitspielers:");
		GridData grid_neu = new GridData(GridData.HORIZONTAL_ALIGN_FILL);
		grid_neu.horizontalSpan = 2;
		labelName.setLayoutData(grid_neu);
		final Text textName = new Text(shellNeuMitspieler, SWT.LEFT);
		textName.setFocus();
		textName.setTextLimit(50);
		textName.setLayoutData(grid_neu);

		// Eingabefeld f�r Mitspieler-Email
		Label labelEmail = new Label(shellNeuMitspieler, SWT.LEFT);
		labelEmail.setText("Email-Adresse:");
		GridData gridMail = new GridData(GridData.HORIZONTAL_ALIGN_FILL);
		gridMail.horizontalSpan = 2;
		labelEmail.setLayoutData(gridMail);
		final Text textEmail = new Text(shellNeuMitspieler, SWT.LEFT);
		textEmail.setFocus();
		textEmail.setTextLimit(50);
		textEmail.setLayoutData(grid_neu);

		// Erstellen-Button
		Button buttonErstellen = new Button(shellNeuMitspieler, SWT.FULL_SELECTION);
		buttonErstellen.setText("Erstellen");
		buttonErstellen.setLayoutData(new GridData(GridData.HORIZONTAL_ALIGN_FILL));
		buttonErstellen.addSelectionListener(new SelectionListener() {

			public void widgetSelected(SelectionEvent arg0) {
				// Tippspiel in DB eintragen
				try {
					DBInsert.insertTable_Mitspieler(textName.getText(), textEmail.getText(), (SessionVariablen.getInstance().getShell()));
				}
				catch (DBException e) {
					e.handleError();
					return;
				}

				Util.showMessage("Fertig", "Der Mitspieler wurde angelegt.");
				textName.setText("");
				textEmail.setText("");
				textName.setFocus();
			}

			public void widgetDefaultSelected(SelectionEvent arg0) {
				;
			}
		});

		// Fertig-Button
		Button buttonFertig = new Button(shellNeuMitspieler, SWT.FULL_SELECTION);
		buttonFertig.setText("Fertig");
		buttonFertig.setLayoutData(new GridData(GridData.HORIZONTAL_ALIGN_FILL));
		buttonFertig.addSelectionListener(new SelectionListener() {

			public void widgetSelected(SelectionEvent arg0) {
				shellNeuMitspieler.close();
				(SessionVariablen.getInstance().getShell()).setVisible(true);
			}

			public void widgetDefaultSelected(SelectionEvent arg0) {
				;
			}
		});

		textName.setFocus();

		// Position des Fensters festlegen
		Dimension d = Toolkit.getDefaultToolkit().getScreenSize();
		shellNeuMitspieler.setLocation((d.width - Konstanten.WINDOW_NEU_MITSPIELER_BREITE) / 2,
				(d.height - Konstanten.WINDOW_NEU_MITSPIELER_HOEHE) / 2);

		// Fenster jetzt anzeigen
		(SessionVariablen.getInstance().getShell()).setVisible(false);
		shellNeuMitspieler.open();
	}

	/**
	 * Erstellt das Fenster f�r den Men�punkt "Neu --> Tippspiel"
	 */
	public void createWindowNeuTippspiel(Display display) {

		// Neues Fenster erstellen
		final Shell shellNeuTipp = Util.createShell(display, "Neues Tippspiel erstellen", Konstanten.WINDOW_NEU_TIPPSPIEL_BREITE,
				Konstanten.WINDOW_NEU_TIPPSPIEL_HOEHE);

		// Layout setzen
		GridLayout gridlNeuTipp = new GridLayout();
		gridlNeuTipp.verticalSpacing = 10;
		gridlNeuTipp.marginHeight = 20;
		gridlNeuTipp.marginWidth = 20;
		gridlNeuTipp.numColumns = 2;
		gridlNeuTipp.makeColumnsEqualWidth = false;
		shellNeuTipp.setLayout(gridlNeuTipp);

		// Eingabefeld f�r Tippspiel-Name
		Label labelName = new Label(shellNeuTipp, SWT.LEFT);
		labelName.setText("Name des neuen Tippspiels:");
		GridData gridNeu = new GridData(GridData.HORIZONTAL_ALIGN_FILL);
		gridNeu.horizontalSpan = 2;
		labelName.setLayoutData(gridNeu);
		final Text textName = new Text(shellNeuTipp, SWT.LEFT);
		textName.setFocus();
		textName.setTextLimit(20);
		textName.setLayoutData(gridNeu);

		// Erstellen-Button
		Button buttonErstellen = new Button(shellNeuTipp, SWT.FULL_SELECTION);
		buttonErstellen.setText("Erstellen");
		buttonErstellen.setLayoutData(new GridData(GridData.HORIZONTAL_ALIGN_FILL));
		buttonErstellen.addSelectionListener(new SelectionListener() {

			public void widgetSelected(SelectionEvent arg0) {

				// Tippspiel in DB eintragen
				try {
					DBInsert.insertTable_Tippspiel(textName.getText(), (SessionVariablen.getInstance().getShell()));
				}
				catch (DBException e) {
					e.handleError();
					return;
				}

				// Daten �bernehmen
				SessionVariablen.getInstance().setTippspielname(textName.getText());
				(SessionVariablen.getInstance().getNamelabel()).setText(SessionVariablen.getInstance().getTippspielname());

				// Richtiges Fenster anzeigen
				shellNeuTipp.close();
				(SessionVariablen.getInstance().getShell()).setVisible(true);
			}

			public void widgetDefaultSelected(SelectionEvent arg0) {
				;
			}
		});

		// Abbrechen-Button
		Button buttonAbbruch = new Button(shellNeuTipp, SWT.FULL_SELECTION);
		buttonAbbruch.setText("Abbrechen");
		buttonAbbruch.setLayoutData(new GridData(GridData.HORIZONTAL_ALIGN_FILL));
		buttonAbbruch.addSelectionListener(new SelectionListener() {

			public void widgetSelected(SelectionEvent arg0) {
				shellNeuTipp.close();
				(SessionVariablen.getInstance().getShell()).setVisible(true);
			}

			public void widgetDefaultSelected(SelectionEvent arg0) {
				;
			}
		});

		// Position des Fensters festlegen
		Dimension d = Toolkit.getDefaultToolkit().getScreenSize();
		shellNeuTipp.setLocation((d.width - Konstanten.WINDOW_NEU_TIPPSPIEL_BREITE) / 2,
				(d.height - Konstanten.WINDOW_NEU_TIPPSPIEL_HOEHE) / 2);

		// Fenster jetzt anzeigen
		(SessionVariablen.getInstance().getShell()).setVisible(false);
		shellNeuTipp.open();
	}

	/**
	 * Erstellt das Fenster f�r den Men�punkt "Verwaltung --> Vereine verwalten"
	 */
	public void createWindowNeuFussballer(Display display) {

		// Neues Fenster erstellen
		final Shell shellNeuFussballer = Util.createShell(display, "Neuen Fu�baller hinzuf�gen", Konstanten.WINDOW_NEU_FUSSBALLER_BREITE,
				Konstanten.WINDOW_NEU_FUSSBALLER_HOEHE);

		// Layout setzen
		GridLayout gridlNeuFussballer = new GridLayout();
		gridlNeuFussballer.horizontalSpacing = 10;
		gridlNeuFussballer.verticalSpacing = 10;
		gridlNeuFussballer.marginHeight = 20;
		gridlNeuFussballer.marginWidth = 20;
		gridlNeuFussballer.numColumns = 3;
		gridlNeuFussballer.makeColumnsEqualWidth = false;
		shellNeuFussballer.setLayout(gridlNeuFussballer);

		// Text f�r die Auswahlliste der vorhandenen Mitspieler
		Label labelMitspieler = new Label(shellNeuFussballer, SWT.LEFT);
		labelMitspieler.setText("Vorhandene Mitspieler:");
		labelMitspieler.setLayoutData(new GridData(GridData.HORIZONTAL_ALIGN_FILL));

		// Text f�r die Auswahlliste der vorhandenen Fu�baller
		Label labelFussballer = new Label(shellNeuFussballer, SWT.LEFT);
		labelFussballer.setText("Vorhandene Fu�baller:");
		labelFussballer.setLayoutData(new GridData(GridData.HORIZONTAL_ALIGN_FILL));

		// Dummy-Label
		Label labelDummy = new Label(shellNeuFussballer, SWT.LEFT);
		labelDummy.setText("");

		// Auswahlliste der vorhandenen Mitspieler
		final List listMitspieler = new List(shellNeuFussballer, SWT.V_SCROLL);

		try {
			Util.fill_Mitspielerliste_TS(listMitspieler);
		}
		catch (DBException e) {
			e.handleError();
			return;
		}

		GridData gridMitspieler = new GridData(GridData.HORIZONTAL_ALIGN_FILL);
		gridMitspieler.heightHint = 250;
		gridMitspieler.widthHint = 150;
		gridMitspieler.verticalSpan = 2;
		listMitspieler.setLayoutData(gridMitspieler);

		listMitspieler.addSelectionListener(new SelectionListener() {

			public void widgetSelected(SelectionEvent arg0) {
				// Liste leeren
				SessionVariablen.getInstance().getListFussballer().removeAll();
				SessionVariablen.getInstance().setFussballerId(-1);

				int id = 0;
				String[] gewaehlt = new String[10];

				// Gew�hlten Mitspieler auslesen
				gewaehlt = listMitspieler.getSelection();

				// ID des Eintrags holen
				try {
					id = DBRead.getMitspielerID(gewaehlt[0]);
				}
				catch (DBException e) {
					e.handleError();
				}

				SessionVariablen.getInstance().setMitspielerId(id);

				// Fu�ballerliste einlesen
				if (!Util.fill_Fussballerliste_MS(SessionVariablen.getInstance().getListFussballer())) {
					Util.showMessage("Fehler", "Fehler beim F�llen der Fu�ballerliste!");
				}
			}

			public void widgetDefaultSelected(SelectionEvent arg0) {
				;
			}
		});

		SessionVariablen.getInstance().setListFussballer(new List(shellNeuFussballer, SWT.V_SCROLL));
		GridData gridFussballer = new GridData(GridData.HORIZONTAL_ALIGN_FILL);
		gridFussballer.heightHint = 250;
		gridFussballer.widthHint = 150;
		gridFussballer.verticalSpan = 2;
		SessionVariablen.getInstance().getListFussballer().setLayoutData(gridFussballer);

		// Rahmen f�r Eingabe neuer Fu�baller
		Group grpNeu = new Group(shellNeuFussballer, SWT.NONE);
		grpNeu.setText("Neuen Fu�baller hinzuf�gen");
		GridLayout gridlGroup = new GridLayout();
		gridlGroup.numColumns = 4;
		gridlGroup.makeColumnsEqualWidth = true;
		grpNeu.setLayout(gridlGroup);
		grpNeu.setLayoutData(new GridData(GridData.HORIZONTAL_ALIGN_FILL));

		// Dummy-Label f�r Leerzeile
		labelDummy = new Label(grpNeu, SWT.TOP);
		labelDummy = new Label(grpNeu, SWT.TOP);
		labelDummy = new Label(grpNeu, SWT.TOP);
		labelDummy = new Label(grpNeu, SWT.TOP);

		// Eingabefeld f�r Fu�baller-Name
		Label labelName = new Label(grpNeu, SWT.TOP);
		labelName.setText("Name:");
		final Text textName = new Text(grpNeu, SWT.LEFT);
		textName.setTextLimit(50);
		GridData gridName = new GridData(GridData.HORIZONTAL_ALIGN_FILL);
		gridName.horizontalSpan = 3;
		textName.setLayoutData(gridName);

		// Text f�r die Auswahlliste der vorhandenen Vereine
		Label labelVerein = new Label(grpNeu, SWT.LEFT);
		labelVerein.setText("Verein:");
		labelVerein.setLayoutData(new GridData(GridData.HORIZONTAL_ALIGN_FILL));

		// Auswahlliste der vorhandenen Vereine
		final List listVereine = new List(grpNeu, SWT.V_SCROLL);
		GridData gridVereine = new GridData(GridData.HORIZONTAL_ALIGN_FILL);
		gridVereine.heightHint = 120;
		gridVereine.widthHint = 50;
		gridVereine.horizontalSpan = 3;
		listVereine.setLayoutData(gridVereine);

		// Vereinsliste einlesen
		if (!Util.fill_Vereinsliste(listVereine)) {
			Util.showMessage("Fehler", "Fehler beim F�llen der Vereinsliste!");
		}

		// Text f�r die Auswahlliste der Position
		Label labelPosition = new Label(grpNeu, SWT.LEFT);
		labelPosition.setText("Position:");
		labelPosition.setLayoutData(new GridData(GridData.HORIZONTAL_ALIGN_FILL));

		// Auswahlliste der Positionen
		final List listPositionen = new List(grpNeu, SWT.V_SCROLL);
		GridData gridPositionen = new GridData(GridData.HORIZONTAL_ALIGN_FILL);
		gridPositionen.heightHint = 54;
		gridPositionen.widthHint = 50;
		gridPositionen.horizontalSpan = 3;
		listPositionen.setLayoutData(gridPositionen);

		// Positionsliste f�llen
		Util.fill_Positionsliste(listPositionen);

		// Dummy-Label f�r Leerzeile
		labelDummy = new Label(grpNeu, SWT.TOP);
		labelDummy = new Label(grpNeu, SWT.TOP);
		labelDummy = new Label(grpNeu, SWT.TOP);
		labelDummy = new Label(grpNeu, SWT.TOP);

		// Hinzuf�gen-Button
		Button buttonHinzufuegen = new Button(grpNeu, SWT.CENTER);
		buttonHinzufuegen.setText("Hinzuf�gen");
		GridData gridButton = new GridData(GridData.HORIZONTAL_ALIGN_FILL);
		gridButton.horizontalSpan = 2;
		buttonHinzufuegen.setLayoutData(gridButton);
		buttonHinzufuegen.addSelectionListener(new SelectionListener() {

			public void widgetSelected(SelectionEvent arg0) {
				// Kein Mitspieler geladen
				if (listMitspieler.getSelectionIndex() == -1) {
					Util.showMessage("Fehler", "Bitte w�hlen Sie einen Mitspieler!");
					return;
				}

				// Kein Verein ausgew�hlt
				if (listVereine.getSelectionIndex() == -1) {
					Util.showMessage("Fehler", "Bitte w�hlen Sie einen Verein!");
					return;
				}

				// Keine Position ausgew�hlt
				if (listPositionen.getSelectionIndex() == -1) {
					Util.showMessage("Fehler", "Bitte w�hlen Sie eine Spielposition!");
					return;
				}

				// Kein Name eingetragen
				if ("".equalsIgnoreCase(textName.getText())) {
					Util.showMessage("Fehler", "Bitte erfassen Sie einen Fu�ballernamen!");
					return;
				}

				int id = 0;
				String[] gewaehlt = new String[10];

				// Gew�hlten Verein auslesen
				gewaehlt = listVereine.getSelection();

				// ID des Eintrags holen
				try {
					id = DBRead.getVereinID(gewaehlt[0]);
				}
				catch (DBException e) {
					e.handleError();
				}

				SessionVariablen.getInstance().setVereinId(id);

				// Fu�baller in DB eintragen
				try {
					DBInsert.insertTable_Fussballer(textName.getText(), (SessionVariablen.getInstance().getShell()),
							listPositionen.getSelectionIndex());
				}
				catch (DBException e) {
					e.handleError();
					return;
				}

				// Daten �bernehmen
				SessionVariablen.getInstance().getListFussballer().add(textName.getText());
				textName.setText("");
				textName.setFocus();
				SessionVariablen.getInstance().setVereinId(-1);
			}

			public void widgetDefaultSelected(SelectionEvent arg0) {
				;
			}
		});

		// Dummy-Label
		labelDummy = new Label(shellNeuFussballer, SWT.TOP);
		labelDummy = new Label(shellNeuFussballer, SWT.TOP);

		// Fertig-Button
		Button buttonFertig = new Button(shellNeuFussballer, SWT.FULL_SELECTION);
		buttonFertig.setText("Fertig");
		buttonFertig.setLayoutData(new GridData(GridData.HORIZONTAL_ALIGN_FILL));
		buttonFertig.addSelectionListener(new SelectionListener() {

			public void widgetSelected(SelectionEvent arg0) {
				shellNeuFussballer.close();
				(SessionVariablen.getInstance().getShell()).setVisible(true);
			}

			public void widgetDefaultSelected(SelectionEvent arg0) {
				;
			}
		});

		// Position des Fensters festlegen
		Dimension d = Toolkit.getDefaultToolkit().getScreenSize();
		shellNeuFussballer.setLocation((d.width - Konstanten.WINDOW_NEU_FUSSBALLER_BREITE) / 2,
				(d.height - Konstanten.WINDOW_NEU_FUSSBALLER_HOEHE) / 2);

		// Fenster jetzt anzeigen
		(SessionVariablen.getInstance().getShell()).setVisible(false);
		shellNeuFussballer.open();
	}
}
