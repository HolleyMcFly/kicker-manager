package de.managerspiel.windows;

import java.awt.Dimension;
import java.awt.Toolkit;

import org.eclipse.swt.SWT;
import org.eclipse.swt.events.SelectionEvent;
import org.eclipse.swt.events.SelectionListener;
import org.eclipse.swt.layout.GridData;
import org.eclipse.swt.layout.GridLayout;
import org.eclipse.swt.widgets.Button;
import org.eclipse.swt.widgets.Display;
import org.eclipse.swt.widgets.Group;
import org.eclipse.swt.widgets.Label;
import org.eclipse.swt.widgets.List;
import org.eclipse.swt.widgets.Shell;
import org.eclipse.swt.widgets.Text;

import de.managerspiel.db.DBRead;
import de.managerspiel.db.DBUpdate;
import de.managerspiel.exception.DBException;
import de.managerspiel.util.Konstanten;
import de.managerspiel.util.SessionVariablen;
import de.managerspiel.util.Util;

public class WinBearbeiten {

	/**
	 * Erstellt das Fenster f�r den Men�punkt "Bearbeiten --> Tippspiel"
	 */
	public void createWindowTippspielBearbeiten(Display display) {

		final Shell shellBearbeitentippspiel = Util.createShell(display, "Tippspiel bearbeiten",
				Konstanten.WINDOW_BEARBEITEN_TIPPSPIEL_BREITE, Konstanten.WINDOW_BEARBEITEN_TIPPSPIEL_HOEHE);

		// Layout setzen
		GridLayout gridlBearbeitentippspiel = new GridLayout();
		gridlBearbeitentippspiel.horizontalSpacing = 10;
		gridlBearbeitentippspiel.verticalSpacing = 10;
		gridlBearbeitentippspiel.marginHeight = 20;
		gridlBearbeitentippspiel.marginWidth = 20;
		gridlBearbeitentippspiel.numColumns = 2;
		gridlBearbeitentippspiel.makeColumnsEqualWidth = false;
		shellBearbeitentippspiel.setLayout(gridlBearbeitentippspiel);

		// Text f�r die Auswahlliste der vorhandenen Tippspiele
		Label labelTippspiele = new Label(shellBearbeitentippspiel, SWT.LEFT);
		labelTippspiele.setText("Vorhandene Tippspiele:");
		labelTippspiele.setLayoutData(new GridData(GridData.HORIZONTAL_ALIGN_FILL));

		// Dummy-Label
		Label labelDummy = new Label(shellBearbeitentippspiel, SWT.LEFT);
		labelDummy.setText("");

		// Auswahlliste der vorhandenen Tippspiele
		final List listTippspiele = new List(shellBearbeitentippspiel, SWT.V_SCROLL);

		try {
			Util.fill_Tippspielliste(listTippspiele);
		}
		catch (DBException re) {
			Util.showMessage("Fehler", "Fehler beim F�llen der Tippspielliste!");
			return;
		}

		GridData gridTippspiele = new GridData(GridData.HORIZONTAL_ALIGN_FILL);
		gridTippspiele.heightHint = 150;
		gridTippspiele.widthHint = 150;
		gridTippspiele.verticalSpan = 2;
		listTippspiele.setLayoutData(gridTippspiele);

		listTippspiele.addSelectionListener(new SelectionListener() {

			public void widgetSelected(SelectionEvent arg0) {
				String[] gewaehlt = new String[10];

				// Gew�hltes Tippspiel auslesen
				gewaehlt = listTippspiele.getSelection();

				// Name �bernehmen
				SessionVariablen.getInstance().getTextBearbeiten().setText(gewaehlt[0]);
			}

			public void widgetDefaultSelected(SelectionEvent arg0) {}
		});

		// Rahmen f�r die Bearbeitung der Tippspiele
		Group grpBearbeiten = new Group(shellBearbeitentippspiel, SWT.NONE);
		grpBearbeiten.setText("Tippspiel bearbeiten");
		GridLayout gridl_group = new GridLayout();
		gridl_group.numColumns = 4;
		gridl_group.makeColumnsEqualWidth = true;
		grpBearbeiten.setLayout(gridl_group);
		grpBearbeiten.setLayoutData(new GridData(GridData.HORIZONTAL_ALIGN_FILL));

		// Dummy-Label f�r Leerzeile
		labelDummy = new Label(grpBearbeiten, SWT.TOP);
		labelDummy = new Label(grpBearbeiten, SWT.TOP);
		labelDummy = new Label(grpBearbeiten, SWT.TOP);
		labelDummy = new Label(grpBearbeiten, SWT.TOP);

		// Eingabefeld f�r Tippspiel-Name
		Label label_name = new Label(grpBearbeiten, SWT.TOP);
		label_name.setText("Name:");
		SessionVariablen.getInstance().setTextBearbeiten(new Text(grpBearbeiten, SWT.LEFT));
		SessionVariablen.getInstance().getTextBearbeiten().setTextLimit(50);
		GridData gridName = new GridData(GridData.HORIZONTAL_ALIGN_FILL);
		gridName.horizontalSpan = 3;
		SessionVariablen.getInstance().getTextBearbeiten().setLayoutData(gridName);

		// Speichern-Button
		Button buttonSpeichern = new Button(grpBearbeiten, SWT.CENTER);
		buttonSpeichern.setText("Speichern");
		GridData gridButton = new GridData(GridData.HORIZONTAL_ALIGN_FILL);
		gridButton.horizontalSpan = 2;
		buttonSpeichern.setLayoutData(gridButton);

		buttonSpeichern.addSelectionListener(new SelectionListener() {

			public void widgetSelected(SelectionEvent arg0) {
				if (listTippspiele.getSelectionIndex() == -1) {
					Util.showMessage("Fehler", "Bitte w�hlen Sie ein Tippspiel!");
					return;
				}

				if ("".equalsIgnoreCase(SessionVariablen.getInstance().getTextBearbeiten().getText())) {
					Util.showMessage("Fehler", "Bitte erfassen Sie einen Tippspielnamen!");
					return;
				}

				int id = 0;
				String[] gewaehlt = new String[10];

				// Gew�hltes Tippspiel auslesen
				gewaehlt = listTippspiele.getSelection();

				// ID des Eintrags holen
				try {
					id = DBRead.getTippspielID(gewaehlt[0]);
				}
				catch (DBException e) {
					e.handleError();
				}

				SessionVariablen.getInstance().setTippspielId(id);

				// Tippspiel in DB eintragen
				try {
					DBUpdate.updateTable_Tippspiel(id, SessionVariablen.getInstance().getTextBearbeiten().getText());
				}
				catch (DBException e) {
					e.handleError();
					return;
				}

				// Daten �bernehmen
				listTippspiele.removeAll();

				try {
					Util.fill_Tippspielliste(listTippspiele);
				}
				catch (DBException re) {
					re.handleError();
					return;
				}

				SessionVariablen.getInstance().getTextBearbeiten().setText("");
				SessionVariablen.getInstance().getTextBearbeiten().setFocus();
				SessionVariablen.getInstance().setTippspielId(-1);
			}

			public void widgetDefaultSelected(SelectionEvent arg0) {
				;
			}
		});

		// Dummy-Label
		labelDummy = new Label(shellBearbeitentippspiel, SWT.TOP);

		// Fertig-Button
		Button buttonFertig = new Button(shellBearbeitentippspiel, SWT.FULL_SELECTION);
		buttonFertig.setText("Fertig");
		buttonFertig.setLayoutData(new GridData(GridData.HORIZONTAL_ALIGN_FILL));
		buttonFertig.addSelectionListener(new SelectionListener() {

			public void widgetSelected(SelectionEvent arg0) {
				shellBearbeitentippspiel.close();
				(SessionVariablen.getInstance().getShell()).setVisible(true);
			}

			public void widgetDefaultSelected(SelectionEvent arg0) {
				;
			}
		});

		// Position des Fensters festlegen
		Dimension d = Toolkit.getDefaultToolkit().getScreenSize();
		shellBearbeitentippspiel.setLocation((d.width - Konstanten.WINDOW_BEARBEITEN_TIPPSPIEL_BREITE) / 2,
				(d.height - Konstanten.WINDOW_BEARBEITEN_TIPPSPIEL_HOEHE) / 2);

		// Fenster jetzt anzeigen
		(SessionVariablen.getInstance().getShell()).setVisible(false);
		shellBearbeitentippspiel.open();
	}

	/**
	 * Erstellt das Fenster f�r den Men�punkt "Bearbeiten --> Mitspieler"
	 */
	public void createWindowMitspielerBearbeiten(Display display) {

		// Neues Fenster erstellen
		final Shell shellBearbeitenmitspieler = Util.createShell(display, "Mitspieler bearbeiten",
				Konstanten.WINDOW_BEARBEITEN_MITSPIELER_BREITE, Konstanten.WINDOW_BEARBEITEN_MITSPIELER_HOEHE);

		// Layout setzen
		GridLayout gridlBearbeitenmitspieler = new GridLayout();
		gridlBearbeitenmitspieler.horizontalSpacing = 10;
		gridlBearbeitenmitspieler.verticalSpacing = 10;
		gridlBearbeitenmitspieler.marginHeight = 20;
		gridlBearbeitenmitspieler.marginWidth = 20;
		gridlBearbeitenmitspieler.numColumns = 2;
		gridlBearbeitenmitspieler.makeColumnsEqualWidth = false;
		shellBearbeitenmitspieler.setLayout(gridlBearbeitenmitspieler);

		// Text f�r die Auswahlliste der vorhandenen Mitspieler
		Label labelMitspieler = new Label(shellBearbeitenmitspieler, SWT.LEFT);
		labelMitspieler.setText("Vorhandene Mitspieler:");
		labelMitspieler.setLayoutData(new GridData(GridData.HORIZONTAL_ALIGN_FILL));

		// Dummy-Label
		Label labelDummy = new Label(shellBearbeitenmitspieler, SWT.LEFT);
		labelDummy.setText("");

		// Auswahlliste der vorhandenen Mitspieler
		final List list_mitspieler = new List(shellBearbeitenmitspieler, SWT.V_SCROLL);

		try {
			Util.fill_Mitspielerliste_TS(list_mitspieler);
		}
		catch (DBException e) {
			e.handleError();
			return;
		}

		GridData gridMitspieler = new GridData(GridData.HORIZONTAL_ALIGN_FILL);
		gridMitspieler.heightHint = 150;
		gridMitspieler.widthHint = 150;
		gridMitspieler.verticalSpan = 2;
		list_mitspieler.setLayoutData(gridMitspieler);

		list_mitspieler.addSelectionListener(new SelectionListener() {

			public void widgetSelected(SelectionEvent arg0) {
				String[] gewaehlt = new String[10];

				// Gew�hlten Mitspieler auslesen
				gewaehlt = list_mitspieler.getSelection();

				// Name �bernehmen
				SessionVariablen.getInstance().getTextBearbeiten().setText(gewaehlt[0]);
			}

			public void widgetDefaultSelected(SelectionEvent arg0) {
				;
			}
		});

		// Rahmen f�r die Bearbeitung der Mitspieler
		Group grpBearbeiten = new Group(shellBearbeitenmitspieler, SWT.NONE);
		grpBearbeiten.setText("Mitspieler bearbeiten");
		GridLayout gridlGroup = new GridLayout();
		gridlGroup.numColumns = 4;
		gridlGroup.makeColumnsEqualWidth = true;
		grpBearbeiten.setLayout(gridlGroup);
		grpBearbeiten.setLayoutData(new GridData(GridData.HORIZONTAL_ALIGN_FILL));

		// Dummy-Label f�r Leerzeile
		labelDummy = new Label(grpBearbeiten, SWT.TOP);
		labelDummy = new Label(grpBearbeiten, SWT.TOP);
		labelDummy = new Label(grpBearbeiten, SWT.TOP);
		labelDummy = new Label(grpBearbeiten, SWT.TOP);

		// Eingabefeld f�r Mitspieler-Name
		Label labelName = new Label(grpBearbeiten, SWT.TOP);
		labelName.setText("Name:");
		SessionVariablen.getInstance().setTextBearbeiten(new Text(grpBearbeiten, SWT.LEFT));
		SessionVariablen.getInstance().getTextBearbeiten().setTextLimit(50);
		GridData gridName = new GridData(GridData.HORIZONTAL_ALIGN_FILL);
		gridName.horizontalSpan = 3;
		SessionVariablen.getInstance().getTextBearbeiten().setLayoutData(gridName);

		// Speichern-Button
		Button buttonspeichern = new Button(grpBearbeiten, SWT.CENTER);
		buttonspeichern.setText("Speichern");
		GridData gridbutton = new GridData(GridData.HORIZONTAL_ALIGN_FILL);
		gridbutton.horizontalSpan = 2;
		buttonspeichern.setLayoutData(gridbutton);
		buttonspeichern.addSelectionListener(new SelectionListener() {

			public void widgetSelected(SelectionEvent arg0) {
				// Kein Mitspieler gew�hlt
				if (list_mitspieler.getSelectionIndex() == -1) {
					Util.showMessage("Fehler", "Bitte w�hlen Sie einen Mitspieler!");
					return;
				}

				// Kein Name eingetragen
				if ("".equalsIgnoreCase(SessionVariablen.getInstance().getTextBearbeiten().getText())) {
					Util.showMessage("Fehler", "Bitte erfassen Sie einen Mitspielernamen!");
					return;
				}

				int id = 0;
				String[] gewaehlt = new String[10];

				// Gew�hlten Mitspieler auslesen
				gewaehlt = list_mitspieler.getSelection();

				// ID des Eintrags holen
				try {
					id = DBRead.getMitspielerID(gewaehlt[0]);
				}
				catch (DBException e) {
					e.handleError();
				}

				SessionVariablen.getInstance().setMitspielerId(id);

				// Mitspieler in DB eintragen
				try {
					DBUpdate.updateTable_Mitspieler(id, SessionVariablen.getInstance().getTextBearbeiten().getText());
				}
				catch (DBException e) {
					e.handleError();
					return;
				}

				// Daten �bernehmen
				list_mitspieler.removeAll();

				try {
					Util.fill_Mitspielerliste_TS(list_mitspieler);
				}
				catch (DBException e) {
					e.handleError();
					return;
				}

				SessionVariablen.getInstance().getTextBearbeiten().setText("");
				SessionVariablen.getInstance().getTextBearbeiten().setFocus();
				SessionVariablen.getInstance().setMitspielerId(-1);
			}

			public void widgetDefaultSelected(SelectionEvent arg0) {
				;
			}
		});

		// Dummy-Label
		labelDummy = new Label(shellBearbeitenmitspieler, SWT.TOP);

		// Fertig-Button
		Button buttonfertig = new Button(shellBearbeitenmitspieler, SWT.FULL_SELECTION);
		buttonfertig.setText("Fertig");
		buttonfertig.setLayoutData(new GridData(GridData.HORIZONTAL_ALIGN_FILL));
		buttonfertig.addSelectionListener(new SelectionListener() {

			public void widgetSelected(SelectionEvent arg0) {
				shellBearbeitenmitspieler.close();
				(SessionVariablen.getInstance().getShell()).setVisible(true);
			}

			public void widgetDefaultSelected(SelectionEvent arg0) {}
		});

		// Position des Fensters festlegen
		Dimension d = Toolkit.getDefaultToolkit().getScreenSize();
		shellBearbeitenmitspieler.setLocation((d.width - Konstanten.WINDOW_BEARBEITEN_MITSPIELER_BREITE) / 2,
				(d.height - Konstanten.WINDOW_BEARBEITEN_MITSPIELER_HOEHE) / 2);

		// Fenster jetzt anzeigen
		(SessionVariablen.getInstance().getShell()).setVisible(false);
		shellBearbeitenmitspieler.open();
	}

	/**
	 * Erstellt das Fenster f�r den Men�punkt "Bearbeiten --> Fu�baller"
	 */
	public void createWindowFussballerBearbeiten(Display display) {
		// Neues Fenster erstellen
		final Shell shellBearbeitenfussballer = Util.createShell(display, "Fu�baller bearbeiten",
				Konstanten.WINDOW_BEARBEITEN_FUSSBALLER_BREITE, Konstanten.WINDOW_BEARBEITEN_FUSSBALLER_HOEHE);

		// Layout setzen
		GridLayout gridlBbearbeitenfussballer = new GridLayout();
		gridlBbearbeitenfussballer.horizontalSpacing = 10;
		gridlBbearbeitenfussballer.verticalSpacing = 10;
		gridlBbearbeitenfussballer.marginHeight = 20;
		gridlBbearbeitenfussballer.marginWidth = 20;
		gridlBbearbeitenfussballer.numColumns = 2;
		gridlBbearbeitenfussballer.makeColumnsEqualWidth = false;
		shellBearbeitenfussballer.setLayout(gridlBbearbeitenfussballer);

		// Text f�r die Auswahlliste der vorhandenen Fu�baller
		Label labelFussballer = new Label(shellBearbeitenfussballer, SWT.LEFT);
		labelFussballer.setText("Vorhandene Fu�baller:");
		labelFussballer.setLayoutData(new GridData(GridData.HORIZONTAL_ALIGN_FILL));

		// Dummy-Label
		Label labelDummy = new Label(shellBearbeitenfussballer, SWT.LEFT);
		labelDummy.setText("");

		// Auswahlliste der vorhandenen Fu�baller
		final List listFussballer = new List(shellBearbeitenfussballer, SWT.V_SCROLL);

		if (!Util.fill_Fussballerliste(listFussballer)) {
			Util.showMessage("Fehler", "Fehler beim F�llen der Fu�ballerliste!");
		}

		GridData gridFussballer = new GridData(GridData.HORIZONTAL_ALIGN_FILL);
		gridFussballer.heightHint = 150;
		gridFussballer.widthHint = 150;
		gridFussballer.verticalSpan = 2;
		listFussballer.setLayoutData(gridFussballer);

		listFussballer.addSelectionListener(new SelectionListener() {

			public void widgetSelected(SelectionEvent arg0) {
				String[] gewaehlt = new String[10];

				// Gew�hlten Fu�baller auslesen
				gewaehlt = listFussballer.getSelection();

				// Name �bernehmen
				SessionVariablen.getInstance().getTextBearbeiten().setText(gewaehlt[0]);

				// ID lesen
				int id = 0;
				try {
					id = DBRead.getFussballerID(gewaehlt[0]);
				}
				catch (DBException e) {
					e.handleError();
				}

				// Position lesen
				int position = 0;
				try {
					position = DBRead.getFussballerpositionID(id);
				}
				catch (DBException e) {
					e.handleError();
				}

				// Position in der Liste ausw�hlen
				SessionVariablen.getInstance().getListPositionen().select(position);
			}

			public void widgetDefaultSelected(SelectionEvent arg0) {
				;
			}
		});

		// Rahmen f�r die Bearbeitung der Fu�baller
		Group grpBearbeiten = new Group(shellBearbeitenfussballer, SWT.NONE);
		grpBearbeiten.setText("Fu�baller bearbeiten");
		GridLayout gridlGroup = new GridLayout();
		gridlGroup.numColumns = 4;
		gridlGroup.makeColumnsEqualWidth = true;
		grpBearbeiten.setLayout(gridlGroup);
		grpBearbeiten.setLayoutData(new GridData(GridData.HORIZONTAL_ALIGN_FILL));

		// Dummy-Label f�r Leerzeile
		labelDummy = new Label(grpBearbeiten, SWT.TOP);
		labelDummy = new Label(grpBearbeiten, SWT.TOP);
		labelDummy = new Label(grpBearbeiten, SWT.TOP);
		labelDummy = new Label(grpBearbeiten, SWT.TOP);

		// Eingabefeld f�r Fu�baller-Name
		Label labelName = new Label(grpBearbeiten, SWT.TOP);
		labelName.setText("Name:");
		SessionVariablen.getInstance().setTextBearbeiten(new Text(grpBearbeiten, SWT.LEFT));
		SessionVariablen.getInstance().getTextBearbeiten().setTextLimit(50);
		GridData gridName = new GridData(GridData.HORIZONTAL_ALIGN_FILL);
		gridName.horizontalSpan = 3;
		SessionVariablen.getInstance().getTextBearbeiten().setLayoutData(gridName);

		// Text f�r die Auswahlliste der Position
		Label label_position = new Label(grpBearbeiten, SWT.LEFT);
		label_position.setText("Position:");
		label_position.setLayoutData(new GridData(GridData.HORIZONTAL_ALIGN_FILL));

		// Auswahlliste der Positionen�
		SessionVariablen.getInstance().setListPositionen(new List(grpBearbeiten, SWT.V_SCROLL));
		GridData gridPositionen = new GridData(GridData.HORIZONTAL_ALIGN_FILL);
		gridPositionen.heightHint = 55;
		gridPositionen.widthHint = 50;
		gridPositionen.horizontalSpan = 3;
		SessionVariablen.getInstance().getListPositionen().setLayoutData(gridPositionen);

		// Positionsliste f�llen
		Util.fill_Positionsliste(SessionVariablen.getInstance().getListPositionen());

		// Speichern-Button
		Button buttonSpeichern = new Button(grpBearbeiten, SWT.CENTER);
		buttonSpeichern.setText("Speichern");
		GridData gridButton = new GridData(GridData.HORIZONTAL_ALIGN_FILL);
		gridButton.horizontalSpan = 2;
		buttonSpeichern.setLayoutData(gridButton);
		buttonSpeichern.addSelectionListener(new SelectionListener() {

			public void widgetSelected(SelectionEvent arg0) {
				// Kein Fu�baller gew�hlt
				if (listFussballer.getSelectionIndex() == -1) {
					Util.showMessage("Fehler", "Bitte w�hlen Sie einen Fu�baller!");
					return;
				}

				// Kein Name eingetragen
				if ("".equalsIgnoreCase(SessionVariablen.getInstance().getTextBearbeiten().getText())) {
					Util.showMessage("Fehler", "Bitte erfassen Sie einen Fu�ballernamen!");
					return;
				}

				int id = 0;
				String[] gewaehlt = new String[10];

				// Gew�hlten Fu�baller auslesen
				gewaehlt = listFussballer.getSelection();

				// ID des Eintrags holen
				try {
					id = DBRead.getFussballerID(gewaehlt[0]);
				}
				catch (DBException e) {
					e.handleError();
				}

				SessionVariablen.getInstance().setFussballerId(id);

				// Position auslesen
				int position = 0;
				position = SessionVariablen.getInstance().getListPositionen().getSelectionIndex();

				// Fu�baller in DB eintragen
				try {
					DBUpdate.updateTable_Fussballer(id, SessionVariablen.getInstance().getTextBearbeiten().getText(), position);
				}
				catch (DBException e) {
					e.handleError();
					return;
				}

				// Daten �bernehmen
				listFussballer.removeAll();

				if (!Util.fill_Fussballerliste(listFussballer)) {
					Util.showMessage("Fehler", "Fehler beim F�llen der Fu�ballerliste!");
					return;
				}

				SessionVariablen.getInstance().getTextBearbeiten().setText("");
				SessionVariablen.getInstance().getTextBearbeiten().setFocus();
				SessionVariablen.getInstance().setFussballerId(-1);
			}

			public void widgetDefaultSelected(SelectionEvent arg0) {
				;
			}
		});

		// Dummy-Label
		labelDummy = new Label(shellBearbeitenfussballer, SWT.TOP);

		// Fertig-Button
		Button buttonFertig = new Button(shellBearbeitenfussballer, SWT.FULL_SELECTION);
		buttonFertig.setText("Fertig");
		buttonFertig.setLayoutData(new GridData(GridData.HORIZONTAL_ALIGN_FILL));
		buttonFertig.addSelectionListener(new SelectionListener() {

			public void widgetSelected(SelectionEvent arg0) {
				shellBearbeitenfussballer.close();
				(SessionVariablen.getInstance().getShell()).setVisible(true);
			}

			public void widgetDefaultSelected(SelectionEvent arg0) {
				;
			}
		});

		// Position des Fensters festlegen
		Dimension d = Toolkit.getDefaultToolkit().getScreenSize();
		shellBearbeitenfussballer.setLocation((d.width - Konstanten.WINDOW_BEARBEITEN_FUSSBALLER_BREITE) / 2,
				(d.height - Konstanten.WINDOW_BEARBEITEN_FUSSBALLER_HOEHE) / 2);

		// Fenster jetzt anzeigen
		(SessionVariablen.getInstance().getShell()).setVisible(false);
		shellBearbeitenfussballer.open();
	}
}
