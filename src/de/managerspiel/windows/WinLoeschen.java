package de.managerspiel.windows;

//import java.awt.Composite;
import java.awt.Dimension;
import java.awt.Toolkit;

import org.eclipse.swt.SWT;
import org.eclipse.swt.events.SelectionEvent;
import org.eclipse.swt.events.SelectionListener;
import org.eclipse.swt.layout.GridData;
import org.eclipse.swt.layout.GridLayout;
import org.eclipse.swt.widgets.Button;
import org.eclipse.swt.widgets.Display;
import org.eclipse.swt.widgets.Label;
import org.eclipse.swt.widgets.List;
import org.eclipse.swt.widgets.Shell;

import de.managerspiel.db.DBDelete;
import de.managerspiel.db.DBRead;
import de.managerspiel.exception.DBException;
import de.managerspiel.util.Konstanten;
import de.managerspiel.util.SessionVariablen;
import de.managerspiel.util.Util;

public class WinLoeschen {

	/**
	 * Erstellt das Fenster f�r den Men�punkt "L�schen --> Fu�baller l�schen"
	 */
	public void createWindowFussballerLoeschen(Display display) {

		// Neues Fenster erstellen
		final Shell shellFussballerloeschen = Util.createShell(display, "Fu�baller l�schen", Konstanten.WINDOW_FUSSBALLER_LOESCHEN_BREITE,
				Konstanten.WINDOW_FUSSBALLER_LOESCHEN_HOEHE);

		// Layout setzen
		GridLayout gridlFussballerloeschen = new GridLayout();
		gridlFussballerloeschen.horizontalSpacing = 10;
		gridlFussballerloeschen.verticalSpacing = 10;
		gridlFussballerloeschen.marginHeight = 20;
		gridlFussballerloeschen.marginWidth = 20;
		gridlFussballerloeschen.numColumns = 2;
		gridlFussballerloeschen.makeColumnsEqualWidth = false;
		shellFussballerloeschen.setLayout(gridlFussballerloeschen);

		// Text f�r die Auswahlliste der vorhandenen Fu�baller
		Label labelVorhanden = new Label(shellFussballerloeschen, SWT.LEFT);
		labelVorhanden.setText("Vorhandene Fu�baller:");
		labelVorhanden.setLayoutData(new GridData(GridData.HORIZONTAL_ALIGN_FILL));

		// Dummy-Label
		Label labelDummy = new Label(shellFussballerloeschen, SWT.LEFT);
		labelDummy.setText("");

		// Auswahlliste der vorhandenen Fu�baller
		final List listVorhanden = new List(shellFussballerloeschen, SWT.V_SCROLL);

		if (!Util.fill_Fussballerliste(listVorhanden)) {
			Util.showMessage("Fehler", "Fehler beim F�llen der Fu�ballerliste!");
		}

		GridData gridListe = new GridData(GridData.HORIZONTAL_ALIGN_FILL);
		gridListe.heightHint = 100;
		gridListe.widthHint = 150;
		listVorhanden.setLayoutData(gridListe);

		// L�schen-Button
		Button buttonLoeschen = new Button(shellFussballerloeschen, SWT.LEFT);
		buttonLoeschen.setText("L�schen");
		buttonLoeschen.setLayoutData(new GridData(GridData.HORIZONTAL_ALIGN_FILL));
		buttonLoeschen.addSelectionListener(new SelectionListener() {

			public void widgetSelected(SelectionEvent arg0) {
				// Kein Fu�baller ausgew�hlt
				if (listVorhanden.getSelectionIndex() == -1) {
					Util.showMessage("Fehler", "Bitte w�hlen Sie einen Fu�baller!");
					return;
				}

				int id = 0;
				String[] gewaehlt = new String[10];

				// Gew�hlten Fu�baller auslesen
				gewaehlt = listVorhanden.getSelection();

				try {
					// ID des Fu�ballers holen
					id = DBRead.getFussballerID(gewaehlt[0]);
					// Referenz zum Fu�baller aus DB l�schen
					DBDelete.deleteTable_R_MS_FB_FB(id);
					// Fu�baller aus DB l�schen
					DBDelete.deleteTable_Fussballer(id);
				}
				catch (DBException e) {
					e.handleError();
				}

				// Eintrag aus der Listbox l�schen
				listVorhanden.remove(listVorhanden.getSelectionIndex());
			}

			public void widgetDefaultSelected(SelectionEvent arg0) {
				;
			}
		});

		// Fertig-Button
		Button buttonFertig = new Button(shellFussballerloeschen, SWT.FULL_SELECTION);
		buttonFertig.setText("Fertig");
		buttonFertig.setLayoutData(new GridData(GridData.HORIZONTAL_ALIGN_FILL));
		buttonFertig.addSelectionListener(new SelectionListener() {

			public void widgetSelected(SelectionEvent arg0) {
				shellFussballerloeschen.close();
				(SessionVariablen.getInstance().getShell()).setVisible(true);
			}

			public void widgetDefaultSelected(SelectionEvent arg0) {
				;
			}
		});

		// Position des Fensters festlegen
		Dimension d = Toolkit.getDefaultToolkit().getScreenSize();
		shellFussballerloeschen.setLocation((d.width - Konstanten.WINDOW_FUSSBALLER_LOESCHEN_BREITE) / 2,
				(d.height - Konstanten.WINDOW_FUSSBALLER_LOESCHEN_HOEHE) / 2);

		// Fenster jetzt anzeigen
		(SessionVariablen.getInstance().getShell()).setVisible(false);
		shellFussballerloeschen.open();
	}

	/**
	 * Erstellt das Fenster f�r den Men�punkt "L�schen --> Mitspieler l�schen"
	 */
	public void createWindowMitspielerLoeschen(Display display) {

		// Neues Fenster erstellen
		final Shell shellMitspielerloeschen = Util.createShell(display, "Mitspieler l�schen", Konstanten.WINDOW_MITSPIELER_LOESCHEN_BREITE,
				Konstanten.WINDOW_MITSPIELER_LOESCHEN_HOEHE);

		// Layout setzen
		GridLayout gridlMitspielerloeschen = new GridLayout();
		gridlMitspielerloeschen.horizontalSpacing = 10;
		gridlMitspielerloeschen.verticalSpacing = 10;
		gridlMitspielerloeschen.marginHeight = 20;
		gridlMitspielerloeschen.marginWidth = 20;
		gridlMitspielerloeschen.numColumns = 2;
		gridlMitspielerloeschen.makeColumnsEqualWidth = false;
		shellMitspielerloeschen.setLayout(gridlMitspielerloeschen);

		// Text f�r die Auswahlliste der vorhandenen Mitspieler
		Label labelVorhanden = new Label(shellMitspielerloeschen, SWT.LEFT);
		labelVorhanden.setText("Vorhandene Mitspieler:");
		labelVorhanden.setLayoutData(new GridData(GridData.HORIZONTAL_ALIGN_FILL));

		// Dummy-Label
		Label labelDummy = new Label(shellMitspielerloeschen, SWT.LEFT);
		labelDummy.setText("");

		// Auswahlliste der vorhandenen Mitspieler
		final List listVorhanden = new List(shellMitspielerloeschen, SWT.V_SCROLL);

		try {
			Util.fill_Mitspielerliste_TS(listVorhanden);
		}
		catch (DBException e) {
			e.handleError();
			return;
		}

		GridData gridListe = new GridData(GridData.HORIZONTAL_ALIGN_FILL);
		gridListe.heightHint = 100;
		gridListe.widthHint = 150;
		// grid_liste.verticalSpan = 2;
		listVorhanden.setLayoutData(gridListe);

		// L�schen-Button
		Button buttonLoeschen = new Button(shellMitspielerloeschen, SWT.LEFT);
		buttonLoeschen.setText("L�schen");
		buttonLoeschen.setLayoutData(new GridData(GridData.HORIZONTAL_ALIGN_FILL));
		buttonLoeschen.addSelectionListener(new SelectionListener() {

			public void widgetSelected(SelectionEvent arg0) {
				// Kein Mitspieler ausgew�hlt
				if (listVorhanden.getSelectionIndex() == -1) {
					Util.showMessage("Fehler", "Bitte w�hlen Sie einen Mitspieler!");
					return;
				}

				int id = 0;
				String[] gewaehlt = new String[10];

				// Gew�hlten Mitspieler auslesen
				gewaehlt = listVorhanden.getSelection();

				// Kontrollieren, ob Mitspieler_id noch verwendet wird
				boolean inUse = false;

				try {
					// ID des Mitspieler holen
					id = DBRead.getMitspielerID(gewaehlt[0]);
					inUse = Util.isMitspieler_ID_in_use(id);
				}
				catch (DBException e) {
					e.handleError();
				}

				if (inUse) {
					Util.showMessage("Fehler", "Der Mitspieler wird noch verwendet. L�schen Sie zuerst die Fu�baller!");
					return;
				}

				// Mitspieler aus DB l�schen
				try {
					DBDelete.deleteTable_Mitspieler(id);
				}
				catch (DBException e) {
					e.handleError();
					return;
				}

				// Eintrag aus der Listbox l�schen
				listVorhanden.remove(listVorhanden.getSelectionIndex());
			}

			public void widgetDefaultSelected(SelectionEvent arg0) {}
		});

		// Fertig-Button
		Button buttonFertig = new Button(shellMitspielerloeschen, SWT.FULL_SELECTION);
		buttonFertig.setText("Fertig");
		buttonFertig.setLayoutData(new GridData(GridData.HORIZONTAL_ALIGN_FILL));
		buttonFertig.addSelectionListener(new SelectionListener() {

			public void widgetSelected(SelectionEvent arg0) {
				shellMitspielerloeschen.close();
				(SessionVariablen.getInstance().getShell()).setVisible(true);
			}

			public void widgetDefaultSelected(SelectionEvent arg0) {}
		});

		// Position des Fensters festlegen
		Dimension d = Toolkit.getDefaultToolkit().getScreenSize();
		shellMitspielerloeschen.setLocation((d.width - Konstanten.WINDOW_MITSPIELER_LOESCHEN_BREITE) / 2,
				(d.height - Konstanten.WINDOW_MITSPIELER_LOESCHEN_HOEHE) / 2);

		// Fenster jetzt anzeigen
		(SessionVariablen.getInstance().getShell()).setVisible(false);
		shellMitspielerloeschen.open();
	}

	/**
	 * Erstellt das Fenster f�r den Men�punkt "L�schen --> Tippspiel l�schen"
	 */
	public void createWindowTippspielLoeschen(Display display) {

		// Neues Fenster erstellen
		final Shell shellTippspielloeschen = Util.createShell(display, "Tippspiel l�schen", Konstanten.WINDOW_TIPPSPIEL_LOESCHEN_BREITE,
				Konstanten.WINDOW_TIPPSPIEL_LOESCHEN_HOEHE);

		// Layout setzen
		GridLayout gridlTippspielloeschen = new GridLayout();
		gridlTippspielloeschen.horizontalSpacing = 10;
		gridlTippspielloeschen.verticalSpacing = 10;
		gridlTippspielloeschen.marginHeight = 20;
		gridlTippspielloeschen.marginWidth = 20;
		gridlTippspielloeschen.numColumns = 2;
		gridlTippspielloeschen.makeColumnsEqualWidth = false;
		shellTippspielloeschen.setLayout(gridlTippspielloeschen);

		// Text f�r die Auswahlliste der vorhandenen Tippspiele
		Label labelVorhanden = new Label(shellTippspielloeschen, SWT.LEFT);
		labelVorhanden.setText("Vorhandene Tippspiele:");
		labelVorhanden.setLayoutData(new GridData(GridData.HORIZONTAL_ALIGN_FILL));

		// Dummy-Label
		Label labelDummy = new Label(shellTippspielloeschen, SWT.LEFT);
		labelDummy.setText("");

		// Auswahlliste der vorhandenen Tippspiele
		final List listVorhanden = new List(shellTippspielloeschen, SWT.V_SCROLL);

		try {
			Util.fill_Tippspielliste(listVorhanden);
		}
		catch (DBException re) {
			Util.showMessage("Fehler", "Fehler beim F�llen der Tippspielliste!");
			return;
		}

		GridData gridListe = new GridData(GridData.HORIZONTAL_ALIGN_FILL);
		gridListe.heightHint = 100;
		gridListe.widthHint = 150;
		// grid_liste.verticalSpan = 2;
		listVorhanden.setLayoutData(gridListe);

		// L�schen-Button
		Button buttonLoeschen = new Button(shellTippspielloeschen, SWT.LEFT);
		buttonLoeschen.setText("L�schen");
		buttonLoeschen.setLayoutData(new GridData(GridData.HORIZONTAL_ALIGN_FILL));
		buttonLoeschen.addSelectionListener(new SelectionListener() {

			public void widgetSelected(SelectionEvent arg0) {
				// Kein Verein ausgew�hlt
				if (listVorhanden.getSelectionIndex() == -1) {
					Util.showMessage("Fehler", "Bitte w�hlen Sie ein Tippspiel!");
					return;
				}

				int id = 0;
				String[] gewaehlt = new String[10];

				// Gew�hltes Tippspiel auslesen
				gewaehlt = listVorhanden.getSelection();

				boolean inUse = false;
				try {
					// ID des Tippspiels holen
					id = DBRead.getTippspielID(gewaehlt[0]);
					// Kontrollieren, ob Tippspiel_id noch verwendet wird
					inUse = Util.isTippspiel_ID_in_use(id);
				}
				catch (DBException e) {
					e.handleError();
				}

				if (inUse) {
					Util.showMessage("Fehler", "Das Tippspiel wird noch verwendet. L�schen Sie zuerst die Mitspieler!");
				}
				else {
					// Tippspiel aus DB l�schen
					try {
						DBDelete.deleteTable_Tippspiel(id);
					}
					catch (DBException e) {
						e.handleError();
						return;
					}

					// Eintrag aus der Listbox l�schen
					listVorhanden.remove(listVorhanden.getSelectionIndex());

					// SessionVariablen aktualisieren
					SessionVariablen.getInstance().setTippspielId(-1);
					SessionVariablen.getInstance().setTippspielname("");
					(SessionVariablen.getInstance().getNamelabel()).setText("kein Spiel geladen");
				}
			}

			public void widgetDefaultSelected(SelectionEvent arg0) {}
		});

		// Fertig-Button
		Button buttonFertig = new Button(shellTippspielloeschen, SWT.FULL_SELECTION);
		buttonFertig.setText("Fertig");
		buttonFertig.setLayoutData(new GridData(GridData.HORIZONTAL_ALIGN_FILL));
		buttonFertig.addSelectionListener(new SelectionListener() {

			public void widgetSelected(SelectionEvent arg0) {
				shellTippspielloeschen.close();
				(SessionVariablen.getInstance().getShell()).setVisible(true);
			}

			public void widgetDefaultSelected(SelectionEvent arg0) {
				;
			}
		});

		// Position des Fensters festlegen
		Dimension d = Toolkit.getDefaultToolkit().getScreenSize();
		shellTippspielloeschen.setLocation((d.width - Konstanten.WINDOW_TIPPSPIEL_LOESCHEN_BREITE) / 2,
				(d.height - Konstanten.WINDOW_TIPPSPIEL_LOESCHEN_HOEHE) / 2);

		// Fenster jetzt anzeigen
		(SessionVariablen.getInstance().getShell()).setVisible(false);
		shellTippspielloeschen.open();
	}
}
