package de.managerspiel.windows;

//import java.awt.Composite;
import java.awt.Dimension;
import java.awt.Toolkit;

import org.eclipse.swt.SWT;
import org.eclipse.swt.events.SelectionEvent;
import org.eclipse.swt.events.SelectionListener;
import org.eclipse.swt.layout.GridData;
import org.eclipse.swt.layout.GridLayout;
import org.eclipse.swt.widgets.Button;
import org.eclipse.swt.widgets.Display;
import org.eclipse.swt.widgets.Label;
import org.eclipse.swt.widgets.List;
import org.eclipse.swt.widgets.Shell;

import de.managerspiel.db.DBRead;
import de.managerspiel.exception.DBException;
import de.managerspiel.util.Konstanten;
import de.managerspiel.util.SessionVariablen;
import de.managerspiel.util.Util;

public class WinLaden {

	/**
	 * Erstellt das Fenster f�r den Men�punkt "Laden --> Tippspiel"
	 */
	public void createWindowLadenTippspiel(Display display) {

		// Neues Fenster erstellen
		final Shell shellLadentippspiel = Util.createShell(display, "Tippspiel laden", Konstanten.WINDOW_LADEN_TIPPSPIEL_BREITE,
				Konstanten.WINDOW_LADEN_TIPPSPIEL_HOEHE);

		// Layout setzen
		GridLayout gridlLadentippspiel = new GridLayout();
		gridlLadentippspiel.verticalSpacing = 10;
		gridlLadentippspiel.marginHeight = 20;
		gridlLadentippspiel.marginWidth = 20;
		gridlLadentippspiel.numColumns = 2;
		gridlLadentippspiel.makeColumnsEqualWidth = false;
		shellLadentippspiel.setLayout(gridlLadentippspiel);

		// Auswahlliste der vorhandenen Tippspiele
		Label labelVorhanden = new Label(shellLadentippspiel, SWT.LEFT);
		labelVorhanden.setText("W�hlen Sie ein vorhandenes Tippspiel:");
		GridData gridVorhanden = new GridData(GridData.HORIZONTAL_ALIGN_FILL);
		gridVorhanden.horizontalSpan = 2;
		labelVorhanden.setLayoutData(gridVorhanden);
		final List listVorhanden = new List(shellLadentippspiel, SWT.V_SCROLL);

		try {
			Util.fill_Tippspielliste(listVorhanden);
		}
		catch (DBException re) {
			Util.showMessage("Fehler", "Fehler beim F�llen der Tippspielliste!");
			return;
		}

		GridData gridListe = new GridData(GridData.HORIZONTAL_ALIGN_FILL);
		gridListe.horizontalSpan = 2;
		gridListe.heightHint = Konstanten.WINDOW_LADEN_TIPPSPIEL_HOEHE - 130;
		listVorhanden.setLayoutData(gridListe);

		// Laden-Button
		Button buttonLaden = new Button(shellLadentippspiel, SWT.FULL_SELECTION);
		buttonLaden.setText("Laden");
		buttonLaden.setLayoutData(new GridData(GridData.HORIZONTAL_ALIGN_FILL));
		buttonLaden.addSelectionListener(new SelectionListener() {

			public void widgetSelected(SelectionEvent arg0) {
				int id = 0;
				String[] gewaehlt = new String[10];

				// Gew�hltes Tippspiel auslesen
				gewaehlt = listVorhanden.getSelection();

				// Pr�fen, ob Tippspiel ausgew�hlt wurde
				int tippspiel = 0;
				tippspiel = listVorhanden.getSelectionIndex();
				if (tippspiel == -1) {
					Util.showMessage("Fehler", "Bitte w�hlen Sie ein Tippspiel!");
					return;
				}

				// ID des gew�hlten Tippspiels holen
				try {
					id = DBRead.getTippspielID(gewaehlt[0]);
				}
				catch (DBException e) {
					e.handleError();
				}

				// Tippspiel laden
				try {
					DBRead.setTippspielData(id);
				}
				catch (DBException re) {
					Util.showMessage("Fehler", "Das Tippspiel konnte nicht geladen werden!");
					return;
				}

				// Label aktualisieren
				(SessionVariablen.getInstance().getNamelabel()).setText(SessionVariablen.getInstance().getTippspielname());

				// Richtiges Fenster anzeigen
				shellLadentippspiel.close();
				(SessionVariablen.getInstance().getShell()).setVisible(true);
			}

			public void widgetDefaultSelected(SelectionEvent arg0) {
				;
			}
		});

		// Fertig-Button
		Button buttonAbbrechen = new Button(shellLadentippspiel, SWT.FULL_SELECTION);
		buttonAbbrechen.setText("Abbrechen");
		buttonAbbrechen.setLayoutData(new GridData(GridData.HORIZONTAL_ALIGN_FILL));
		buttonAbbrechen.addSelectionListener(new SelectionListener() {

			public void widgetSelected(SelectionEvent arg0) {
				shellLadentippspiel.close();
				(SessionVariablen.getInstance().getShell()).setVisible(true);
			}

			public void widgetDefaultSelected(SelectionEvent arg0) {
				;
			}
		});

		// Position des Fensters festlegen
		Dimension d = Toolkit.getDefaultToolkit().getScreenSize();
		shellLadentippspiel.setLocation((d.width - Konstanten.WINDOW_LADEN_TIPPSPIEL_BREITE) / 2,
				(d.height - Konstanten.WINDOW_LADEN_TIPPSPIEL_HOEHE) / 2);

		// Fenster jetzt anzeigen
		(SessionVariablen.getInstance().getShell()).setVisible(false);
		shellLadentippspiel.open();
	}

	/**
	 * Erstellt das Fenster f�r den Men�punkt "Laden --> Spieltag"
	 */
	public void createWindowLadenSpieltag(Display display) {

		// Neues Fenster erstellen
		final Shell shellLadenspieltag = Util.createShell(display, "Tippspiel laden", Konstanten.WINDOW_LADEN_SPIELTAG_BREITE,
				Konstanten.WINDOW_LADEN_SPIELTAG_HOEHE);

		// Layout setzen
		GridLayout gridlLadenspieltag = new GridLayout();
		gridlLadenspieltag.verticalSpacing = 10;
		gridlLadenspieltag.marginHeight = 20;
		gridlLadenspieltag.marginWidth = 20;
		gridlLadenspieltag.numColumns = 2;
		gridlLadenspieltag.makeColumnsEqualWidth = false;
		shellLadenspieltag.setLayout(gridlLadenspieltag);

		// Auswahlliste der Spieltage
		Label labelSpieltag = new Label(shellLadenspieltag, SWT.LEFT);
		labelSpieltag.setText("W�hlen Sie einen Spieltag:");
		GridData gridSpieltag = new GridData(GridData.HORIZONTAL_ALIGN_FILL);
		gridSpieltag.horizontalSpan = 2;
		labelSpieltag.setLayoutData(gridSpieltag);
		final List listSpieltag = new List(shellLadenspieltag, SWT.V_SCROLL);

		Util.fill_Spieltagliste(listSpieltag);

		GridData gridListe = new GridData(GridData.HORIZONTAL_ALIGN_FILL);
		gridListe.horizontalSpan = 2;
		gridListe.heightHint = Konstanten.WINDOW_LADEN_TIPPSPIEL_HOEHE - 130;
		gridListe.widthHint = Konstanten.WINDOW_LADEN_TIPPSPIEL_BREITE - 80;
		listSpieltag.setLayoutData(gridListe);

		// Festlegen-Button
		Button buttonFestlegen = new Button(shellLadenspieltag, SWT.FULL_SELECTION);
		buttonFestlegen.setText("Spieltag setzen");
		buttonFestlegen.setLayoutData(new GridData(GridData.HORIZONTAL_ALIGN_FILL));
		buttonFestlegen.addSelectionListener(new SelectionListener() {

			public void widgetSelected(SelectionEvent arg0) {
				// Pr�fen, ob Spieltag ausgew�hlt wurde
				int spieltagId = 0;
				spieltagId = listSpieltag.getSelectionIndex();
				if (spieltagId == -1) {
					Util.showMessage("Fehler", "Bitte w�hlen Sie einen Spieltag!");
					return;
				}

				String spieltag = listSpieltag.getItem(spieltagId);
				SessionVariablen.getInstance().setSpieltag(Integer.parseInt(spieltag));

				// Label aktualisieren
				(SessionVariablen.getInstance().getSpieltaglabel()).setText(spieltag + ". Spieltag");

				// Richtiges Fenster anzeigen
				shellLadenspieltag.close();
				(SessionVariablen.getInstance().getShell()).setVisible(true);
			}

			public void widgetDefaultSelected(SelectionEvent arg0) {
				;
			}
		});

		// Abbrechen-Button
		Button buttonAbbrechen = new Button(shellLadenspieltag, SWT.FULL_SELECTION);
		buttonAbbrechen.setText("Abbrechen");
		buttonAbbrechen.setLayoutData(new GridData(GridData.HORIZONTAL_ALIGN_FILL));
		buttonAbbrechen.addSelectionListener(new SelectionListener() {

			public void widgetSelected(SelectionEvent arg0) {
				shellLadenspieltag.close();
				(SessionVariablen.getInstance().getShell()).setVisible(true);
			}

			public void widgetDefaultSelected(SelectionEvent arg0) {
				;
			}
		});

		// Position des Fensters festlegen
		Dimension d = Toolkit.getDefaultToolkit().getScreenSize();
		shellLadenspieltag.setLocation((d.width - Konstanten.WINDOW_LADEN_SPIELTAG_BREITE) / 2,
				(d.height - Konstanten.WINDOW_LADEN_SPIELTAG_HOEHE) / 2);

		// Fenster jetzt anzeigen
		(SessionVariablen.getInstance().getShell()).setVisible(false);
		shellLadenspieltag.open();
	}
}
