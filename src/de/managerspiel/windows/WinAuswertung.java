package de.managerspiel.windows;

import java.awt.Dimension;
import java.awt.Toolkit;

import org.eclipse.swt.SWT;
import org.eclipse.swt.events.SelectionEvent;
import org.eclipse.swt.events.SelectionListener;
import org.eclipse.swt.graphics.Color;
import org.eclipse.swt.layout.GridData;
import org.eclipse.swt.layout.GridLayout;
import org.eclipse.swt.layout.RowLayout;
import org.eclipse.swt.widgets.Button;
import org.eclipse.swt.widgets.Display;
import org.eclipse.swt.widgets.Group;
import org.eclipse.swt.widgets.Label;
import org.eclipse.swt.widgets.Shell;

import de.managerspiel.exception.AuswertungException;
import de.managerspiel.exception.DBException;
import de.managerspiel.main.AuswertungErstellen;
import de.managerspiel.util.Konstanten;
import de.managerspiel.util.SessionVariablen;
import de.managerspiel.util.Util;

public class WinAuswertung {

	/**
	 * Erstellt das Fenster f�r den Menupunkt "Auswertung -> Auswertung durchf�hren"
	 */
	public void createWindowAuswertungErstellen(final Display display) {

		final Shell shellAuswertungerstellen = Util.createShell(display, "Ausgabeoptionen", Konstanten.WINDOW_AUSWERTUNG_ERSTELLEN_BREITE,
				Konstanten.WINDOW_AUSWERTUNG_ERSTELLEN_HOEHE);

		// Layout setzen
		GridLayout gridlAuswertungerstellen = new GridLayout();
		gridlAuswertungerstellen.verticalSpacing = 10;
		gridlAuswertungerstellen.marginHeight = 20;
		gridlAuswertungerstellen.marginWidth = 20;
		gridlAuswertungerstellen.numColumns = 2;
		gridlAuswertungerstellen.makeColumnsEqualWidth = true;

		shellAuswertungerstellen.setLayout(gridlAuswertungerstellen);

		// Layout f�r die Radio Button Groupbox
		GridLayout grl = new GridLayout();
		grl.marginTop = 10;
		grl.marginBottom = 10;
		grl.marginRight = 10;

		// Layout f�r die �berschrift
		GridData grd = new GridData(GridData.HORIZONTAL_ALIGN_FILL);
		grd.horizontalSpan = 2;
		// �berschrift
		Label lblHeader = new Label(shellAuswertungerstellen, SWT.LEFT);
		lblHeader.setText("Bitte w�hlen Sie die Ausgabeoptionen.");
		lblHeader.setBackground(new Color(display, 255, 255, 255));
		lblHeader.setLayoutData(grd);

		// Radiobuttons f�r die Anzahl der Dateien
		Group grCount = new Group(shellAuswertungerstellen, SWT.LEFT);
		grCount.setLayout(new RowLayout(SWT.VERTICAL));
		grCount.setLayoutData(new GridData(GridData.HORIZONTAL_ALIGN_FILL));
		grCount.setText("Dateianzahl");
		grCount.setBackground(new Color(display, 255, 255, 255));
		grCount.setLayout(grl);
		final Button btnOneFile = new Button(grCount, SWT.RADIO);
		btnOneFile.setText("Eine Datei");
		btnOneFile.setSelection(true);
		final Button btnMultipleFiles = new Button(grCount, SWT.RADIO);
		btnMultipleFiles.setText("Mehrere Dateien");

		// Radiobuttons f�r den Ausgabetyp
		Group grType = new Group(shellAuswertungerstellen, SWT.LEFT);
		grType.setLayout(new RowLayout(SWT.VERTICAL));
		grType.setLayoutData(new GridData(GridData.HORIZONTAL_ALIGN_FILL));
		grType.setText("Dateityp");
		grType.setBackground(new Color(display, 255, 255, 255));
		grType.setLayout(grl);
		final Button btnText = new Button(grType, SWT.RADIO);
		btnText.setText("Textdatei");
		btnText.setSelection(true);
		btnText.setEnabled(true);
		final Button btnPdf = new Button(grType, SWT.RADIO);
		btnPdf.setText("PDF");

		GridData grd2 = new GridData(GridData.HORIZONTAL_ALIGN_FILL);
		grd2.horizontalSpan = 2;

		// OK-Button
		Button btnOK = new Button(shellAuswertungerstellen, SWT.FULL_SELECTION);
		btnOK.setText("OK");
		btnOK.setLayoutData(new GridData(GridData.HORIZONTAL_ALIGN_FILL));
		btnOK.addSelectionListener(new SelectionListener() {

			public void widgetSelected(SelectionEvent arg0) {

				AuswertungErstellen aw = new AuswertungErstellen();

				try {
					// Ausgabe in eine Textdatei
					if (btnOneFile.getSelection() && btnText.getSelection()) {
						aw.auswertungDurchfuehren(SessionVariablen.getInstance().getEinstellung(Konstanten.AUSGABEPFAD), true, false, false);
						shellAuswertungerstellen.close();
						(SessionVariablen.getInstance().getShell()).setVisible(true);
						return;
					}

					// Ausgabe in eine PDF Datei
					if (btnOneFile.getSelection() && btnPdf.getSelection()) {
						aw.auswertungDurchfuehren(SessionVariablen.getInstance().getEinstellung(Konstanten.AUSGABEPFAD), true, true, false);
						shellAuswertungerstellen.close();
						(SessionVariablen.getInstance().getShell()).setVisible(true);
						return;
					}

					// Ausgabe in mehrere Textdateien
					if (btnMultipleFiles.getSelection() && btnText.getSelection()) {
						aw.auswertungDurchfuehren(SessionVariablen.getInstance().getEinstellung(Konstanten.AUSGABEPFAD), false, false,
								false);
						shellAuswertungerstellen.close();
						(SessionVariablen.getInstance().getShell()).setVisible(true);
						return;
					}

					// Ausgabe in mehrere PDF Dateien
					if (btnMultipleFiles.getSelection() && btnPdf.getSelection()) {
						Util.showMessage("Hinweis", "Der PDF Export in mehrere Dateien ist noch nicht implementiert.");
					}
				}
				catch (DBException e) {
					e.handleError();
				}
				catch (AuswertungException e) {
					e.handleError();
				}
			}

			public void widgetDefaultSelected(SelectionEvent arg0) {
				;
			}
		});

		// Abbrechen-Button
		Button button_abbrechen = new Button(shellAuswertungerstellen, SWT.FULL_SELECTION);
		button_abbrechen.setText("Abbrechen");
		button_abbrechen.setLayoutData(new GridData(GridData.HORIZONTAL_ALIGN_FILL));
		button_abbrechen.addSelectionListener(new SelectionListener() {

			public void widgetSelected(SelectionEvent arg0) {
				shellAuswertungerstellen.close();
				(SessionVariablen.getInstance().getShell()).setVisible(true);
			}

			public void widgetDefaultSelected(SelectionEvent arg0) {
				;
			}
		});

		// Position des Fensters festlegen
		Dimension d = Toolkit.getDefaultToolkit().getScreenSize();
		shellAuswertungerstellen.setLocation((d.width - Konstanten.WINDOW_AUSWERTUNG_ERSTELLEN_BREITE) / 2,
				(d.height - Konstanten.WINDOW_AUSWERTUNG_ERSTELLEN_HOEHE) / 2);

		// Fenster jetzt anzeigen
		(SessionVariablen.getInstance().getShell()).setVisible(false);
		shellAuswertungerstellen.open();

	}
}
