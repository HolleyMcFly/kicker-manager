package de.managerspiel.windows;

import java.awt.Dimension;
import java.awt.Toolkit;

import org.eclipse.swt.SWT;
import org.eclipse.swt.events.SelectionEvent;
import org.eclipse.swt.events.SelectionListener;
import org.eclipse.swt.layout.GridData;
import org.eclipse.swt.layout.GridLayout;
import org.eclipse.swt.widgets.Button;
import org.eclipse.swt.widgets.Display;
import org.eclipse.swt.widgets.Label;
import org.eclipse.swt.widgets.List;
import org.eclipse.swt.widgets.Shell;

import de.managerspiel.db.DBRead;
import de.managerspiel.db.DBUtil;
import de.managerspiel.exception.DBException;
import de.managerspiel.util.Konstanten;
import de.managerspiel.util.PrioritaetenUtil;
import de.managerspiel.util.SessionVariablen;
import de.managerspiel.util.Util;

public class WinPrioritaetenVerwaltung {

	/**
	 * Erstellt das Auswahlfenster f�r den Men�punkt
	 * "Verwaltung --> Priorit�ten erfassen"
	 */
	public void createWindowPrioritaeten_AuswahlMitspieler(final Display display) {

		final Shell shellPrioAuswahlMitspieler = Util.createShell(display, "Mitspieler ausw�hlen",
				Konstanten.WINDOW_PRIO_AUSWAHL_MITSPIELER_BREITE, Konstanten.WINDOW_PRIO_AUSWAHL_MITSPIELER_HOEHE);

		// Layout setzen
		GridLayout gridlPrioAuswahlMitspieler = new GridLayout();
		gridlPrioAuswahlMitspieler.verticalSpacing = 10;
		gridlPrioAuswahlMitspieler.marginHeight = 20;
		gridlPrioAuswahlMitspieler.marginWidth = 20;
		gridlPrioAuswahlMitspieler.numColumns = 2;
		gridlPrioAuswahlMitspieler.makeColumnsEqualWidth = false;
		shellPrioAuswahlMitspieler.setLayout(gridlPrioAuswahlMitspieler);

		// Auswahlliste der vorhandenen Mitspieler
		Label labelVorhanden = new Label(shellPrioAuswahlMitspieler, SWT.LEFT);
		labelVorhanden.setText("W�hlen Sie einen vorhandenen Mitspieler:");
		GridData gridVorhanden = new GridData(GridData.HORIZONTAL_ALIGN_FILL);
		gridVorhanden.horizontalSpan = 2;
		labelVorhanden.setLayoutData(gridVorhanden);

		final List listVorhanden = new List(shellPrioAuswahlMitspieler, SWT.V_SCROLL);

		try {
			Util.fill_Mitspielerliste_TS(listVorhanden);
		}
		catch (DBException e) {
			e.handleError();
			return;
		}

		GridData gridListe = new GridData(GridData.HORIZONTAL_ALIGN_FILL);
		gridListe.horizontalSpan = 2;
		gridListe.heightHint = Konstanten.WINDOW_PRIO_AUSWAHL_MITSPIELER_HOEHE - 130;
		listVorhanden.setLayoutData(gridListe);

		// Weiter-Button
		Button buttonWeiter = new Button(shellPrioAuswahlMitspieler, SWT.FULL_SELECTION);
		buttonWeiter.setText("Laden");
		buttonWeiter.setLayoutData(new GridData(GridData.HORIZONTAL_ALIGN_FILL));
		buttonWeiter.addSelectionListener(new SelectionListener() {

			public void widgetSelected(SelectionEvent arg0) {
				int id = 0;
				String[] gewaehlt = new String[10];

				// Gew�hlten Mitspieler auslesen
				gewaehlt = listVorhanden.getSelection();

				// Pr�fen, ob Mitspieler ausgew�hlt wurde
				int mitspieler = 0;
				mitspieler = listVorhanden.getSelectionIndex();
				if (mitspieler == -1) {
					Util.showMessage("Fehler", "Bitte w�hlen Sie einen Mitspieler!");
					return;
				}

				// ID des gew�hlten Mitspielers holen
				try {
					id = DBRead.getMitspielerID(gewaehlt[0]);
					// Mitspieler laden
					DBRead.getMitspieler(id);
				}
				catch (DBException e) {
					e.handleError();
				}

				// ID setzen
				SessionVariablen.getInstance().setMitspielerId(id);

				// Richtiges Fenster anzeigen
				shellPrioAuswahlMitspieler.close();

				WinPrioritaetenVerwaltung cw = new WinPrioritaetenVerwaltung();
				cw.createWindowPrioritaetenErfassen(display);
			}

			public void widgetDefaultSelected(SelectionEvent arg0) {
				;
			}
		});

		// Abbrechen-Button
		Button buttonAbbrechen = new Button(shellPrioAuswahlMitspieler, SWT.FULL_SELECTION);
		buttonAbbrechen.setText("Abbrechen");
		buttonAbbrechen.setLayoutData(new GridData(GridData.HORIZONTAL_ALIGN_FILL));
		buttonAbbrechen.addSelectionListener(new SelectionListener() {

			public void widgetSelected(SelectionEvent arg0) {
				shellPrioAuswahlMitspieler.close();
				(SessionVariablen.getInstance().getShell()).setVisible(true);
			}

			public void widgetDefaultSelected(SelectionEvent arg0) {
				;
			}
		});

		// Position des Fensters festlegen
		Dimension d = Toolkit.getDefaultToolkit().getScreenSize();
		shellPrioAuswahlMitspieler.setLocation((d.width - Konstanten.WINDOW_PRIO_AUSWAHL_MITSPIELER_BREITE) / 2,
				(d.height - Konstanten.WINDOW_PRIO_AUSWAHL_MITSPIELER_HOEHE) / 2);

		// Fenster jetzt anzeigen
		(SessionVariablen.getInstance().getShell()).setVisible(false);
		shellPrioAuswahlMitspieler.open();
	}

	/**
	 * Erstellt das Fenster f�r den Men�punkt
	 * "Verwaltung --> Priorit�ten erfassen"
	 */
	public void createWindowPrioritaetenErfassen(Display display) {

		int spieltag = SessionVariablen.getInstance().getSpieltag();
		final Shell shellPrioritaetenErfassen = Util.createShell(display, "Priorit�ten f�r den " + spieltag + ". Spieltag erfassen",
				Konstanten.WINDOW_PRIO_ERFASSEN_BREITE, Konstanten.WINDOW_PRIO_ERFASSEN_HOEHE);

		// Layout setzen
		GridLayout gridlPrioritaetenErfassen = new GridLayout();
		gridlPrioritaetenErfassen.horizontalSpacing = 10;
		gridlPrioritaetenErfassen.verticalSpacing = 10;
		gridlPrioritaetenErfassen.marginHeight = 20;
		gridlPrioritaetenErfassen.marginWidth = 20;
		gridlPrioritaetenErfassen.numColumns = 3;
		gridlPrioritaetenErfassen.makeColumnsEqualWidth = false;
		shellPrioritaetenErfassen.setLayout(gridlPrioritaetenErfassen);

		// Text f�r die Auswahllisten der vorhandenen Fu�baller
		Label labelVorhanden = new Label(shellPrioritaetenErfassen, SWT.LEFT);
		labelVorhanden.setText("Bitte w�hlen Sie mit Hilfe der Pfeiltasten:");
		labelVorhanden.setLayoutData(new GridData(GridData.HORIZONTAL_ALIGN_FILL));

		// Dummy-Label
		Label labelDummy = new Label(shellPrioritaetenErfassen, SWT.LEFT);
		labelDummy.setText("");
		labelDummy = new Label(shellPrioritaetenErfassen, SWT.LEFT);
		labelDummy.setText("");

		// Falls noch keine Priorit�ten erfasst wurden,
		// werden die der vorangegangenen Spieltags kopiert
		int anzahlPrioritaetenMitspieler;
		try {
			anzahlPrioritaetenMitspieler = DBRead.getAnzahlPrioritaeten();
		}
		catch (DBException e) {
			e.handleError();
			return;
		}

		// Falls Anzahl Priorit�ten = 0, versuche zu kopieren
		if (anzahlPrioritaetenMitspieler == 0) {
			try {
				DBUtil.copyPrioritaeten();
			}
			catch (DBException e) {
				e.handleError();
				Util.showMessage("Achtung!", "Achtung, die Priorit�ten m�ssen neu erfasst werden!");
			}
		}

		// Auswahlliste der vorhandenen Torm�nner
		final List listTormaenner = new List(shellPrioritaetenErfassen, SWT.V_SCROLL);

		try {
			Util.fill_Fussballerliste_SP_sorted(listTormaenner, Konstanten.SPIELPOSITION_TORMANN);
		}
		catch (DBException e) {
			e.handleError();
		}

		GridData gridListeTormann = new GridData(GridData.HORIZONTAL_ALIGN_FILL);
		gridListeTormann.heightHint = Konstanten.MAX_TORMANN * Konstanten.ZEILENHOEHE_FUSSBALLER;
		gridListeTormann.widthHint = 150;
		gridListeTormann.verticalSpan = 2;
		listTormaenner.setLayoutData(gridListeTormann);

		// Nach-oben-Button
		Button buttonTormannNachOben = new Button(shellPrioritaetenErfassen, SWT.FULL_SELECTION);
		buttonTormannNachOben.setText("^");
		buttonTormannNachOben.setLayoutData(new GridData(GridData.HORIZONTAL_ALIGN_FILL));
		buttonTormannNachOben.addSelectionListener(new SelectionListener() {

			public void widgetSelected(SelectionEvent arg0) {
				// Kontrolliere, ob ein Fu�baller ausgew�hlt ist
				if (listTormaenner.getSelectionIndex() == -1) {
					Util.showMessage("Fehler", "Bitte w�hlen Sie einen Fu�baller");
					return;
				}

				// Auswahl merken
				int gewaehlter_spieler = listTormaenner.getSelectionIndex();

				// Prioritaeten.fussballer_nach_oben (list_tormaenner,
				// Konstanten.SPIELPOSITION_TORMANN);

				// Auswahl wieder setzen
				if (gewaehlter_spieler != 0)
					listTormaenner.setSelection(gewaehlter_spieler - 1);
			}

			public void widgetDefaultSelected(SelectionEvent arg0) {
				;
			}
		});

		// Dummy-Label
		labelDummy = new Label(shellPrioritaetenErfassen, SWT.LEFT);
		labelDummy.setText("");

		// Nach-unten-Button
		Button buttonTormannNachUnten = new Button(shellPrioritaetenErfassen, SWT.FULL_SELECTION);
		buttonTormannNachUnten.setText("v");
		buttonTormannNachUnten.setLayoutData(new GridData(GridData.HORIZONTAL_ALIGN_FILL));
		buttonTormannNachUnten.addSelectionListener(new SelectionListener() {

			public void widgetSelected(SelectionEvent arg0) {
				// Kontrolliere, ob ein Fu�baller ausgew�hlt ist
				if (listTormaenner.getSelectionIndex() == -1) {
					Util.showMessage("Fehler", "Bitte w�hlen Sie einen Fu�baller");
					return;
				}

				try {
					PrioritaetenUtil.fussballerNachUnten(listTormaenner, Konstanten.SPIELPOSITION_TORMANN);
				}
				catch (DBException e) {
					e.handleError();
				}
			}

			public void widgetDefaultSelected(SelectionEvent arg0) {
				;
			}
		});

		// Dummy-Label
		labelDummy = new Label(shellPrioritaetenErfassen, SWT.LEFT);
		labelDummy.setText("");

		// Auswahlliste der vorhandenen Abwehrspieler
		final List listVerteidiger = new List(shellPrioritaetenErfassen, SWT.V_SCROLL);

		try {
			Util.fill_Fussballerliste_SP_sorted(listVerteidiger, Konstanten.SPIELPOSITION_VERTEIDIGER);
		}
		catch (DBException e) {
			e.handleError();
		}

		GridData gridListeVerteidiger = new GridData(GridData.HORIZONTAL_ALIGN_FILL);
		gridListeVerteidiger.heightHint = Konstanten.MAX_VERTEIDIGER * Konstanten.ZEILENHOEHE_FUSSBALLER;
		gridListeVerteidiger.widthHint = 150;
		gridListeVerteidiger.verticalSpan = 2;
		listVerteidiger.setLayoutData(gridListeVerteidiger);

		// Nach-oben-Button
		Button buttonVerteidigerNachOben = new Button(shellPrioritaetenErfassen, SWT.FULL_SELECTION);
		buttonVerteidigerNachOben.setText("^");
		buttonVerteidigerNachOben.setLayoutData(new GridData(GridData.HORIZONTAL_ALIGN_FILL));
		buttonVerteidigerNachOben.addSelectionListener(new SelectionListener() {

			public void widgetSelected(SelectionEvent arg0) {
				// Kontrolliere, ob ein Fu�baller ausgew�hlt ist
				if (listVerteidiger.getSelectionIndex() == -1) {
					Util.showMessage("Fehler", "Bitte w�hlen Sie einen Fu�baller");
					return;
				}

				// Auswahl merken
				int gewaehlter_spieler = listVerteidiger.getSelectionIndex();

				try {
					PrioritaetenUtil.fussballerNachOben(listVerteidiger, Konstanten.SPIELPOSITION_VERTEIDIGER);
				}
				catch (DBException e) {
					e.handleError();
				}

				// Auswahl wieder setzen
				if (gewaehlter_spieler != 0)
					listVerteidiger.setSelection(gewaehlter_spieler - 1);
			}

			public void widgetDefaultSelected(SelectionEvent arg0) {
				;
			}
		});

		// Dummy-Label
		labelDummy = new Label(shellPrioritaetenErfassen, SWT.LEFT);
		labelDummy.setText("");

		// Nach-unten-Button
		Button buttonVerteidigerNachUnten = new Button(shellPrioritaetenErfassen, SWT.FULL_SELECTION);
		buttonVerteidigerNachUnten.setText("v");
		buttonVerteidigerNachUnten.setLayoutData(new GridData(GridData.HORIZONTAL_ALIGN_FILL));
		buttonVerteidigerNachUnten.addSelectionListener(new SelectionListener() {

			public void widgetSelected(SelectionEvent arg0) {
				// Kontrolliere, ob ein Fu�baller ausgew�hlt ist
				if (listVerteidiger.getSelectionIndex() == -1) {
					Util.showMessage("Fehler", "Bitte w�hlen Sie einen Fu�baller");
					return;
				}

				// Auswahl merken
				int gewaehlter_spieler = listVerteidiger.getSelectionIndex();

				try {
					PrioritaetenUtil.fussballerNachUnten(listVerteidiger, Konstanten.SPIELPOSITION_VERTEIDIGER);
				}
				catch (DBException e) {
					e.handleError();
				}

				// Auswahl wieder setzen
				if (gewaehlter_spieler != (listVerteidiger.getItemCount() - 1))
					listVerteidiger.setSelection(gewaehlter_spieler + 1);
			}

			public void widgetDefaultSelected(SelectionEvent arg0) {
				;
			}
		});

		// Dummy-Label
		labelDummy = new Label(shellPrioritaetenErfassen, SWT.LEFT);
		labelDummy.setText("");

		// Auswahlliste der vorhandenen Mittelfeldspieler
		final List listMittelfeldspieler = new List(shellPrioritaetenErfassen, SWT.V_SCROLL);

		try {
			Util.fill_Fussballerliste_SP_sorted(listMittelfeldspieler, Konstanten.SPIELPOSITION_MITTELFELDSPIELER);
		}
		catch (DBException e) {
			e.handleError();
		}

		GridData gridListeMittelfeldspieler = new GridData(GridData.HORIZONTAL_ALIGN_FILL);
		gridListeMittelfeldspieler.heightHint = Konstanten.MAX_MITTELFELD * Konstanten.ZEILENHOEHE_FUSSBALLER;
		gridListeMittelfeldspieler.widthHint = 150;
		gridListeMittelfeldspieler.verticalSpan = 2;
		listMittelfeldspieler.setLayoutData(gridListeMittelfeldspieler);

		// Nach-oben-Button
		Button buttonMittelfeldspielerNachOben = new Button(shellPrioritaetenErfassen, SWT.FULL_SELECTION);
		buttonMittelfeldspielerNachOben.setText("^");
		buttonMittelfeldspielerNachOben.setLayoutData(new GridData(GridData.HORIZONTAL_ALIGN_FILL));
		buttonMittelfeldspielerNachOben.addSelectionListener(new SelectionListener() {

			public void widgetSelected(SelectionEvent arg0) {
				// Kontrolliere, ob ein Fu�baller ausgew�hlt ist
				if (listMittelfeldspieler.getSelectionIndex() == -1) {
					Util.showMessage("Fehler", "Bitte w�hlen Sie einen Fu�baller");
					return;
				}

				// Auswahl merken
				int gewaehlter_spieler = listMittelfeldspieler.getSelectionIndex();

				try {
					PrioritaetenUtil.fussballerNachOben(listMittelfeldspieler, Konstanten.SPIELPOSITION_MITTELFELDSPIELER);
				}
				catch (DBException e) {
					e.handleError();
				}

				// Auswahl wieder setzen
				if (gewaehlter_spieler != 0)
					listMittelfeldspieler.setSelection(gewaehlter_spieler - 1);
			}

			public void widgetDefaultSelected(SelectionEvent arg0) {}
		});

		// Dummy-Label
		labelDummy = new Label(shellPrioritaetenErfassen, SWT.LEFT);
		labelDummy.setText("");

		// Nach-unten-Button
		Button buttonMittelfeldspielerNachUnten = new Button(shellPrioritaetenErfassen, SWT.FULL_SELECTION);
		buttonMittelfeldspielerNachUnten.setText("v");
		buttonMittelfeldspielerNachUnten.setLayoutData(new GridData(GridData.HORIZONTAL_ALIGN_FILL));
		buttonMittelfeldspielerNachUnten.addSelectionListener(new SelectionListener() {

			public void widgetSelected(SelectionEvent arg0) {
				// Kontrolliere, ob ein Fu�baller ausgew�hlt ist
				if (listMittelfeldspieler.getSelectionIndex() == -1) {
					Util.showMessage("Fehler", "Bitte w�hlen Sie einen Fu�baller");
					return;
				}

				// Auswahl merken
				int gewaehlter_spieler = listMittelfeldspieler.getSelectionIndex();

				try {
					PrioritaetenUtil.fussballerNachUnten(listMittelfeldspieler, Konstanten.SPIELPOSITION_MITTELFELDSPIELER);
				}
				catch (DBException e) {
					e.handleError();
				}

				// Auswahl wieder setzen
				if (gewaehlter_spieler != (listMittelfeldspieler.getItemCount() - 1))
					listMittelfeldspieler.setSelection(gewaehlter_spieler + 1);
			}

			public void widgetDefaultSelected(SelectionEvent arg0) {}
		});

		// Dummy-Label
		labelDummy = new Label(shellPrioritaetenErfassen, SWT.LEFT);
		labelDummy.setText("");

		// Auswahlliste der vorhandenen St�rmer
		final List listStuermer = new List(shellPrioritaetenErfassen, SWT.V_SCROLL);

		try {
			Util.fill_Fussballerliste_SP_sorted(listStuermer, Konstanten.SPIELPOSITION_STUERMER);
		}
		catch (DBException e) {
			e.handleError();
		}

		GridData gridListeStuermer = new GridData(GridData.HORIZONTAL_ALIGN_FILL);
		gridListeStuermer.heightHint = Konstanten.MAX_STUERMER * Konstanten.ZEILENHOEHE_FUSSBALLER;
		gridListeStuermer.widthHint = 150;
		gridListeStuermer.verticalSpan = 2;
		listStuermer.setLayoutData(gridListeStuermer);

		// Nach-oben-Button
		Button buttonStuermerNachOben = new Button(shellPrioritaetenErfassen, SWT.FULL_SELECTION);
		buttonStuermerNachOben.setText("^");
		buttonStuermerNachOben.setLayoutData(new GridData(GridData.HORIZONTAL_ALIGN_FILL));
		buttonStuermerNachOben.addSelectionListener(new SelectionListener() {

			public void widgetSelected(SelectionEvent arg0) {
				// Kontrolliere, ob ein Fu�baller ausgew�hlt ist
				if (listStuermer.getSelectionIndex() == -1) {
					Util.showMessage("Fehler", "Bitte w�hlen Sie einen Fu�baller");
					return;
				}

				// Auswahl merken
				int gewaehlter_spieler = listStuermer.getSelectionIndex();

				try {
					PrioritaetenUtil.fussballerNachOben(listStuermer, Konstanten.SPIELPOSITION_STUERMER);
				}
				catch (DBException e) {
					e.handleError();
				}

				// Auswahl wieder setzen
				if (gewaehlter_spieler != 0)
					listStuermer.setSelection(gewaehlter_spieler - 1);
			}

			public void widgetDefaultSelected(SelectionEvent arg0) {}
		});

		// Dummy-Label
		labelDummy = new Label(shellPrioritaetenErfassen, SWT.LEFT);
		labelDummy.setText("");

		// Nach-unten-Button
		Button buttonStuermerNachUnten = new Button(shellPrioritaetenErfassen, SWT.FULL_SELECTION);
		buttonStuermerNachUnten.setText("v");
		buttonStuermerNachUnten.setLayoutData(new GridData(GridData.HORIZONTAL_ALIGN_FILL));
		buttonStuermerNachUnten.addSelectionListener(new SelectionListener() {

			public void widgetSelected(SelectionEvent arg0) {
				// Kontrolliere, ob ein Fu�baller ausgew�hlt ist
				if (listStuermer.getSelectionIndex() == -1) {
					Util.showMessage("Fehler", "Bitte w�hlen Sie einen Fu�baller");
					return;
				}

				// Auswahl merken
				int gewaehlterSpieler = listStuermer.getSelectionIndex();

				try {
					PrioritaetenUtil.fussballerNachUnten(listStuermer, Konstanten.SPIELPOSITION_STUERMER);
				}
				catch (DBException e) {
					e.handleError();
				}

				// Auswahl wieder setzen
				if (gewaehlterSpieler != (listStuermer.getItemCount() - 1))
					listStuermer.setSelection(gewaehlterSpieler + 1);
			}

			public void widgetDefaultSelected(SelectionEvent arg0) {}
		});

		// Dummy-Label
		labelDummy = new Label(shellPrioritaetenErfassen, SWT.LEFT);
		labelDummy.setText("");

		// Speichern-Button
		Button buttonSpeichern = new Button(shellPrioritaetenErfassen, SWT.FULL_SELECTION);
		buttonSpeichern.setText("Speichern");
		buttonSpeichern.setLayoutData(new GridData(GridData.HORIZONTAL_ALIGN_FILL));
		buttonSpeichern.addSelectionListener(new SelectionListener() {

			public void widgetSelected(SelectionEvent arg0) {
				shellPrioritaetenErfassen.close();
				(SessionVariablen.getInstance().getShell()).setVisible(true);
			}

			public void widgetDefaultSelected(SelectionEvent arg0) {}
		});

		// Dummy-Label
		labelDummy = new Label(shellPrioritaetenErfassen, SWT.LEFT);
		labelDummy.setText("");

		// Abbrechen-Button
		Button buttonAbbrechen = new Button(shellPrioritaetenErfassen, SWT.FULL_SELECTION);
		buttonAbbrechen.setText("Abbrechen");
		buttonAbbrechen.setLayoutData(new GridData(GridData.HORIZONTAL_ALIGN_FILL));
		buttonAbbrechen.addSelectionListener(new SelectionListener() {

			public void widgetSelected(SelectionEvent arg0) {
				shellPrioritaetenErfassen.close();
				(SessionVariablen.getInstance().getShell()).setVisible(true);
			}

			public void widgetDefaultSelected(SelectionEvent arg0) {}
		});

		// Position des Fensters festlegen
		Dimension d = Toolkit.getDefaultToolkit().getScreenSize();
		shellPrioritaetenErfassen.setLocation((d.width - Konstanten.WINDOW_PRIO_ERFASSEN_BREITE) / 2,
				(d.height - Konstanten.WINDOW_PRIO_ERFASSEN_HOEHE) / 2);

		// Fenster jetzt anzeigen
		(SessionVariablen.getInstance().getShell()).setVisible(false);
		shellPrioritaetenErfassen.open();

		// Wenn noch keine Priorit�ten vorhanden, einen Satz speichern
		try {
			if (DBRead.getNumberPrios() == 0) {
				PrioritaetenUtil.saveInitialPrios(shellPrioritaetenErfassen);
			}
		}
		catch (DBException e) {
			e.handleError();
		}
	}
}
