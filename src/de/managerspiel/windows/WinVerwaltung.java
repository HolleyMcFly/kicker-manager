package de.managerspiel.windows;

import java.awt.Dimension;
import java.awt.Toolkit;

import org.eclipse.swt.SWT;
import org.eclipse.swt.events.SelectionEvent;
import org.eclipse.swt.events.SelectionListener;
import org.eclipse.swt.layout.GridData;
import org.eclipse.swt.layout.GridLayout;
import org.eclipse.swt.widgets.Button;
import org.eclipse.swt.widgets.Display;
import org.eclipse.swt.widgets.Group;
import org.eclipse.swt.widgets.Label;
import org.eclipse.swt.widgets.List;
import org.eclipse.swt.widgets.Shell;
import org.eclipse.swt.widgets.Text;

import de.managerspiel.db.DBDelete;
import de.managerspiel.db.DBInsert;
import de.managerspiel.db.DBRead;
import de.managerspiel.db.DBUpdate;
import de.managerspiel.exception.DBException;
import de.managerspiel.util.Konstanten;
import de.managerspiel.util.SessionVariablen;
import de.managerspiel.util.Util;

public class WinVerwaltung {

	/**
	 * Erstellt das Fenster f�r den Men�punkt "Verwaltung --> Vereine verwalten"
	 */
	public void createWindowVerwaltungVerein(Display display) {

		final Shell shellVerwaltungVerein = Util.createShell(display, "Vereine verwalten", Konstanten.WINDOW_VERWALTUNG_VEREIN_BREITE,
				Konstanten.WINDOW_VERWALTUNG_VEREIN_HOEHE);

		// Layout setzen
		GridLayout gridlVerwaltungVerein = new GridLayout();
		gridlVerwaltungVerein.horizontalSpacing = 10;
		gridlVerwaltungVerein.verticalSpacing = 10;
		gridlVerwaltungVerein.marginHeight = 20;
		gridlVerwaltungVerein.marginWidth = 20;
		gridlVerwaltungVerein.numColumns = 2;
		gridlVerwaltungVerein.makeColumnsEqualWidth = false;
		shellVerwaltungVerein.setLayout(gridlVerwaltungVerein);

		// Text f�r die Auswahlliste der vorhandenen Vereine
		Label labelVorhanden = new Label(shellVerwaltungVerein, SWT.LEFT);
		labelVorhanden.setText("Vorhandene Vereine:");
		labelVorhanden.setLayoutData(new GridData(GridData.HORIZONTAL_ALIGN_FILL));

		// Dummy-Label
		Label labelDummy = new Label(shellVerwaltungVerein, SWT.LEFT);
		labelDummy.setText("");

		// Auswahlliste der vorhandenen Vereine
		final List listVorhanden = new List(shellVerwaltungVerein, SWT.V_SCROLL);

		if (!Util.fill_Vereinsliste(listVorhanden)) {
			Util.showMessage("Fehler", "Fehler beim F�llen der Vereinsliste!");
			return;
		}

		GridData gridListe = new GridData(GridData.HORIZONTAL_ALIGN_FILL);
		gridListe.heightHint = 250;
		gridListe.widthHint = 150;
		gridListe.verticalSpan = 3;
		listVorhanden.setLayoutData(gridListe);
		listVorhanden.addSelectionListener(new SelectionListener() {

			public void widgetSelected(SelectionEvent arg0) {
				String eintrag[] = new String[1000];
				eintrag = listVorhanden.getSelection();

				SessionVariablen.getInstance().getTextBearbeiten().setText(eintrag[0]);
				// Text merken f�r die Suche
				SessionVariablen.getInstance().setTextOriginal(SessionVariablen.getInstance().getTextBearbeiten().getText());
			}

			public void widgetDefaultSelected(SelectionEvent arg0) {}
		});

		// Rahmen f�r Eingabe neuer Vereine
		Group grpNeu = new Group(shellVerwaltungVerein, SWT.NONE);
		grpNeu.setText("Neuen Verein hinzuf�gen");
		GridLayout gridlGroup = new GridLayout();
		gridlGroup.numColumns = 2;
		grpNeu.setLayout(gridlGroup);
		grpNeu.setLayoutData(new GridData(GridData.HORIZONTAL_ALIGN_FILL));

		// Dummy-Label f�r Leerzeile
		labelDummy = new Label(grpNeu, SWT.TOP);
		labelDummy = new Label(grpNeu, SWT.TOP);

		// Eingabefeld f�r Vereins-Name
		Label labelName = new Label(grpNeu, SWT.TOP);
		labelName.setText("Name:");
		final Text textName = new Text(grpNeu, SWT.LEFT);
		textName.setTextLimit(50);

		// Hinzuf�gen-Button
		Button buttonHinzufuegen = new Button(grpNeu, SWT.CENTER);
		buttonHinzufuegen.setText("Hinzuf�gen");
		GridData gridButton = new GridData(GridData.HORIZONTAL_ALIGN_FILL);
		gridButton.horizontalSpan = 2;
		buttonHinzufuegen.setLayoutData(gridButton);
		buttonHinzufuegen.addSelectionListener(new SelectionListener() {

			public void widgetSelected(SelectionEvent arg0) {
				
				// Verein in DB eintragen
				try {
					DBInsert.insertTable_Verein(textName.getText(), (SessionVariablen.getInstance().getShell()));
				}
				catch (DBException e) {
					e.handleError();
					return;
				}
				
				// Daten �bernehmen
				listVorhanden.add(textName.getText());
				textName.setText("");
				textName.setFocus();
			}

			public void widgetDefaultSelected(SelectionEvent arg0) {
				;
			}
		});

		// Rahmen f�r Bearbeitung von Vereinen
		Group grpBearbeiten = new Group(shellVerwaltungVerein, SWT.NONE);
		grpBearbeiten.setText("Verein bearbeiten");
		GridLayout gridl_bearbeiten = new GridLayout();
		gridl_bearbeiten.numColumns = 2;
		grpBearbeiten.setLayout(gridl_bearbeiten);
		grpBearbeiten.setLayoutData(new GridData(GridData.HORIZONTAL_ALIGN_FILL));

		// Dummy-Label f�r Leerzeile
		labelDummy = new Label(grpBearbeiten, SWT.TOP);
		labelDummy = new Label(grpBearbeiten, SWT.TOP);

		// Eingabefeld f�r Vereins-Name
		Label labelBearbeiten = new Label(grpBearbeiten, SWT.TOP);
		labelBearbeiten.setText("Name:");
		SessionVariablen.getInstance().setTextBearbeiten(new Text(grpBearbeiten, SWT.LEFT));
		SessionVariablen.getInstance().getTextBearbeiten().setTextLimit(50);

		// Hinzuf�gen-Button
		Button buttonBearbeiten = new Button(grpBearbeiten, SWT.CENTER);
		buttonBearbeiten.setText("Speichern");
		GridData gridSpeichern = new GridData(GridData.HORIZONTAL_ALIGN_FILL);
		gridSpeichern.horizontalSpan = 2;
		buttonBearbeiten.setLayoutData(gridSpeichern);
		buttonBearbeiten.addSelectionListener(new SelectionListener() {

			public void widgetSelected(SelectionEvent arg0) {
				int id;
				try {
					id = DBRead.getVereinID(SessionVariablen.getInstance().getTextOriginal());
					DBUpdate.updateTable_Verein(id, SessionVariablen.getInstance().getTextBearbeiten().getText());
				}
				catch (DBException e) {
					e.handleError();
					return;
				}

				// Liste neu einlesen
				Util.fill_Vereinsliste(listVorhanden);
				SessionVariablen.getInstance().getTextBearbeiten().setText("");
			}

			public void widgetDefaultSelected(SelectionEvent arg0) {}
		});

		// Rahmen f�r Verein l�schen
		Group grpLoesch = new Group(shellVerwaltungVerein, SWT.NONE);
		grpLoesch.setText("Verein l�schen");
		GridLayout gridlLoesch = new GridLayout();
		grpLoesch.setLayout(gridlLoesch);
		grpLoesch.setLayoutData(new GridData(GridData.HORIZONTAL_ALIGN_FILL));

		// Dummy-Label f�r Leerzeile
		labelDummy = new Label(grpLoesch, SWT.TOP);
		labelDummy = new Label(grpLoesch, SWT.TOP);

		// L�schen-Button
		Button buttonLoeschen = new Button(grpLoesch, SWT.LEFT);
		buttonLoeschen.setText("L�schen");
		buttonLoeschen.setLayoutData(new GridData(GridData.HORIZONTAL_ALIGN_FILL));
		buttonLoeschen.addSelectionListener(new SelectionListener() {

			public void widgetSelected(SelectionEvent arg0) {
				// Kein Verein ausgew�hlt
				if (listVorhanden.getSelectionIndex() == -1) {
					Util.showMessage("Fehler", "Bitte w�hlen Sie einen Verein!");
					return;
				}

				int id = 0;
				String[] gewaehlt = new String[10];

				// Gew�hlten Verein auslesen
				gewaehlt = listVorhanden.getSelection();

				try {
					// ID des Eintrags holen
					id = DBRead.getVereinID(gewaehlt[0]);
					// Verein aus DB l�schen
					DBDelete.deleteTable_Verein(id);
				}
				catch (DBException e) {
					e.handleError();
					return;
				}

				// Eintrag aus der Listbox l�schen
				listVorhanden.remove(listVorhanden.getSelectionIndex());
			}

			public void widgetDefaultSelected(SelectionEvent arg0) {
				;
			}
		});

		// Fertig-Button
		Button buttonFertig = new Button(shellVerwaltungVerein, SWT.FULL_SELECTION);
		buttonFertig.setText("Fertig");
		buttonFertig.setLayoutData(new GridData(GridData.HORIZONTAL_ALIGN_FILL));
		buttonFertig.addSelectionListener(new SelectionListener() {

			public void widgetSelected(SelectionEvent arg0) {
				shellVerwaltungVerein.close();
				(SessionVariablen.getInstance().getShell()).setVisible(true);
			}

			public void widgetDefaultSelected(SelectionEvent arg0) {}
		});

		// Position des Fensters festlegen
		Dimension d = Toolkit.getDefaultToolkit().getScreenSize();
		shellVerwaltungVerein.setLocation((d.width - Konstanten.WINDOW_VERWALTUNG_VEREIN_BREITE) / 2,
				(d.height - Konstanten.WINDOW_VERWALTUNG_VEREIN_HOEHE) / 2);

		// Fenster jetzt anzeigen
		(SessionVariablen.getInstance().getShell()).setVisible(false);
		shellVerwaltungVerein.open();
	}

	/**
	 * Erstellt das Fenster f�r den Men�punkt
	 * "Verwaltung --> Fussballer wechseln"
	 */
	public void createWindowFussballerWechseln(Display display) {

		final Shell shellFussballerWechseln = Util.createShell(display, "Fu�baller wechseln", Konstanten.WINDOW_FUSSBALLER_WECHSELN_BREITE,
				Konstanten.WINDOW_FUSSBALLER_WECHSELN_HOEHE);

		// Layout setzen
		GridLayout gridlFussballerWechseln = new GridLayout();
		gridlFussballerWechseln.horizontalSpacing = 10;
		gridlFussballerWechseln.verticalSpacing = 10;
		gridlFussballerWechseln.marginHeight = 20;
		gridlFussballerWechseln.marginWidth = 20;
		gridlFussballerWechseln.numColumns = 3;
		gridlFussballerWechseln.makeColumnsEqualWidth = false;
		shellFussballerWechseln.setLayout(gridlFussballerWechseln);

		// Text f�r die Auswahlliste der vorhandenen Fu�baller
		Label labelVorhanden = new Label(shellFussballerWechseln, SWT.LEFT);
		labelVorhanden.setText("Vorhandene Fu�baller:");
		labelVorhanden.setLayoutData(new GridData(GridData.HORIZONTAL_ALIGN_FILL));

		// Dummy-Label
		Label labelDummy = new Label(shellFussballerWechseln, SWT.LEFT);
		labelDummy.setText("");
		labelDummy = new Label(shellFussballerWechseln, SWT.LEFT);
		labelDummy.setText("");

		// Auswahlliste der vorhandenen Fu�baller
		final List listVorhanden = new List(shellFussballerWechseln, SWT.V_SCROLL);

		try {
			Util.fill_Fussballerliste_aktuell(listVorhanden);
		}
		catch (DBException e) {
			e.handleError();
		}

		GridData gridListe = new GridData(GridData.HORIZONTAL_ALIGN_FILL);
		gridListe.heightHint = 250;
		gridListe.widthHint = 150;
		gridListe.verticalSpan = 3;
		listVorhanden.setLayoutData(gridListe);

		// Wechseln ins Ausland-Button
		Button buttonWechselA = new Button(shellFussballerWechseln, SWT.CENTER);
		buttonWechselA.setText("Wechsel ins Ausland");
		GridData gridButton = new GridData(GridData.HORIZONTAL_ALIGN_FILL);
		gridButton.horizontalSpan = 1;
		buttonWechselA.setLayoutData(gridButton);
		buttonWechselA.addSelectionListener(new SelectionListener() {

			public void widgetSelected(SelectionEvent arg0) {
				if (listVorhanden.getSelectionIndex() == -1) {
					Util.showMessage("Fehler", "Bitte w�hlen Sie einen Fu�baller!");
					return;
				}

				try {
					DBUpdate.updateTable_Fussballer_VereinAusland(listVorhanden.getItem(listVorhanden.getSelectionIndex()),
							SessionVariablen.getInstance().getSpieltag() - 1);
				}
				catch (DBException e) {
					e.handleError();
				}

				// Liste neu einlesen, damit Indizes stimmen
				listVorhanden.removeAll();
				Util.fill_Fussballerliste(listVorhanden);

				Util.showMessage("Fehler", "Der Fu�baller ist erfolgreich ins Ausland gewechselt.");
			}

			public void widgetDefaultSelected(SelectionEvent arg0) {
				;
			}
		});

		// Dummy-Label
		labelDummy = new Label(shellFussballerWechseln, SWT.LEFT);
		labelDummy.setText("");

		// Rahmen f�r den Wechsel zu neuem inl�ndischen Verein
		Group grpBearbeiten = new Group(shellFussballerWechseln, SWT.NONE);
		grpBearbeiten.setText("Wechsel zu inl�ndischem Verein");
		GridLayout gridlBearbeiten = new GridLayout();
		gridlBearbeiten.numColumns = 2;
		grpBearbeiten.setLayout(gridlBearbeiten);
		grpBearbeiten.setLayoutData(new GridData(GridData.HORIZONTAL_ALIGN_FILL));

		// Dummy-Label f�r Leerzeile
		labelDummy = new Label(grpBearbeiten, SWT.TOP);
		labelDummy = new Label(grpBearbeiten, SWT.TOP);

		// Auswahlliste der vorhandenen Vereine
		final List listVereine = new List(grpBearbeiten, SWT.V_SCROLL);

		if (!Util.fill_Vereinsliste(listVereine)) {
			Util.showMessage("Fehler", "Fehler beim F�llen der Vereinsliste!");
			return;
		}

		GridData gridVereinsliste = new GridData(GridData.HORIZONTAL_ALIGN_FILL);
		gridVereinsliste.heightHint = 150;
		gridVereinsliste.widthHint = 150;
		gridVereinsliste.verticalSpan = 1;
		listVereine.setLayoutData(gridVereinsliste);

		// Eintragen-Button
		Button buttonBearbeiten = new Button(grpBearbeiten, SWT.CENTER);
		buttonBearbeiten.setText("Speichern");
		GridData gridSpeichern = new GridData(GridData.HORIZONTAL_ALIGN_FILL);
		gridSpeichern.horizontalSpan = 2;
		buttonBearbeiten.setLayoutData(gridSpeichern);
		buttonBearbeiten.addSelectionListener(new SelectionListener() {

			public void widgetSelected(SelectionEvent arg0) {
				if (listVorhanden.getSelectionIndex() == -1) {
					Util.showMessage("Fehler", "Bitte w�hlen Sie einen Fu�baller!");
					return;
				}

				if (listVereine.getSelectionIndex() == -1) {
					Util.showMessage("Fehler", "Bitte w�hlen Sie einen neuen Verein!");
					return;
				}

				try {
					DBUpdate.updateTable_Fussballer_VereinInland(listVorhanden.getItem(listVorhanden.getSelectionIndex()),
							listVereine.getSelectionIndex(), SessionVariablen.getInstance().getSpieltag() - 1);
				}
				catch (DBException e) {
					e.handleError();
					return;
				}

				Util.showMessage("Fehler", "Der Fu�baller ist erfolgreich im Inland gewechselt.");
			}

			public void widgetDefaultSelected(SelectionEvent arg0) {
				;
			}
		});

		// Dummy-Label
		labelDummy = new Label(shellFussballerWechseln, SWT.LEFT);
		labelDummy.setText("");

		// Fertig-Button
		Button buttonFertig = new Button(shellFussballerWechseln, SWT.FULL_SELECTION);
		buttonFertig.setText("Fertig");
		buttonFertig.setLayoutData(new GridData(GridData.HORIZONTAL_ALIGN_FILL));
		buttonFertig.addSelectionListener(new SelectionListener() {

			public void widgetSelected(SelectionEvent arg0) {
				shellFussballerWechseln.close();
				(SessionVariablen.getInstance().getShell()).setVisible(true);
			}

			public void widgetDefaultSelected(SelectionEvent arg0) {
				;
			}
		});

		// Position des Fensters festlegen
		Dimension d = Toolkit.getDefaultToolkit().getScreenSize();
		shellFussballerWechseln.setLocation((d.width - Konstanten.WINDOW_FUSSBALLER_WECHSELN_BREITE) / 2,
				(d.height - Konstanten.WINDOW_FUSSBALLER_WECHSELN_HOEHE) / 2);

		// Fenster jetzt anzeigen
		(SessionVariablen.getInstance().getShell()).setVisible(false);
		shellFussballerWechseln.open();
	}
}
