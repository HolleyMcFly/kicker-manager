package de.managerspiel.util;

import org.eclipse.swt.widgets.List;
import org.eclipse.swt.widgets.Shell;

import de.managerspiel.db.DBInsert;
import de.managerspiel.db.DBRead;
import de.managerspiel.db.DBUpdate;
import de.managerspiel.exception.DBException;

public class PrioritaetenUtil {

	/**
	 * Speichert die Priorit�ten f�r einen Mitspieler f�r einen Spieltag initial
	 */
	public static boolean saveInitialPrios(Shell shell) throws DBException {

		// Hole alle Torm�nner des Mitspielers
		java.util.List<String> tormaenner = DBRead.getFussballer_MS_SP(Konstanten.SPIELPOSITION_TORMANN);

		// Hole die ID und schreibe einen Satz
		int defPrio = -1;
		for (String tormann : tormaenner) {
			int fussballerId = DBRead.getFussballerID(tormann);
			DBInsert.insertTable_Prioritaeten(fussballerId, defPrio++, SessionVariablen.getInstance().getSpieltag());
		}

		// Hole alle Verteidiger des Mitspielers
		java.util.List<String> verteidigerList = DBRead.getFussballer_MS_SP(Konstanten.SPIELPOSITION_VERTEIDIGER);

		// Hole die ID und schreibe einen Satz
		defPrio = -1;
		for (String verteidiger : verteidigerList) {
			int fussballerId = DBRead.getFussballerID(verteidiger);
			DBInsert.insertTable_Prioritaeten(fussballerId, defPrio++, SessionVariablen.getInstance().getSpieltag());
		}

		// Hole alle Mittelfeldspieler des Mitspielers
		java.util.List<String> mittelfeldList = DBRead.getFussballer_MS_SP(Konstanten.SPIELPOSITION_MITTELFELDSPIELER);

		// Hole die ID und schreibe einen Satz
		defPrio = -1;
		for (String mittelfeldspieler : mittelfeldList) {
			int fussballerId = DBRead.getFussballerID(mittelfeldspieler);
			DBInsert.insertTable_Prioritaeten(fussballerId, defPrio++, SessionVariablen.getInstance().getSpieltag());
		}

		// Hole alle St�rmer des Mitspielers
		java.util.List<String> stuermerList = DBRead.getFussballer_MS_SP(Konstanten.SPIELPOSITION_STUERMER);

		// Hole die ID und schreibe einen Satz
		defPrio = -1;
		for (String stuermer : stuermerList) {
			int fussballer_id = DBRead.getFussballerID(stuermer);
			DBInsert.insertTable_Prioritaeten(fussballer_id, defPrio++, SessionVariablen.getInstance().getSpieltag());
		}
		return true;
	}

	/**
	 * Verschiebt einen Fussballer in einer Liste eine Position nach oben und
	 * liest die Liste neu ein
	 */
	public static boolean fussballerNachOben(List liste, int spielposition) throws DBException {
		int index = liste.getSelectionIndex();

		// Wenn der erste Fu�baller ausgew�hlt ist, ist nichts zu tun
		if (index == 0)
			return true;

		// Fu�baller_id auslesen und in die Session schreiben
		String fussballer = liste.getItem(index);
		int fussballerId = DBRead.getFussballerID(fussballer);
		SessionVariablen.getInstance().setFussballerId(fussballerId);

		// gew�hlten Fu�baller updaten. Als Priorit�t wird index �bergeben. Die
		// Indizes beginnen mit 0, also muss man f�r die Priorit�t eins
		// abziehen.
		DBUpdate.updateTable_Prioritaeten(index - 1);

		// �bergeordneten Fu�baller auslesen
		String fussballerAlt = liste.getItem(index - 1);
		fussballerId = DBRead.getFussballerID(fussballerAlt);
		SessionVariablen.getInstance().setFussballerId(fussballerId);

		DBUpdate.updateTable_Prioritaeten(index);

		// Liste neu einlesen
		liste.removeAll();
		Util.fill_Fussballerliste_SP_sorted(liste, spielposition);

		return true;
	}

	/**
	 * Verschiebt einen Fussballer in einer Liste eine Position nach oben und
	 * liest die Liste neu ein
	 */
	public static boolean fussballerNachUnten(List liste, int spielposition) throws DBException {

		int index = liste.getSelectionIndex();

		// Wenn der letzte Fu�baller ausgew�hlt ist, ist nichts zu tun
		if (index == (liste.getItemCount() - 1))
			return true;

		// Fu�baller_id auslesen und in die Session schreiben
		String fussballer = liste.getItem(index);
		int fussballerId = DBRead.getFussballerID(fussballer);
		SessionVariablen.getInstance().setFussballerId(fussballerId);

		// gew�hlten Fu�baller updaten. Als Priorit�t wird index �bergeben. Die
		// Indizes beginnen mit 0, also muss man f�r die Priorit�t eins
		// abziehen.
		DBUpdate.updateTable_Prioritaeten(index + 1);

		// Untergeordnete Fu�baller auslesen
		String fussballerAlt = liste.getItem(index + 1);
		fussballerId = DBRead.getFussballerID(fussballerAlt);
		SessionVariablen.getInstance().setFussballerId(fussballerId);

		DBUpdate.updateTable_Prioritaeten(index);

		// Liste neu einlesen
		liste.removeAll();
		Util.fill_Fussballerliste_SP_sorted(liste, spielposition);

		return true;
	}
}