package de.managerspiel.util;

import java.util.ArrayList;
import java.util.Collections;

import org.eclipse.swt.graphics.Image;
import org.eclipse.swt.widgets.Display;
import org.eclipse.swt.widgets.List;
import org.eclipse.swt.widgets.MessageBox;
import org.eclipse.swt.widgets.Shell;

import de.managerspiel.db.DBRead;
import de.managerspiel.exception.DBException;
import de.managerspiel.model.Prioritaeteneintrag;
import de.managerspiel.sort.PriolisteSort;
import de.managerspiel.sort.PriolisteSort.SortColumn;
import de.managerspiel.sort.PriolisteSort.SortDirection;
import de.managerspiel.sort.StringlisteSort;

public class Util {

	/**
	 * F�llt die �bergebene Liste mit allen Eintr�gen aus der Tabelle Tippspiel
	 */
	public static void fill_Tippspielliste(List liste) throws DBException {

		try {
			java.util.List<String> tippspiele = DBRead.getTippspiele();

			int pos = 0;
			for (String tippspiel : tippspiele) {

				liste.add(tippspiel, pos);
				pos++;
			}
		}
		catch (DBException re) {
			throw re;
		}
	}

	/**
	 * F�llt die �bergebene Liste mit allen Eintr�gen aus der Tabelle Verein
	 */
	public static boolean fill_Vereinsliste(List liste) {

		java.util.List<String> fussballer = null;

		// Liste l�schen, falls Eintr�ge vorhanden
		liste.removeAll();

		// Eintr�ge lesen
		try {
			fussballer = DBRead.getVereine();
		}
		catch (DBException re) {
			return false;
		}

		// Liste f�llen
		int idx = 0;
		for (String fussballerEintrag : fussballer) {
			liste.add(fussballerEintrag, idx);
			idx++;
		}
		return true;
	}

	/**
	 * F�llt die �bergebene Liste mit allen Eintr�gen aus der Tabelle Mitspieler
	 * zum aktuellen Tippspiel
	 */
	public static void fill_Mitspielerliste_TS(List liste) throws DBException {

		java.util.List<String> mitspieler = DBRead.getAlleMitspieler();

		// Liste f�llen
		int idx = 0;
		for (String spieler : mitspieler) {
			liste.add(spieler, idx);
			idx++;
		}
	}

	/**
	 * F�llt die �bergebene Liste mit allen Eintr�gen aus der Tabelle Fu�baller
	 * zum aktuellen Mitspieler
	 */
	public static boolean fill_Fussballerliste_MS(List liste) {

		java.util.List<String> fussballer = null;

		// Eintr�ge lesen
		try {
			fussballer = DBRead.getAlleFussballerMS();
		}
		catch (DBException re) {
			return false;
		}

		// Liste f�llen
		int idx = 0;
		for (String fussballerEintrag : fussballer) {
			liste.add(fussballerEintrag, idx);
			idx++;
		}
		return true;
	}

	/**
	 * F�llt die �bergebene Liste mit allen Eintr�gen aus der Tabelle Fu�baller
	 */
	public static boolean fill_Fussballerliste(List liste) {

		java.util.List<String> eintraege = null;

		// Eintr�ge lesen
		try {
			eintraege = DBRead.getAlleFussballer();
		}
		catch (DBException re) {
			return false;
		}

		// Sortierkriterien setzen
		StringlisteSort stringeintrag;
		stringeintrag = new StringlisteSort();
		stringeintrag.setDirection(de.managerspiel.sort.StringlisteSort.SortDirection.ASCENDING);

		// Sortierung durchf�hren
		Collections.sort(eintraege, stringeintrag);

		// Sortierte Eintr�ge zur�ckschreiben
		for (String fussballer : eintraege)
			liste.add(fussballer);

		return true;
	}

	/**
	 * F�llt die �bergebene Liste mit allen Eintr�gen aus der Tabelle Fu�baller
	 * Spieler, die ins Ausland gewechselt sind, werden nicht beachtet
	 */
	public static void fill_Fussballerliste_aktuell(List liste) throws DBException {

		java.util.List<String> aktuelleFussballer = new ArrayList<String>();

		// Eintr�ge lesen
		java.util.List<String> fussballer = DBRead.getAlleFussballer();

		// Filterung nur aktuelle Fussballer
		for (String fussballerEintrag : fussballer) {
			int fussballerId = DBRead.getFussballerID(fussballerEintrag);
			if (DBRead.isFussballerAktuell(fussballerId) == true)
				aktuelleFussballer.add(fussballerEintrag);
		}

		// Liste sortieren
		StringlisteSort stringeintrag = new StringlisteSort();
		stringeintrag.setDirection(de.managerspiel.sort.StringlisteSort.SortDirection.ASCENDING);
		Collections.sort(aktuelleFussballer, stringeintrag);

		// Sortierte Eintr�ge zur�ckschreiben
		for (String aktuellerFussballer : aktuelleFussballer) {
			liste.add(aktuellerFussballer);
		}
	}

	/**
	 * Kontrolliert, ob Tippspiel-ID irgendwo benutzt wird
	 */
	public static boolean isTippspiel_ID_in_use(int id) throws DBException {
		// Ist ID in Tabelle Mitspieler eingetragen?
		if (DBRead.isTippspielIdInMitspieler(id))
			return true;
		else
			return false;
	}

	/**
	 * Kontrolliert, ob Mitspieler-ID irgendwo benutzt wird
	 */
	public static boolean isMitspieler_ID_in_use(int id) throws DBException {
		// Ist ID in Tabelle R_MS_FB eingetragen?
		if (DBRead.isMitspielerIdInR_MS_FB(id))
			return true;
		else
			return false;
	}

	/**
	 * F�llt die �bergebene Liste mit den m�glichen Positionen der Fu�baller
	 */
	public static void fill_Positionsliste(List liste) {
		// Liste f�llen
		liste.add("Tor", Konstanten.SPIELPOSITION_TORMANN);
		liste.add("Abwehr", Konstanten.SPIELPOSITION_VERTEIDIGER);
		liste.add("Mittelfeld", Konstanten.SPIELPOSITION_MITTELFELDSPIELER);
		liste.add("Sturm", Konstanten.SPIELPOSITION_STUERMER);
		return;
	}

	/**
	 * F�llt die �bergebene Liste mit den m�glichen Spieltagen
	 */
	public static void fill_Spieltagliste(List liste) {
		// Liste f�llen
		liste.add("1", 0);
		liste.add("2", 1);
		liste.add("3", 2);
		liste.add("4", 3);
		liste.add("5", 4);
		liste.add("6", 5);
		liste.add("7", 6);
		liste.add("8", 7);
		liste.add("9", 8);
		liste.add("10", 9);
		liste.add("11", 10);
		liste.add("12", 11);
		liste.add("13", 12);
		liste.add("14", 13);
		liste.add("15", 14);
		liste.add("16", 15);
		liste.add("17", 16);
		liste.add("18", 17);
		liste.add("19", 18);
		liste.add("20", 19);
		liste.add("21", 20);
		liste.add("22", 21);
		liste.add("23", 22);
		liste.add("24", 23);
		liste.add("25", 24);
		liste.add("26", 25);
		liste.add("27", 26);
		liste.add("28", 27);
		liste.add("29", 28);
		liste.add("30", 29);
		liste.add("31", 30);
		liste.add("32", 31);
		liste.add("33", 32);
		liste.add("34", 33);
		return;
	}

	/**
	 * F�llt die �bergebene Liste mit den Fu�ballern einer Position eines
	 * Mitspielers, sortiert nach Priorit�t
	 */
	public static void fill_Fussballerliste_SP_sorted(List liste, int spielposition) throws DBException {
		int anzahl_eintraege = 0;
		java.util.List<Prioritaeteneintrag> prioEintraege = new ArrayList<Prioritaeteneintrag>();

		// Eintr�ge lesen
		java.util.List<String> fussballerList = DBRead.getFussballer_MS_SP(spielposition);

		// Datens�tze f�llen
		for (String fussballer : fussballerList) {
			// Nur Fu�baller, die nicht im Ausland spielen
			int fussballerId = DBRead.getFussballerID(fussballer);
			if (DBRead.isFussballerAktuell(fussballerId) == false)
				continue;

			Prioritaeteneintrag prioEintrag = new Prioritaeteneintrag();

			// Fu�ballername
			prioEintrag.setFussballername(fussballer);
			anzahl_eintraege++;

			// Fu�baller_id
			prioEintrag.setFussballer_id(DBRead.getFussballerID(fussballer));

			// Priorit�t
			SessionVariablen.getInstance().setFussballerId(prioEintrag.getFussballer_id());
			prioEintrag.setPrioritaet(DBRead.getPrioritaet());

			prioEintraege.add(prioEintrag);
		}

		// Sortierkriterien setzen
		PriolisteSort listeneintrag = new PriolisteSort();
		listeneintrag.setColumn(SortColumn.PRIORITAET);
		listeneintrag.setDirection(SortDirection.ASCENDING);

		// Sortierung durchf�hren
		Collections.sort(prioEintraege, listeneintrag);

		// Sortierte Eintr�ge zur�ckschreiben
		for (int i = 0; i < anzahl_eintraege; i++) {
			Prioritaeteneintrag prio_temp = (Prioritaeteneintrag) prioEintraege.get(i);
			liste.add(prio_temp.getFussballername());
		}
	}

	public static Shell createShell(Display display, String titel, int width, int height) {
		Shell shell = new Shell(display);
		shell.setText(titel);
		shell.setImage(new Image(display, "images/fussball.ico"));
		shell.setSize(width, height);
		return shell;
	}

	public static boolean einstellungExists(String key) {
		String value = SessionVariablen.getInstance().getEinstellung(key);
		if (value == null)
			return false;

		return true;
	}
	
	public static void showMessage(String titel, String text) {
		Shell shell = SessionVariablen.getInstance().getShell();
		MessageBox msgBox = new MessageBox(shell);
		msgBox.setText(titel);
		msgBox.setMessage(text);
		msgBox.open();
	}
	
	public static boolean isTippspielGeladen() {
		
		if (SessionVariablen.getInstance().getTippspielId() == -1) {
			Util.showMessage("Fehler", "Bitte laden Sie zuerst ein Tippspiel!");
			return false;
		}
		
		return true;
	}
	
	public static boolean isSpieltagGeladen() {
		
		if (SessionVariablen.getInstance().getSpieltag() == -1) {
			Util.showMessage("Fehler", "Bitte laden Sie zuerst einen Spieltag!");
			return false;
		}
		
		return true;
	}
}
