package de.managerspiel.util;

/**
 * Beinhaltet die Konstanten f�r das Auslesen der Kickerseite Ziel ist, bei
 * einer �nderung des Seitenaufbaus von Kicker nur in dieser Datei �ndern zu
 * m�ssen.
 */
public class KickerKonst {

	public final static String SEITENSTART = "<TH CLASS=\"FIRST ALIGNLEFT\">";
	public final static String SEITENENDE = "<TH CLASS=\"FIRST ALIGNLEFT\"";

	public final static String BLANK = "&NBSP;";

	public final static String NACH_FUSSBALLER_NAME = "</A> (";
	public final static String NACH_FUSSBALLER_NAME_2 = "</A>&N";
	public final static String NACH_FUSSBALLER_NAME_BEI_GELBROT = "</A><DIV";
	public final static String NACH_FUSSBALLER_NAME_BEI_ROT = "</A><DIV";

	public final static String TORSCHUETZE_ANFANG = "</DIV>TORSCH�TZEN</TH>";
	public final static String GELBROTE_KARTEN_ANFANG = ">GELB-ROTE KARTEN</DIV>";
	public final static String GELBE_KARTEN_ANFANG = ">GELBE KARTEN</DIV>";
	public final static String ROTE_KARTEN_ANFANG = ">ROTE KARTEN</DIV>";
	public final static String ANSTOSS_ANFANG = "<B>ANSTO";

	public final static String EIGENTOR = "EIGENTOR";
}
