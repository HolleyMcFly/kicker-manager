package de.managerspiel.util;

public class Konstanten {

	// Gr��en der einzelnen Fester
	public static final int WINDOW_NEU_HAUPTMENUE_BREITE = 550;
	public static final int WINDOW_NEU_HAUPTMENUE_HOEHE = 380;
	public static final int WINDOW_NEU_TIPPSPIEL_BREITE = 200;
	public static final int WINDOW_NEU_TIPPSPIEL_HOEHE = 150;
	public static final int WINDOW_NEU_MITSPIELER_BREITE = 200;
	public static final int WINDOW_NEU_MITSPIELER_HOEHE = 200;
	public static final int WINDOW_NEU_FUSSBALLER_BREITE = 630;
	public static final int WINDOW_NEU_FUSSBALLER_HOEHE = 450;
	public static final int WINDOW_LADEN_TIPPSPIEL_BREITE = 250;
	public static final int WINDOW_LADEN_TIPPSPIEL_HOEHE = 230;
	public static final int WINDOW_LADEN_SPIELTAG_BREITE = 240;
	public static final int WINDOW_LADEN_SPIELTAG_HOEHE = 230;
	public static final int WINDOW_PRIO_AUSWAHL_MITSPIELER_BREITE = 270;
	public static final int WINDOW_PRIO_AUSWAHL_MITSPIELER_HOEHE = 240;
	public static final int WINDOW_PRIO_ERFASSEN_BREITE = 400;
	public static final int WINDOW_PRIO_ERFASSEN_HOEHE = 500;
	public static final int ZEILENHOEHE_FUSSBALLER = 15;
	public static final int WINDOW_VERWALTUNG_VEREIN_BREITE = 400;
	public static final int WINDOW_VERWALTUNG_VEREIN_HOEHE = 430;
	public static final int WINDOW_FUSSBALLER_WECHSELN_BREITE = 450;
	public static final int WINDOW_FUSSBALLER_WECHSELN_HOEHE = 400;
	public static final int WINDOW_TIPPSPIEL_LOESCHEN_BREITE = 300;
	public static final int WINDOW_TIPPSPIEL_LOESCHEN_HOEHE = 230;
	public static final int WINDOW_MITSPIELER_LOESCHEN_BREITE = 300;
	public static final int WINDOW_MITSPIELER_LOESCHEN_HOEHE = 230;
	public static final int WINDOW_FUSSBALLER_LOESCHEN_BREITE = 300;
	public static final int WINDOW_FUSSBALLER_LOESCHEN_HOEHE = 230;
	public static final int WINDOW_EINSTELLUNGEN_BREITE = 450;
	public static final int WINDOW_EINSTELLUNGEN_HOEHE = 150;
	public static final int WINDOW_BEARBEITEN_TIPPSPIEL_BREITE = 480;
	public static final int WINDOW_BEARBEITEN_TIPPSPIEL_HOEHE = 300;
	public static final int WINDOW_BEARBEITEN_MITSPIELER_BREITE = 480;
	public static final int WINDOW_BEARBEITEN_MITSPIELER_HOEHE = 300;
	public static final int WINDOW_BEARBEITEN_FUSSBALLER_BREITE = 480;
	public static final int WINDOW_BEARBEITEN_FUSSBALLER_HOEHE = 300;
	public static final int WINDOW_AUSWERTUNG_BREITE = 960;
	public static final int WINDOW_AUSWERTUNG_HOEHE = 830;
	public static final int WINDOW_INFO_BREITE = 250;
	public static final int WINDOW_INFO_HOEHE = 200;
	public static final int WINDOW_AUSWERTUNG_ERSTELLEN_BREITE = 320;
	public static final int WINDOW_AUSWERTUNG_ERSTELLEN_HOEHE = 220;

	// Fehlercodes der Datenbank
	public static final int DB_ERROR_TABLE_EXISTS = -21;
	public static final int DB_ERROR_TABLE_EMPTY = -35;

	// Spielpositionen
	public static final int SPIELPOSITION_TORMANN = 0;
	public static final int SPIELPOSITION_VERTEIDIGER = 1;
	public static final int SPIELPOSITION_MITTELFELDSPIELER = 2;
	public static final int SPIELPOSITION_STUERMER = 3;

	// Maximalanzahl auf den Positionen
	public static final int MAX_TORMANN = 3;
	public static final int MAX_TORMANN_AUFGESTELLT = 1;
	public static final int MAX_VERTEIDIGER = 6;
	public static final int MAX_VERTEIDIGER_AUFGESTELLT = 3;
	public static final int MAX_MITTELFELD = 8;
	public static final int MAX_MITTELFELDSPIELER_AUFGESTELLT = 5;
	public static final int MAX_STUERMER = 5;
	public static final int MAX_STUERMER_AUFGESTELLT = 2;

	// Die maximale Anzahl der Fu�baller eines Mitspielers
	public static final int MAX_FUSSBALLER = 25;

	// Die maximale Anzahl an Eintr�gen in der Auswertungsmaske
	public static final int MAX_EINTRAEGE_AUSWERTUNG = 20;

	// Punkte f�r die Auswertung
	public static final int PUNKTE_TOR_STUERMER = 3;
	public static final int PUNKTE_TOR_MITTELFELDSPIELER = 4;
	public static final int PUNKTE_TOR_VERTEIDIGER = 5;
	public static final int PUNKTE_TOR_TORMANN = 6;
	public static final int PUNKTE_GELBROTE_KARTE = -3;
	public static final int PUNKTE_ROTE_KARTE = -6;
	public static final int PUNKTE_NOTE_1_0 = 10;
	public static final int PUNKTE_NOTE_1_5 = 8;
	public static final int PUNKTE_NOTE_2_0 = 6;
	public static final int PUNKTE_NOTE_2_5 = 4;
	public static final int PUNKTE_NOTE_3_0 = 2;
	public static final int PUNKTE_NOTE_3_5 = 0;
	public static final int PUNKTE_NOTE_4_0 = -2;
	public static final int PUNKTE_NOTE_4_5 = -4;
	public static final int PUNKTE_NOTE_5_0 = -6;
	public static final int PUNKTE_NOTE_5_5 = -8;
	public static final int PUNKTE_NOTE_6_0 = -10;
	public static final int PUNKTE_POSITION_FEHLT = -5;

	public static final String AUSGABEDATEI = "Auswertung";

	// Schl�ssel f�r Einstellungen
	public static final String AUSGABEPFAD = "Ausgabepfad";
}
