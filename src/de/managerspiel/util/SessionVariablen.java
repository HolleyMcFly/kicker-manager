package de.managerspiel.util;

import java.sql.Connection;
import java.sql.Statement;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.Map;

import org.eclipse.swt.widgets.Label;
import org.eclipse.swt.widgets.List;
import org.eclipse.swt.widgets.Shell;
import org.eclipse.swt.widgets.Text;

import de.managerspiel.model.Auswertungseintrag;

public class SessionVariablen {

	// Einzige Instanz der Klasse SessionVariablen
	private static SessionVariablen instance;

	private Text textBearbeiten;
	private List listFussballer;
	private List listPositionen;
	private String textOriginal;

	// DB-Verbindung
	private Statement stmt;
	private Connection con;

	// Anzeige
	private Shell shell;
	private Label namelabel;
	private Label spieltaglabel;

	// ID und Name des aktuellen Tippspiels
	private int tippspielId;
	private String tippspielname;

	// Aktueller Spieltag
	private int spieltag;
	private String spieltagtext;

	// ID des aktuellen Mitspielers
	private int mitspielerId;

	// ID des aktuellen Vereins
	private int vereinId;

	// ID des aktuellen Fußballers
	private int fussballerId;

	// Einstellungen
	private Map<String, String> einstellungen = new HashMap<String, String>();

	// Konstruktor
	// Muss private sein, um eine Singleton Klasse zu realisieren
	private SessionVariablen() {
		this.stmt = null;
		this.con = null;
		this.tippspielId = -1;
		this.tippspielname = "";
		this.spieltag = -1;
		this.spieltagtext = "";
		this.mitspielerId = -1;
		this.vereinId = -1;
		this.fussballerId = -1;
	}

	public static SessionVariablen getInstance() {
		if (SessionVariablen.instance == null)
			SessionVariablen.instance = new SessionVariablen();
		return SessionVariablen.instance;
	}

	public Statement getStmt() {
		return stmt;
	}

	public void setStmt(Statement stmt) {
		this.stmt = stmt;
	}

	public Connection getCon() {
		return con;
	}

	public void setCon(Connection con) {
		this.con = con;
	}

	public int getTippspielId() {
		return tippspielId;
	}

	public void setTippspielId(int tippspielId) {
		this.tippspielId = tippspielId;
	}

	public String getTippspielname() {
		return tippspielname;
	}

	public void setTippspielname(String tippspielname) {
		this.tippspielname = tippspielname;
	}

	public int getMitspielerId() {
		return mitspielerId;
	}

	public void setMitspielerId(int mitspielerId) {
		this.mitspielerId = mitspielerId;
	}

	public int getVereinId() {
		return vereinId;
	}

	public void setVereinId(int vereinId) {
		this.vereinId = vereinId;
	}

	public int getFussballerId() {
		return fussballerId;
	}

	public void setFussballerId(int fussballerId) {
		this.fussballerId = fussballerId;
	}

	public Shell getShell() {
		return shell;
	}

	public void setShell(Shell shell) {
		this.shell = shell;
	}

	public Label getNamelabel() {
		return namelabel;
	}

	public void setNamelabel(Label namelabel) {
		this.namelabel = namelabel;
	}

	public int getSpieltag() {
		return spieltag;
	}

	public void setSpieltag(int spieltag) {
		this.spieltag = spieltag;
	}

	public Label getSpieltaglabel() {
		return spieltaglabel;
	}

	public void setSpieltaglabel(Label spieltaglabel) {
		this.spieltaglabel = spieltaglabel;
	}

	public String getSpieltagtext() {
		return spieltagtext;
	}

	public void setSpieltagtext(String spieltagtext) {
		this.spieltagtext = spieltagtext;
	}

	public List getListFussballer() {
		return listFussballer;
	}

	public void setListFussballer(List listFussballer) {
		this.listFussballer = listFussballer;
	}

	public List getListPositionen() {
		return listPositionen;
	}

	public void setListPositionen(List listPositionen) {
		this.listPositionen = listPositionen;
	}

	public Text getTextBearbeiten() {
		return textBearbeiten;
	}

	public void setTextBearbeiten(Text textBearbeiten) {
		this.textBearbeiten = textBearbeiten;
	}

	public String getTextOriginal() {
		return textOriginal;
	}

	public void setTextOriginal(String textOriginal) {
		this.textOriginal = textOriginal;
	}

	public String getEinstellung(String key) {
		return einstellungen.get(key);
	}

	public void addEinstellung(String key, String value) {
		einstellungen.put(key, value);
	}

	public String removeEinstellung(String key) {
		return einstellungen.remove(key);
	}
}
