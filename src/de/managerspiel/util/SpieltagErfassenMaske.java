package de.managerspiel.util;

import org.eclipse.swt.SWT;
import org.eclipse.swt.widgets.Button;
import org.eclipse.swt.widgets.Combo;
import org.eclipse.swt.widgets.Label;
import org.eclipse.swt.widgets.Shell;
import org.eclipse.swt.widgets.Text;

/**
 * In dieser Klasse stehen alle Felder, die in der Maske f�r die Erfassung der
 * Spieltagsdaten notwendig sind.
 */
public class SpieltagErfassenMaske {

	// URL-Eingabe
	private Label lbl_url;
	private Text txt_url;
	private Button button_auslesen;

	// Combobox f�r die Vereine
	Combo com1;

	// Dummy-Label
	Label label_dummy;

	// Label f�r die Spielernamen erstellen
	private Label[] lbl_name = new Label[Konstanten.MAX_EINTRAEGE_AUSWERTUNG];

	// Checkbox f�r Flag "gespielt"
	private Label[] lbl_gespielt = new Label[Konstanten.MAX_EINTRAEGE_AUSWERTUNG];
	private Button[] butt_gespielt = new Button[Konstanten.MAX_EINTRAEGE_AUSWERTUNG];

	// Scrollleiste f�r die Anzahl der Tore
	private Label[] lbl_tore = new Label[Konstanten.MAX_EINTRAEGE_AUSWERTUNG];
	private Combo[] cmb_tore = new Combo[Konstanten.MAX_EINTRAEGE_AUSWERTUNG];

	// Checkbox f�r Flag "gelb-rote Karte"
	private Label[] lbl_gelbrot = new Label[Konstanten.MAX_EINTRAEGE_AUSWERTUNG];
	private Button[] butt_gelbrot = new Button[Konstanten.MAX_EINTRAEGE_AUSWERTUNG];

	// Checkbox f�r Flag "rote Karte"
	private Label[] lbl_rot = new Label[Konstanten.MAX_EINTRAEGE_AUSWERTUNG];
	private Button[] butt_rot = new Button[Konstanten.MAX_EINTRAEGE_AUSWERTUNG];

	// Auswahlliste f�r die Note
	private Label[] lbl_note = new Label[Konstanten.MAX_EINTRAEGE_AUSWERTUNG];
	private Combo[] cmb_note = new Combo[Konstanten.MAX_EINTRAEGE_AUSWERTUNG];

	public SpieltagErfassenMaske(Shell shell) {
		lbl_url = new Label(shell, SWT.TOP);
		txt_url = new Text(shell, SWT.LEFT);
		button_auslesen = new Button(shell, SWT.CENTER);
		com1 = new Combo(shell, SWT.DROP_DOWN | SWT.READ_ONLY);

		// Dummy-Label
		Label label_dummy = new Label(shell, SWT.LEFT);
		label_dummy.setText("");
		label_dummy = new Label(shell, SWT.LEFT);
		label_dummy.setText("");
		label_dummy = new Label(shell, SWT.LEFT);
		label_dummy.setText("");
		label_dummy = new Label(shell, SWT.LEFT);
		label_dummy.setText("");
		label_dummy = new Label(shell, SWT.LEFT);
		label_dummy.setText("");
		label_dummy = new Label(shell, SWT.LEFT);
		label_dummy.setText("");
		label_dummy = new Label(shell, SWT.LEFT);
		label_dummy.setText("");
		label_dummy = new Label(shell, SWT.LEFT);
		label_dummy.setText("");
		label_dummy = new Label(shell, SWT.LEFT);
		label_dummy.setText("");
		label_dummy = new Label(shell, SWT.LEFT);
		label_dummy.setText("");

		for (int i = 0; i < Konstanten.MAX_EINTRAEGE_AUSWERTUNG; i++) {
			lbl_name[i] = new Label(shell, SWT.LEFT);
			lbl_gespielt[i] = new Label(shell, SWT.LEFT);
			lbl_gespielt[i].setText("Gespielt:");
			butt_gespielt[i] = new Button(shell, SWT.CHECK);
			lbl_note[i] = new Label(shell, SWT.LEFT);
			lbl_note[i].setText("Note:");
			cmb_note[i] = new Combo(shell, SWT.DROP_DOWN | SWT.READ_ONLY);
			cmb_note[i].add("1,0");
			cmb_note[i].add("1,5");
			cmb_note[i].add("2,0");
			cmb_note[i].add("2,5");
			cmb_note[i].add("3,0");
			cmb_note[i].add("3,5");
			cmb_note[i].add("4,0");
			cmb_note[i].add("4,5");
			cmb_note[i].add("5,0");
			cmb_note[i].add("5,5");
			cmb_note[i].add("6,0");
			lbl_tore[i] = new Label(shell, SWT.LEFT);
			lbl_tore[i].setText("Anzahl Tore:");
			cmb_tore[i] = new Combo(shell, SWT.DROP_DOWN | SWT.READ_ONLY);
			cmb_tore[i].add("0");
			cmb_tore[i].add("1");
			cmb_tore[i].add("2");
			cmb_tore[i].add("3");
			cmb_tore[i].add("4");
			cmb_tore[i].add("5");
			cmb_tore[i].add("6");
			cmb_tore[i].add("7");
			cmb_tore[i].add("8");
			cmb_tore[i].add("9");
			cmb_tore[i].add("10");
			lbl_gelbrot[i] = new Label(shell, SWT.LEFT);
			lbl_gelbrot[i].setText("Gelb-rote Karte:");
			butt_gelbrot[i] = new Button(shell, SWT.CHECK);
			lbl_rot[i] = new Label(shell, SWT.LEFT);
			lbl_rot[i].setText("Rote Karte:");
			butt_rot[i] = new Button(shell, SWT.CHECK);
		}
	}

	public Button[] getButt_gelbrot() {
		return butt_gelbrot;
	}

	public void setButt_gelbrot(Button[] butt_gelbrot) {
		this.butt_gelbrot = butt_gelbrot;
	}

	public Button[] getButt_gespielt() {
		return butt_gespielt;
	}

	public void setButt_gespielt(Button[] butt_gespielt) {
		this.butt_gespielt = butt_gespielt;
	}

	public Button[] getButt_rot() {
		return butt_rot;
	}

	public void setButt_rot(Button[] butt_rot) {
		this.butt_rot = butt_rot;
	}

	public Combo[] getCmb_note() {
		return cmb_note;
	}

	public void setCmb_note(Combo[] cmb_note) {
		this.cmb_note = cmb_note;
	}

	public Combo[] getCmb_tore() {
		return cmb_tore;
	}

	public void setCmb_tore(Combo[] cmb_tore) {
		this.cmb_tore = cmb_tore;
	}

	public Combo getCom1() {
		return com1;
	}

	public void setCom1(Combo com1) {
		this.com1 = com1;
	}

	public Label[] getLbl_gelbrot() {
		return lbl_gelbrot;
	}

	public void setLbl_gelbrot(Label[] lbl_gelbrot) {
		this.lbl_gelbrot = lbl_gelbrot;
	}

	public Label[] getLbl_gespielt() {
		return lbl_gespielt;
	}

	public void setLbl_gespielt(Label[] lbl_gespielt) {
		this.lbl_gespielt = lbl_gespielt;
	}

	public Label[] getLbl_name() {
		return lbl_name;
	}

	public void setLbl_name(Label[] lbl_name) {
		this.lbl_name = lbl_name;
	}

	public Label[] getLbl_note() {
		return lbl_note;
	}

	public void setLbl_note(Label[] lbl_note) {
		this.lbl_note = lbl_note;
	}

	public Label[] getLbl_rot() {
		return lbl_rot;
	}

	public void setLbl_rot(Label[] lbl_rot) {
		this.lbl_rot = lbl_rot;
	}

	public Label[] getLbl_tore() {
		return lbl_tore;
	}

	public void setLbl_tore(Label[] lbl_tore) {
		this.lbl_tore = lbl_tore;
	}

	public Label getLbl_url() {
		return lbl_url;
	}

	public void setLbl_url(Label lbl_url) {
		this.lbl_url = lbl_url;
	}

	public Text getTxt_url() {
		return txt_url;
	}

	public void setTxt_url(Text txt_url) {
		this.txt_url = txt_url;
	}

	public Button getButton_auslesen() {
		return button_auslesen;
	}

	public void setButton_auslesen(Button button_auslesen) {
		this.button_auslesen = button_auslesen;
	}
}